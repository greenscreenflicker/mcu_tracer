#define MCU_TRACER_STARTBYTE 0xA5
#define PRINT_RS232_DATA 0

//
int monitor_test_connection(int number, int baud);
void * monitor_recieve_thread(void);
void monitor_recieve_terminate(void);
void monitor_master_allow_decoding(void);
int monitor_master_decode_string(unsigned char* buf, int len);
int monitor_master_check_parity_following_bit(unsigned char* buf, int len);
void monitor_master_req_init(void);
void monitor_master_req_data(void);
void monitor_master_write_var(uint16_t num, int32_t val);
void monitor_master_func_data(void);
void monitor_master_func_exec(uint8_t id);
void monitor_master_emergency(void);
void monitor_master_req_frequently_enable(void);
void monitor_master_req_frequently_disable(void);
void monitor_rs232_disconnect(void);
//requests device information (Order F0)
void monitor_master_device_information(void);
//requests unique id (ORDER F1)
void monitor_master_uniqueid(void);
//requests logs from mcu
void monitor_master_req_log(void);
//debugging function for simple hexdata output
void print_hex_data_simple(unsigned char* buf, int len);
void mcu_tracer_rec_portionierer_process3(unsigned char *data, int len);
int monitor_master_check_parity_dump(unsigned char* buf, int len);
