/*
 * window_varfunc.h
 *
 *  Created on: Mar 11, 2022
 *      Author: michael
 */

#ifndef WINDOW_VARFUNC_H_
#define WINDOW_VARFUNC_H_


enum
{
  VARFUNC_VAR_STORE_ID = 0,
  VARFUNC_VAR_STORE_DESC,
  VARFUNC_VAR_STORE_VALUE,
  VARFUNC_VAR_STORE_COLS
};

// external global variables
extern GtkWidget     *varfunc_vartreeview_scroll;
//function prototypes
void gui_var_init (void);
gboolean gui_var_data_init(struct set_variables *mydd);
gboolean gui_var_data_update(uint32_t *datastream);
gboolean cb_gui_var_data_changed_text(GtkWidget *widget, gpointer   data );
gboolean cb_gui_var_data_noenter(GtkWidget *widget, gpointer   usrdata );
gboolean cb_gui_var_data_switch_pressed(GtkWidget *widget, gpointer   usrdata );
gboolean gui_var_data_single_update(set_single_var_t *data, int forceupdate);

#endif /* WINDOW_VARFUNC_H_ */
