/*
 * window_log.h
 *
 *  Created on: Jun 3, 2020
 *      Author: michael
 */

#ifndef INC_WINDOW_LOG_H_
#define INC_WINDOW_LOG_H_

extern GtkWidget *gui_window_logview_grid;

void gui_window_logview(void);
void gui_window_logview_add_element(struct set_variables *mydd);
void gui_window_logview_allmsgs(struct set_variables *variables);
void gui_window_logview_msg(uint64_t time, char *msg, struct set_variables *mydd);
gboolean gui_window_logview_attach_msg(logview_t *l);

//reloads all logging information
void gui_window_logview_reload(void);

#endif /* INC_WINDOW_LOG_H_ */
