/*
 * window_about.h
 *
 *  Created on: Jun 3, 2020
 *      Author: michael
 */

#ifndef INC_WINDOW_ABOUT_H_
#define INC_WINDOW_ABOUT_H_


//global variables
extern GtkWidget* about_grid;

//global functions
void gui_window_about(void);
gboolean gui_window_label_manufacturer_updatefree(char *str);
gboolean gui_window_label_device_updatefree(char *str);
gboolean gui_window_label_softwareversion_updatefree(char *str);
gboolean gui_window_label_uniqueid_updatefree(char *str);

#endif /* INC_WINDOW_ABOUT_H_ */
