#include <gtk/gtk.h>


#ifndef DEBUGWINDOW_H
#define DEBUGWINDOW_H




enum
{
  COL_ID = 0,
  COL_TIME,
  COL_MSG,
  NUM_COLS
} ;

extern GMainContext *gtk_context_var;


void gui_debug_window(void);

void inject_call(GSourceFunc func, gpointer data);
gboolean variables_window_update(struct set_variables *mydd);
gboolean variables_window_update_vars(uint32_t *datastream);
gboolean variables_window_update_single_var(set_single_var_t *data);
gboolean gui_msg_center_add_msg(char* msg);
gboolean say_hello(void);
gboolean FuncOnMCU_update(mcu_func_t *mcufunctions);

gboolean FuncOnMCU_update(mcu_func_t *mcufunctions);
void callback_func_click_reactivate(int *id_to_reactivate);
void timeout_reset_register(guint function);
void inject_timeout(GSourceFunc func, gpointer data, guint ms);
void timeout_reset_of_fail(guint function);
void func_reset_register_callback(guint function);
char* func_get_desciptor_by_id(int id);
void func_report_execution_fail(int id,int status);

//delete later
void FuncOnMCU_dummydata(void);

extern struct set_variables *varfunc_widgets;
#endif
