#define _POSIX_THREADS 1

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//#include <alloca.h>
#include <gtk/gtk.h>
#include <pthread.h>
//#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
//#include <pwd.h>


//config
#define MAX_LABEL_LENGTH 256
#define FUNC_NAME_LENGTH 256
#define MAX_MSG_LENGTH 1000
#define LOG_MSG_MAX_LENGTH 500

typedef struct set_variables{
	char label[MAX_LABEL_LENGTH];
	int type; //0 notused (terminator), 1 long, 2 float, 3 toogle, 4 unsigned long
	int rw;
	union{
		int32_t data_l;
		float data_f;
		uint32_t data_u;
	};
	GtkWidget* label_widget;
	GtkWidget* data_widget;
} set_var_t;


typedef struct mcu_func{
	char name[FUNC_NAME_LENGTH];
	int id;
	GtkWidget* button;
} mcu_func_t;

typedef struct set_single_var{
	uint16_t addr;
	uint32_t val;
} set_single_var_t;

typedef struct logview{
	set_var_t *variables;
	char msg[LOG_MSG_MAX_LENGTH];
	uint64_t time;
} logview_t;


#include "inc/interfacer.h"
#include "inc/connect.h"
#include "inc/rs232.h"
#include "inc/debugwindow.h"
#include "inc/settingssaver.h"
#include "inc/window_about.h"
#include "inc/window_log.h"
#include "inc/window_varfunc.h"

#define MCU_TRACER_NAME "mcu_tracer"
#define SOFTWARE_VERSION_MAJOR 3
#define SOFTWARE_VERSION_MINOR 1

#define ORDER_00_PING 			 0x00
#define ORDER_01_INIT		     0x01
#define ORDER_02_UPDATE_ALL_VAL  0x02
#define ORDER_03_UPDATE_SIG_VAL  0x03
#define ORDER_08_FUNC_INIT		 0x08
#define ORDER_09_FUNC_REP		 0x09
#define ORDER_F0_SUPPLIER_INFO   0xF0
#define ORDER_F1_UNIQUE_ID       0xF1
#define ORDER_FD_LOG		     0xFD
#define ORDER_FE_MSG			 0xFE
#define ORDER_FF_EMERGENY		 0xFF

//Size in bytes
#define WORKING_BUFFER_SIZE		10000
#define WORKING_PREVENTIVE_FLUSH		8000

extern GMutex *lock_rs232;
extern GMutex *lock_paritycheckbit;
extern GMutex *lock_rec_thread;


#define BAUDRATES_COUNT 30
#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
	#define COMPORT_COUNT 39
	#define COMPORT_ENTRY_LEN 16
	#include <pwd.h>
#else 											/*Windows*/
	#define COMPORT_COUNT 16
	#define COMPORT_ENTRY_LEN 10
#endif
