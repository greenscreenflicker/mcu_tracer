#include "unified_header.h"

GtkWidget *debugwindow;
GtkWidget *debugwindow_grid;
GtkWidget *debugwindow_grid_var_func;
GtkWidget *debugwindow_varfunc_cont;
GtkWidget *debugwindow_variable_view;
GtkWidget *debugwindow_fonmcu;
GtkWidget *debugwindow_fonmcu_grid;
GtkWidget *debugwindow_set_variables;
GtkWidget *debugwindow_set_variables_grid;
GtkWidget *debugwindow_set_variables_scroll=NULL;
GtkWidget *debugwindow_read_variables;
GtkWidget *debugwindow_init_button;
GtkWidget *debugwindow_stream_button;
GtkWidget *debugwindow_emergency_button;
GtkWidget *debugwindow_quit_button;
GtkWidget *debugwindow_grid_msg_center;
GtkWidget *debugwindow_grid_msg_center_del_button;
GtkWidget *debugwindow_grid_msg_center_treeview;
GtkListStore  *debugwindow_grid_msg_center_treeview_list_store;
GMainContext *gtk_context_var;

GtkWidget *debugwindow_scroll_msg;

int system_streaming=0;



struct set_variables *varfunc_widgets;
struct set_variables *read_variables_data;
struct mcu_func *mcu_functions=NULL;

//Toogle Button variable change. Writes changes to memory
void callback_set_variables_changed(GtkWidget *widget, gpointer   data ){
	//printf("go callback\n");
	
    gint loop=0;
    while(varfunc_widgets[loop].type){
		//printf("loop %i (%i==%i)",loop,set_variables_data[loop].data_widget,widget);
		if(varfunc_widgets[loop].data_widget==widget){
			//printf("go callback\n");	
			if(varfunc_widgets[loop].type!=3){
				printf("this is not a toogle button.\n");
			}
			if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(varfunc_widgets[loop].data_widget))==TRUE){
				varfunc_widgets[loop].data_l=1;
				//printf("%s: active\n",set_variables_data[loop].label);
			}else{
				varfunc_widgets[loop].data_l=0;
				//printf("%s: deactive\n",set_variables_data[loop].label);
			}
			fflush(stdout);
			monitor_master_write_var(loop,varfunc_widgets[loop].data_l);
			return;
		}

		loop=loop+1;
	}
	printf("button not found\n");
	fflush(stdout);
}

//If you press enter, the value is saved to the right elements.
void callback_set_variables_changed_text(GtkWidget *widget, gpointer   data ){
	//printf("We had a text enter.\n");
    gint loop=0;
    while(varfunc_widgets[loop].type){
		//printf("loop %i (%i==%i)",loop,set_variables_data[loop].data_widget,widget);
		if(varfunc_widgets[loop].data_widget==widget){	
			if(varfunc_widgets[loop].type==1){
				varfunc_widgets[loop].data_l=atoi(gtk_entry_get_text(GTK_ENTRY(varfunc_widgets[loop].data_widget)));
				//printf("entered: %i\n",set_variables_data[loop].data_l);
			}else if(varfunc_widgets[loop].type==2){
				varfunc_widgets[loop].data_f=atof(gtk_entry_get_text(GTK_ENTRY(varfunc_widgets[loop].data_widget)));
			}else if(varfunc_widgets[loop].type==4){
				varfunc_widgets[loop].data_l=strtoul(gtk_entry_get_text(GTK_ENTRY(varfunc_widgets[loop].data_widget)),NULL,0);
				//printf("entered: %u\n",set_variables_data[loop].data_l);
			}
			//transfer data to mcu
			monitor_master_write_var(loop,varfunc_widgets[loop].data_l);
			return;
		}
		loop=loop+1;
	}
}

//If an element is entered or left, it's value its set to the orignal value.
//This does this function
void callback_set_variables_no_enter(GtkWidget *widget, gpointer   usrdata ){
    gint loop=0;
    while(varfunc_widgets[loop].type){
		if(varfunc_widgets[loop].data_widget==widget){
			//printf("%s was changed...\n",set_variables_data[loop].label);
			//Set string to orginal
			char str[30];
			if(varfunc_widgets[loop].type==1){
				sprintf(str, "%i", varfunc_widgets[loop].data_l);
			}else if(varfunc_widgets[loop].type==2){
				sprintf(str, "%lf", varfunc_widgets[loop].data_f);
			}else if(varfunc_widgets[loop].type==4){
				sprintf(str, "%u", varfunc_widgets[loop].data_l);
			}

			gtk_entry_set_text(GTK_ENTRY(varfunc_widgets[loop].data_widget),str);
			return;
		}
		loop=loop+1;
	}
}


void variables_window (void){
	debugwindow_varfunc_cont=gtk_grid_new();
	
	debugwindow_set_variables=gtk_frame_new ("MCU variables");
	gtk_grid_attach(GTK_GRID(debugwindow_varfunc_cont),debugwindow_set_variables,1,1,1,1);
	//adjust spacing

	
	//Now we can add elements to the grid
    varfunc_widgets=NULL;
 
	//Draw functions frame
	debugwindow_fonmcu=gtk_frame_new("Functions");
	gtk_widget_set_vexpand (debugwindow_fonmcu, TRUE);
	gtk_widget_set_hexpand (debugwindow_fonmcu, TRUE);
	debugwindow_fonmcu_grid=gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(debugwindow_fonmcu), debugwindow_fonmcu_grid);  	
	
	FuncOnMCU_dummydata();
	gtk_grid_attach(GTK_GRID(debugwindow_varfunc_cont),debugwindow_fonmcu,2,1,1,1);
}

int variables_setvariables_count(struct set_variables *mydd){
	int loop=0;
	while(mydd[loop].type){
		loop++;
	}
	return loop;
}

gboolean variables_window_update(struct set_variables *mydd){
	//update newer window
	int setvariables=variables_setvariables_count(mydd)+1;
	struct set_variables *newwindow=malloc(sizeof(struct set_variables)*setvariables);
	memcpy(newwindow,mydd,sizeof(struct set_variables)*setvariables);
	gui_var_data_init(newwindow);

	//recrusivly destroy grid
	if(debugwindow_set_variables_scroll) gtk_widget_destroy(debugwindow_set_variables_scroll);
	free(varfunc_widgets); //free óld memory
	
	//create new grid
	debugwindow_set_variables_grid=gtk_grid_new();
	//add it to container 
	//make window scrollable (if required)

	//make window scrollable (if required)
	debugwindow_set_variables_scroll=gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (debugwindow_set_variables_scroll),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(debugwindow_set_variables_scroll),debugwindow_set_variables_grid);

	gtk_container_add(GTK_CONTAINER(debugwindow_set_variables), debugwindow_set_variables_scroll );
	 
	//struct set_variables *mydd;
    if(mydd==NULL){
		printf("data aquisition failed\n");
		//return;
	}
    varfunc_widgets=mydd;
    gint loop=0;
    
    while(mydd[loop].type){
		//printf("%i: creating %s\n",loop,mydd[loop].label);
		
		mydd[loop].label_widget=gtk_label_new(mydd[loop].label);
	
		char str[30];
		if(mydd[loop].type==1){
			snprintf(str,30, "%i", mydd[loop].data_l);
			mydd[loop].data_widget=gtk_entry_new();
			gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			g_signal_connect (mydd[loop].data_widget, "activate",  G_CALLBACK (callback_set_variables_changed_text), NULL);
			g_signal_connect (mydd[loop].data_widget, "state-changed",  G_CALLBACK (callback_set_variables_no_enter), NULL);
		}else if(mydd[loop].type==2){
			snprintf(str,30, "%lf", mydd[loop].data_f);
			mydd[loop].data_widget=gtk_entry_new();
			gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			g_signal_connect (mydd[loop].data_widget, "activate",  G_CALLBACK (callback_set_variables_changed_text), NULL);
			g_signal_connect (mydd[loop].data_widget, "state-changed",  G_CALLBACK (callback_set_variables_no_enter), NULL);
		}else if(mydd[loop].type==3){
			//gboolean active=(mydd[0].data_l!=0);
			mydd[loop].data_widget=gtk_check_button_new ();
			gboolean active=TRUE;
			if(mydd[loop].data_l==0){
				active=FALSE;
			}
			
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(mydd[loop].data_widget),active);
			g_signal_connect (mydd[loop].data_widget, "toggled",  G_CALLBACK (callback_set_variables_changed), NULL);
		}else if(mydd[loop].type ==4){
			snprintf(str,30, "%u", mydd[loop].data_l);
			mydd[loop].data_widget=gtk_entry_new();
			gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			g_signal_connect (mydd[loop].data_widget, "activate",  G_CALLBACK (callback_set_variables_changed_text), NULL);
			g_signal_connect (mydd[loop].data_widget, "state-changed",  G_CALLBACK (callback_set_variables_no_enter), NULL);
		}
		
		//Horizontaly expand
		gtk_widget_set_hexpand (mydd[loop].label_widget, TRUE);
		gtk_widget_set_hexpand (mydd[loop].data_widget, TRUE);
		
		//read write selection
		if(mydd[loop].rw>0){
			gtk_widget_set_sensitive (mydd[loop].data_widget, FALSE);
		}
		
		gtk_grid_attach (GTK_GRID (debugwindow_set_variables_grid), mydd[loop].label_widget, 0, loop, 1, 1); //pos x, pos y, width x, with y
		gtk_grid_attach (GTK_GRID (debugwindow_set_variables_grid), mydd[loop].data_widget, 1, loop, 1, 1); //pos x, pos y, width x, with y
	
		loop=loop+1;
	}
	//adjust spacing to have equal appreance
	gtk_grid_set_row_spacing(GTK_GRID(debugwindow_set_variables_grid),3);
	//printf("%i: creating %s\n",1,mydd[1].label);
	gtk_widget_show_all(debugwindow_set_variables);
	return G_SOURCE_REMOVE;
}

gboolean variables_window_update_vars(uint32_t *datastream){

	//use other function
	gui_var_data_update(datastream);

	//Updates the values in the specific input types
	struct set_variables *mydd;
	mydd=varfunc_widgets; //local working copy to easy my life
    gint loop=0;
    
    while(mydd[loop].type){
		mydd[loop].data_l=datastream[loop];
		char str[30];
		if(mydd[loop].type==1){
			snprintf(str,30, "%i", mydd[loop].data_l);
			//do not overwrite, when user is editing
			if(gtk_widget_is_focus (GTK_WIDGET(mydd[loop].data_widget))==FALSE){
				gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			}
		}else if(mydd[loop].type==2){
			snprintf(str,30, "%lf", mydd[loop].data_f);
			//do not overwrite, when user is editing
			if(gtk_widget_is_focus (GTK_WIDGET(mydd[loop].data_widget))==FALSE){
				gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			}
		}else if(mydd[loop].type==3){
			//mydd[loop].data_widget=gtk_check_button_new ();
			//gboolean active=(mydd[0].data_l!=0);
			gboolean active=TRUE;
			if(mydd[loop].data_l==0){
				active=FALSE;
			}
			g_signal_handlers_block_by_func(mydd[loop].data_widget,callback_set_variables_changed,NULL);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(mydd[loop].data_widget),active);
			g_signal_handlers_unblock_by_func(mydd[loop].data_widget,callback_set_variables_changed,NULL);
		}else if(mydd[loop].type==4){
			snprintf(str,30, "%u", mydd[loop].data_l);
			//do not overwrite, when user is editing
			if(gtk_widget_is_focus (GTK_WIDGET(mydd[loop].data_widget))==FALSE){
				gtk_entry_set_text(GTK_ENTRY(mydd[loop].data_widget),str);
			}
		}
		loop=loop+1;
	}
	free(datastream);
	return G_SOURCE_REMOVE;	
}


gboolean variables_window_update_single_var(set_single_var_t *data){
	//update new window
	gui_var_data_single_update(data,1);


	int updateid=data->addr;
	struct set_variables *mydd;
	mydd=varfunc_widgets;
	int loop=0;
	int foundinsertid=0;
	while(mydd[loop].type){
		if(loop==updateid){
			foundinsertid=1;
			break;
		}
		loop++;
	}
	if(foundinsertid==0){
		printf("WARNING: id %i during update not existend.\n",foundinsertid);
		return G_SOURCE_REMOVE;
	}

	mydd[updateid].data_l=data->val;
	//printf("addr:%i|data:%i\n",data->addr,data->val);
	char str[30];
	if(mydd[updateid].type==1){
		snprintf(str, 30,"%i", mydd[updateid].data_l);
		gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
	}else if(mydd[updateid].type==2){
		snprintf(str, 30, "%lf", mydd[updateid].data_f);
		gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
	}else if(mydd[updateid].type==3){
		gboolean active=TRUE;
		if(mydd[updateid].data_l==0){
			active=FALSE;
		}
		g_signal_handlers_block_by_func(mydd[updateid].data_widget,callback_set_variables_changed,NULL);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(mydd[updateid].data_widget),active);
		g_signal_handlers_unblock_by_func(mydd[updateid].data_widget,callback_set_variables_changed,NULL);
	}else if(mydd[updateid].type==4){
		snprintf(str, 30, "%u", mydd[updateid].data_l);
		gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
	}
	free(data);
	return G_SOURCE_REMOVE;	
}


// FuncOnMCU
void callback_func_clicked(GtkWidget *widget, gpointer   data ){
	int loop=0;
    while(mcu_functions[loop].id>0){
		if(mcu_functions[loop].button==widget){
			//we found it
			//printf("ID %i was clicked.\n",mcu_functions[loop].id);
		               
            gtk_widget_set_sensitive (mcu_functions[loop].button, FALSE);
			monitor_master_func_exec(mcu_functions[loop].id);
			timeout_reset_register(mcu_functions[loop].id);
			return;
		}
		loop++;
	}
}

void callback_func_click_reactivate(int *id_to_reactivate){
	//reactivates a button after click
	int reactivate=*id_to_reactivate;
	//printf("I reactivated %i\n",reactivate);

	int loop=0;
	//We loop here on propose to avoid crashes on corrupt data.
	while(mcu_functions[loop].id>0){
		if(mcu_functions[loop].id==reactivate){
			//we found it
            gtk_widget_set_sensitive (mcu_functions[loop].button, TRUE);
			return;
		}
		loop++;
	}
	free(id_to_reactivate);
}


char* func_get_desciptor_by_id(int id){
	int loop=0;
	//We loop here on propose to avoid crashes on corrupt data.
	while(mcu_functions[loop].id>0){
		if(mcu_functions[loop].id==id){
			//we found it
			return mcu_functions[loop].name;
		}
		loop++;
	}
}

void func_report_execution_fail_execute(int *id){
	int function=id[0];
	int funcstatus=id[1];
	char *msg=malloc(sizeof(char)*1000);
	snprintf(msg,1000,"FuncFail \"%s\" (id=%i) returned error code %i",func_get_desciptor_by_id(function),function,funcstatus);
	inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
	free(id);
}

void func_report_execution_fail(int id,int status){
	int *data=malloc(2*sizeof(int));
	data[0]=id;
	data[1]=status;
	inject_call((GSourceFunc)func_report_execution_fail_execute, data);
}

gboolean callback_func_click_reactivate_timeout(int *id_to_reactivate){
	//printf("Timeout reactivated %i\n",*id_to_reactivate);
	callback_func_click_reactivate(id_to_reactivate);
	return 	G_SOURCE_REMOVE;
}

void timeout_reset_register(guint function){
	//
	int *data=malloc(sizeof(int));
	data[0]=function;
	inject_timeout((GSourceFunc)callback_func_click_reactivate_timeout, data,1500);
}

void timeout_reset_of_fail(guint function){
	int *data=malloc(sizeof(int));
	data[0]=function;
	inject_timeout((GSourceFunc)callback_func_click_reactivate_timeout, data,500);
}

void func_reset_register_callback(guint function){
	int *data=malloc(sizeof(int));
	data[0]=function;
	inject_call((GSourceFunc)callback_func_click_reactivate, data);
}

void FuncOnMCU_dummydata(void){
	mcu_func_t *mcufunctions=malloc(sizeof(mcu_func_t)*5);
	int fill=0;
	/*
	strcpy(mcufunctions[fill].name,"button 1");
	mcufunctions[fill].id=1;
	// next item
	fill=fill+1;
	strcpy(mcufunctions[fill].name,"button 2");
	mcufunctions[fill].id=2;	
	fill=fill+1;
	strcpy(mcufunctions[fill].name,"button 3");
	mcufunctions[fill].id=3;
	fill=fill+1;*/
	mcufunctions[fill].id=-1;
	FuncOnMCU_update(mcufunctions);
}

void callback_FuncOnMCU_windowhide(void){
	char *msg=malloc(sizeof(char)*1000);
	strcpy(msg,"MCUTRACER: User minimized function window.");
	inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
	gtk_widget_hide(debugwindow_fonmcu);
}


gboolean FuncOnMCU_update(mcu_func_t *mcufunctions){
	//recrusivly destroy grid
	gtk_widget_destroy(debugwindow_fonmcu_grid);
	
	//if something should produce glib errors, check this!
	free(mcu_functions); //free óld memory
	
	//create new grid
	debugwindow_fonmcu_grid=gtk_grid_new();
	int data=0;
	while(mcufunctions[data].id>0){
		//Add buttons
		mcufunctions[data].button=gtk_button_new_with_label(mcufunctions[data].name);
		g_signal_connect (mcufunctions[data].button, "clicked",G_CALLBACK (callback_func_clicked), NULL);
		gtk_widget_set_hexpand(mcufunctions[data].button,TRUE);
		gtk_grid_attach (GTK_GRID (debugwindow_fonmcu_grid), mcufunctions[data].button, 1, 1+data, 1, 1);
		data=data+1;
	}
	//Empty string was transmitted
	if(data==0){
		mcufunctions[data].button=gtk_label_new("Functions not supported by the firmware.\nUpdate to newer firmware.");
		gtk_widget_set_halign((mcufunctions[data].button),GTK_ALIGN_CENTER); //center label
		gtk_widget_set_valign((mcufunctions[data].button),GTK_ALIGN_CENTER); //center label
		gtk_widget_set_hexpand(mcufunctions[data].button,TRUE);
		//gtk_widget_set_vexpand(mcufunctions[data].button,TRUE);
		
		
		gtk_grid_attach (GTK_GRID (debugwindow_fonmcu_grid), mcufunctions[data].button, 1, 1, 2, 1 );
		
		GtkWidget *removebutton;
		removebutton=gtk_button_new_with_label("remove message");
		gtk_grid_attach (GTK_GRID (debugwindow_fonmcu_grid), removebutton, 1, 2, 2, 1 );
		g_signal_connect (removebutton, "clicked",G_CALLBACK (callback_FuncOnMCU_windowhide), NULL);
		
	}
	gtk_container_add(GTK_CONTAINER(debugwindow_fonmcu), debugwindow_fonmcu_grid); 
	gtk_widget_show_all(debugwindow_fonmcu);
	mcu_functions=mcufunctions;
	return G_SOURCE_REMOVE;
}





/* Message center */


unsigned int gui_msgcenterid=0;
gboolean gui_msg_center_add_msg(char* msg){
	GtkTreeIter    iter;
	char timeformat[100];
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
    info = localtime( &rawtime );
    strftime(timeformat,80,"%H:%M:%S %d.%m.%Y", info);
   
	gtk_list_store_prepend (debugwindow_grid_msg_center_treeview_list_store, &iter);
	gtk_list_store_set (debugwindow_grid_msg_center_treeview_list_store, &iter,
					  COL_ID,gui_msgcenterid,
					  COL_TIME,timeformat,
					  COL_MSG,msg,
                      -1);
    /*
    if(gui_msgcenterid>15){
		GtkTreePath *path;
		path = gtk_tree_path_new_from_string ("16"); //who required that bad workaround? stupid implementation!
		gtk_tree_model_get_iter (GTK_TREE_MODEL (debugwindow_grid_msg_center_treeview_list_store),
                           &iter,
                           path);
        gtk_list_store_remove (debugwindow_grid_msg_center_treeview_list_store,
                       &iter);
    }*/
	gui_msgcenterid=gui_msgcenterid+1;
	gtk_widget_show(debugwindow_grid_msg_center_treeview); //make sure we see added element
	//gtk_widget_show_all(debugwindow);
	free(msg);
	return G_SOURCE_REMOVE;	
}

void gui_treeview_fix(void){
	//
	GtkTreeIter    iter;
	gtk_list_store_prepend (debugwindow_grid_msg_center_treeview_list_store, &iter);
	gtk_list_store_remove (debugwindow_grid_msg_center_treeview_list_store,&iter);
	gtk_widget_show(debugwindow_grid_msg_center_treeview);
}

void gui_msg_center_clear_msg(void){
	gtk_list_store_clear (debugwindow_grid_msg_center_treeview_list_store);
	gui_msgcenterid=0;
}

void gui_msg_center_add_msg_test(void){
	 char *str=malloc(50);
	 strcpy(str,"This is a test");
	 gui_msg_center_add_msg(str);
 }
 
static GtkTreeModel *
gui_msg_center_message_list_dummy_data (void)
{
   GtkListStore  *store;
  GtkTreeIter    iter;
  
  store = gtk_list_store_new (NUM_COLS, G_TYPE_UINT, G_TYPE_STRING,G_TYPE_STRING);
  debugwindow_grid_msg_center_treeview_list_store=store;
  
  return GTK_TREE_MODEL (store);
}

void gui_msg_center_message_list_create (void){
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
  GtkWidget           *view;

  view = gtk_tree_view_new ();
  gui_msgcenterid=0;
  /* --- Column #1 --- */

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1,      
                                               "#",  
                                               renderer,
                                               "text", COL_ID,
                                               NULL);

  /* --- Column #2 --- */

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1,      
                                               "time",  
                                               renderer,
                                               "text", COL_TIME,
                                               NULL);
  /* --- Column #3 --- */

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                               -1,      
                                               "message",  
                                               renderer,
                                               "text", COL_MSG,
                                               NULL);

  model = gui_msg_center_message_list_dummy_data ();

  gtk_tree_view_set_model (GTK_TREE_VIEW (view), model);

  /* The tree view has acquired its own reference to the
   *  model, so we can drop ours. That way the model will
   *  be freed automatically when the tree view is destroyed */

  g_object_unref (model);
  gtk_widget_set_hexpand (view, TRUE);
  debugwindow_grid_msg_center_treeview=view;
}



void gui_msg_center_init(void){
	//New GRID
	//debugwindow_grid_msg_center=gtk_grid_new ();
	//Button for deleting messages

	

	gui_msg_center_message_list_create();

	
	
	//gtk_grid_attach (GTK_GRID (debugwindow_grid_msg_center), debugwindow_grid_msg_center_treeview, 0, 0, 1, 1); //pos x, pos y, width x, with y
	
	debugwindow_scroll_msg=gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (debugwindow_scroll_msg),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(debugwindow_scroll_msg),debugwindow_grid_msg_center_treeview);
}
 
void gui_debug_emergency(void){
	//
	monitor_master_emergency(); //send to microcontroller
	char *msg=malloc(100);
	strcpy(msg,"Emergency button pressed");
	gui_msg_center_add_msg(msg);
}

void gui_debug_stream(void){
	if(system_streaming==0){
		gtk_button_set_label (GTK_BUTTON(debugwindow_stream_button),"Stop Streaming");
		system_streaming=1;
		//ToDO: add code for starting streaming here.
		monitor_master_req_frequently_enable();
	}else{
		gtk_button_set_label (GTK_BUTTON(debugwindow_stream_button),"Start Streaming");
		system_streaming=0;
		monitor_master_req_frequently_disable();
	}
}

void gui_debug_quit(void){
	monitor_rs232_disconnect();
	gtk_main_quit ();
}


void gui_debug_init_data(void){
	monitor_master_req_init();
	//a short sleep so we dont messed up
	//usleep(5000); -->This delay is not required, as we added portionierer

	//request function data
	monitor_master_func_data();
	gui_msg_center_clear_msg();

	char *msg=malloc(100);
	strcpy(msg,"PC: Debugging session started");
	gui_msg_center_add_msg(msg);
	//gui_window_about_device_update_request(); //fills about window with mcu information
	//gui_window_logview_reload(); //load log information
	//monitor_master_req_data();
}

void gui_notebook_switched_page(void){
	gui_treeview_fix();
}

void gui_debug_window(void){
	//printf("Starting debug window\n");
	
    debugwindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_position(GTK_WINDOW(debugwindow),GTK_WIN_POS_CENTER);
    gtk_window_set_default_size (GTK_WINDOW(debugwindow),
                             800, //w
                             600); //h
	/* When the window is given the "delete-event" signal (this is given
     * by the window manager, usually by the "close" option, or on the
     * titlebar), we ask it to call the delete_event () function
     * as defined above. The data passed to the callback
     * function is NULL and is ignored in the callback function. */
    g_signal_connect (GTK_WINDOW (debugwindow), "delete-event",
		      G_CALLBACK (gtk_main_quit), NULL);
    
    /* Here we connect the "destroy" event to a signal handler.  
     * This event occurs when we call gtk_widget_destroy() on the window,
     * or if we return FALSE in the "delete-event" callback. */
    g_signal_connect (GTK_WINDOW (debugwindow), "destroy",
		      G_CALLBACK (gui_debug_quit), NULL);

	gtk_window_set_title (GTK_WINDOW (debugwindow), "MCU Tracer");	  
	
	debugwindow_grid=gtk_grid_new (); //Creating Grid

	//adding grid to window
	gtk_container_add (GTK_CONTAINER (debugwindow), debugwindow_grid);

	//Adding variable view. A notebook is used to have different display options.
	debugwindow_variable_view=gtk_notebook_new();
	gtk_widget_set_hexpand (debugwindow_variable_view, TRUE);
	gtk_widget_set_vexpand (debugwindow_variable_view, TRUE);
	//For now we add only dummy elements
	//gui_read_variables_init();
	variables_window();
	gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
			debugwindow_varfunc_cont,
			gtk_label_new("Variables Old"));

	gui_var_init();
	gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
			varfunc_vartreeview_scroll,
			gtk_label_new("Variables New"));

	gui_msg_center_init();
	gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
			debugwindow_scroll_msg,
			gtk_label_new("Messages"));

	gui_window_logview();
	gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
			gui_window_logview_grid,
			gtk_label_new("Log"));

	gui_window_about();
	gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
			about_grid,
			gtk_label_new("About"));
                          /*

    gtk_notebook_append_page (GTK_NOTEBOOK(debugwindow_variable_view),
                          gtk_label_new("unimplemented feature"),
                          gtk_label_new("Setup"));    */                  
     
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_variable_view, 0, 0, 5, 1);
	//adding signal
	g_signal_connect (debugwindow_variable_view, "switch-page",G_CALLBACK (gui_notebook_switched_page), NULL);
	//Requests device information when page is switched
	//g_signal_connect (debugwindow_variable_view, "switch-page",G_CALLBACK (gui_window_about_device_update_request), NULL);
	//Adding the control variable box
	
	
	
	//gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_set_variables, 3, 0, 1, 1); //pos x, pos y, width x, with y
	//Set selection mode of debug window to zero.

	///Adding buttons
	//Init
	debugwindow_init_button=gtk_button_new_with_label ("Reinit");
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_init_button, 0, 1, 1, 1);
	g_signal_connect (debugwindow_init_button, "clicked",G_CALLBACK (gui_debug_init_data), NULL);

	//Stream
	debugwindow_stream_button=gtk_button_new_with_label ("Start Streaming");
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_stream_button, 1, 1, 1, 1);
	g_signal_connect (debugwindow_stream_button, "clicked",G_CALLBACK (gui_debug_stream), NULL);
	//Clear message
	debugwindow_grid_msg_center_del_button=gtk_button_new_with_label("Clear messages");
	g_signal_connect (debugwindow_grid_msg_center_del_button, "clicked",G_CALLBACK (gui_msg_center_clear_msg), NULL);
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_grid_msg_center_del_button, 2, 1, 1, 1);
	
	//Emergency
	debugwindow_emergency_button=gtk_button_new_with_label ("Emergency");
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_emergency_button, 3, 1, 1, 1);
	g_signal_connect (debugwindow_emergency_button, "clicked",G_CALLBACK (gui_debug_emergency), NULL);
	
	//Quit
	debugwindow_quit_button=gtk_button_new_with_label ("Quit");
	gtk_grid_attach (GTK_GRID (debugwindow_grid), debugwindow_quit_button, 4, 1, 1, 1);
	g_signal_connect (debugwindow_quit_button, "clicked",G_CALLBACK (gui_debug_quit), NULL);
	
	

	//Maximize window
	gtk_window_maximize(GTK_WINDOW(debugwindow));
	//Finally showing all
	gtk_widget_show_all(debugwindow);
	//Load our variables
	gui_debug_init_data();
	//allow the read threat to change our window
	monitor_master_allow_decoding();
	usleep(100000); //give mcu some time to react
	gui_debug_stream(); //Start streaming automatically
	

}

gboolean say_hello(void){
	printf("Hello\n");
	return G_SOURCE_REMOVE;
}

void inject_call(GSourceFunc func, gpointer data){
	GSource *source;
	source = g_idle_source_new();
	g_source_set_callback(source, func, data, NULL);
	g_source_attach(source, gtk_context_var);
	g_source_unref(source);
}

void inject_timeout(GSourceFunc func, gpointer data, guint ms){
	GSource *source;
	source = g_timeout_source_new (ms);
	g_source_set_callback(source, func, data, NULL);
	g_source_attach(source,gtk_context_var);
	g_source_unref(source);
}

