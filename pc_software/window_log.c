/*
 * window_log.c
 *
 *  Created on: Jun 3, 2020
 *      Author: michael
 */

#include "unified_header.h"

GtkWidget *gui_window_logview_grid;
GtkWidget *gui_window_logview_grid_frame_msg;
GtkWidget *gui_window_logview_grid_frame_msg_scroll;
GtkWidget *gui_window_logview_grid_frame_msg_treeview;
GtkWidget *gui_window_logview_grid_frame_var;
GtkWidget *gui_window_logview_grid_frame_var_scroll;
GtkWidget *gui_window_logview_grid_frame_var_treeview;

GtkListStore  *logview_msg_treeview_list_store=NULL;
GtkListStore  *logview_msg_treeview_var_store=NULL;

enum
{
  COLLOG_ID = 0,
  COLLOG_TIME,
  COLLOG_MSG,
  COLLOG_PTR,
  LOGNUM_COLS
};

enum
{
  COLLOGVAR_ID = 0,
  COLLOGVAR_DESC,
  COLLOGVAR_VALUE,
  COLLOGVAR_BINARY,
  LOGNUMVAR_COLS
};

static GtkTreeModel *
gui_window_logview_dummydata (void)
{
	GtkListStore  *store;
	GtkTreeIter    iter;

	store = gtk_list_store_new (LOGNUM_COLS, G_TYPE_UINT64, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_UINT64);
	logview_msg_treeview_list_store=store;

	return GTK_TREE_MODEL (store);
}


void gui_window_logview_msginit (void){
	GtkCellRenderer     *renderer;
	GtkTreeModel        *model;
	GtkWidget           *view;

	gui_window_logview_grid_frame_msg_treeview = gtk_tree_view_new ();

	/* --- Column #1 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_msg_treeview),
			-1,
			"#",
			renderer,
			"text", COLLOG_ID,
			NULL);

	/* --- Column #2 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_msg_treeview),
			-1,
			"Time",
			renderer,
			"text", COLLOG_TIME,
			NULL);

	/* --- Column #3 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_msg_treeview),
			-1,
			"Message",
			renderer,
			"text", COLLOG_MSG,
			NULL);

	model = gui_window_logview_dummydata ();

	gtk_tree_view_set_model (GTK_TREE_VIEW (gui_window_logview_grid_frame_msg_treeview), model);

	/* The tree view has acquired its own reference to the
	 *  model, so we can drop ours. That way the model will
	 *  be freed automatically when the tree view is destroyed */

	g_object_unref (model);
	gtk_widget_set_hexpand (gui_window_logview_grid_frame_msg_treeview, TRUE);

	//make window scrollable (if required)
	gui_window_logview_grid_frame_msg_scroll=gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (gui_window_logview_grid_frame_msg_scroll),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(gui_window_logview_grid_frame_msg_scroll),gui_window_logview_grid_frame_msg_treeview);
	gtk_container_add(GTK_CONTAINER(gui_window_logview_grid_frame_msg), gui_window_logview_grid_frame_msg_scroll);
}

//gets called when a selected log message is selected.
void gui_window_msg_selected(void){
    GtkTreeSelection *msg_selection;
    msg_selection=gtk_tree_view_get_selection (GTK_TREE_VIEW(gui_window_logview_grid_frame_msg_treeview));
    if(!msg_selection) return; // just confirm that nothing is zero
    GtkTreeIter iter;
    GtkTreeModel *model;

    if (gtk_tree_selection_get_selected (msg_selection, &model, &iter)){
    	struct set_variables *variable_snapshot;
    	gtk_tree_model_get (model, &iter, COLLOG_PTR, &variable_snapshot, -1);
    	gui_window_logview_allmsgs(variable_snapshot);
    }else{
    	//nothing selected
    }
}

uint32_t logview_cnt=0;
void gui_window_logview_msg(uint64_t time, char *msg, struct set_variables *mydd){
	GtkTreeIter    iter;

	char timeformat[100];
	snprintf(&timeformat[0],100,"%li",time);
	gtk_list_store_append(logview_msg_treeview_list_store, &iter);
	gtk_list_store_set (logview_msg_treeview_list_store, &iter,
			COLLOG_ID,++logview_cnt,
			COLLOG_TIME,timeformat,
			COLLOG_MSG, msg,
			COLLOG_PTR, mydd,
                      -1);

}

void gui_window_logview_varinit (void){
	GtkCellRenderer     *renderer;
	GtkTreeModel        *model;
	GtkWidget           *view;

	gui_window_logview_grid_frame_var_treeview = gtk_tree_view_new ();

	/* --- Column #1 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_var_treeview),
			-1,
			"Variable",
			renderer,
			"text", COLLOGVAR_DESC,
			NULL);

	/* --- Column #2 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_var_treeview),
			-1,
			"Value",
			renderer,
			"text", COLLOGVAR_VALUE,
			NULL);
	/* --- Column #2 --- */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gui_window_logview_grid_frame_var_treeview),
			-1,
			"Binary",
			renderer,
			"text", COLLOGVAR_BINARY,
			NULL);


	logview_msg_treeview_var_store = gtk_list_store_new (LOGNUMVAR_COLS,
			G_TYPE_UINT,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_STRING);

	gtk_tree_view_set_model (GTK_TREE_VIEW (gui_window_logview_grid_frame_var_treeview),
			GTK_TREE_MODEL(logview_msg_treeview_var_store));

	/* The tree view has acquired its own reference to the
	 *  model, so we can drop ours. That way the model will
	 *  be freed automatically when the tree view is destroyed */

	g_object_unref (GTK_TREE_MODEL(logview_msg_treeview_var_store));
	gtk_widget_set_hexpand (gui_window_logview_grid_frame_var_treeview, TRUE);

	//make window scrollable (if required)
	gui_window_logview_grid_frame_var_scroll=gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (gui_window_logview_grid_frame_var_scroll),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(gui_window_logview_grid_frame_var_scroll),gui_window_logview_grid_frame_var_treeview);
	gtk_container_add(GTK_CONTAINER(gui_window_logview_grid_frame_var), gui_window_logview_grid_frame_var_scroll);
}

void gui_window_logview_add_element(struct set_variables *mydd){
	GtkTreeIter    iter;

	char value[100];
	if(mydd->type==0) return;
	if(mydd->type==1){
		snprintf(value,100,"%i",mydd->data_l);
	}else if(mydd->type==2){
		snprintf(value,100,"%f",mydd->data_f);
	}else if(mydd->type==3){
		if(mydd->data_u){
			strncpy(value,"[XXX]",100);
		}else{
			strncpy(value,"",100);
		}
	}else if(mydd->type==4){
		snprintf(value,100,"%u",mydd->data_u);
	}else{
		printf("gui_window_logview_add_element: Variable type not yet supported.\n");
	}

	char binary[100];
	//https://stackoverflow.com/questions/14733761/printf-formatting-for-hex
	snprintf(binary,100,"%08x", mydd->data_u);

	gtk_list_store_append(logview_msg_treeview_var_store, &iter);
	gtk_list_store_set (logview_msg_treeview_var_store, &iter,
			COLLOGVAR_ID,1,
			COLLOGVAR_DESC,mydd->label,
			COLLOGVAR_VALUE,value,
			COLLOGVAR_BINARY,binary,
                      -1);
}

void gui_window_logview_allmsgs(struct set_variables *variables){
	//empty list
	gtk_list_store_clear(logview_msg_treeview_var_store);

	//check if variables can work
	if(!variables){
		printf("gui_window_logview_allmsgs: Received empty log variables.\n");
		fflush(stdout);
		return;
	}

	//look to add elements again
	while(variables->type){ //terminate if we get the terminator
		gui_window_logview_add_element(variables);
		variables++;
	}
}

gboolean gui_window_logview_attach_msg(logview_t *l){
	if(!l->variables){
		printf("gui_window_logview_attach_msg: should attach empty variable. Probably an error!\n");
		fflush(stdout);
		return G_SOURCE_REMOVE;	
	}
	if(!l->msg){
		printf("gui_window_logview_attach_msg: Got string with nullpointer\n");
		fflush(stdout);
		return G_SOURCE_REMOVE;	
	}
	
	if(strlen(l->msg)<2){
		printf("gui_window_logview_attach_msg: Got string with strlen %i (<2)\n",(int) strlen(l->msg));
		fflush(stdout);
		return G_SOURCE_REMOVE;	
	}
	
	//printf("processed logmsg:\n-time:%li\n-msg:%s\n",l->time, l->msg);
	//fflush(stdout);

	gui_window_logview_msg(l->time, l->msg, l->variables);

	free(l);
	return G_SOURCE_REMOVE;	
}

//removes all messages and frees memory if there is any
void gui_window_varview_free(void){
	if(logview_cnt<1) return;
	logview_cnt=0;
	GtkTreeIter iter;
	if(!logview_msg_treeview_list_store) return;
	while(gtk_tree_model_get_iter_first (GTK_TREE_MODEL(logview_msg_treeview_list_store),&iter)){
    	void* data=0;
    	gtk_tree_model_get (GTK_TREE_MODEL(logview_msg_treeview_list_store), &iter, COLLOG_PTR, &data, -1);
    	if(data){
    		free(data);
    	}
    	if(gtk_list_store_iter_is_valid (logview_msg_treeview_list_store,&iter)){
			gtk_list_store_remove(GTK_LIST_STORE(logview_msg_treeview_list_store),&iter);
		}else{
			printf("gui_window_varview_free: Iter was invalid.\n");
		}
	}
	gtk_list_store_clear(logview_msg_treeview_var_store);
}

void gui_window_logview_reload(void){
	//clear all variables
	gui_window_varview_free();
	//request data
	monitor_master_req_log();
}

void gui_window_logview(void){
	gui_window_logview_grid=gtk_grid_new();
	gtk_widget_set_vexpand (gui_window_logview_grid, TRUE);
	gui_window_logview_grid_frame_msg=gtk_frame_new("Messages");
	gtk_widget_set_vexpand (gui_window_logview_grid_frame_msg, TRUE);
	gui_window_logview_grid_frame_var=gtk_frame_new("Variables");
	gtk_widget_set_vexpand (gui_window_logview_grid_frame_var, TRUE);
	gtk_grid_attach(GTK_GRID(gui_window_logview_grid), gui_window_logview_grid_frame_msg, 0, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(gui_window_logview_grid), gui_window_logview_grid_frame_var, 1, 0, 1, 1);

	gui_window_logview_msginit();
	//test removed
	//gui_window_logview_msg(10,"Hallo",NULL);
	gui_window_logview_varinit();

	g_signal_connect (gui_window_logview_grid_frame_msg_treeview, "cursor-changed",G_CALLBACK (gui_window_msg_selected), NULL);

	//temporary reinit button beloew
	GtkWidget* button;
	button=gtk_button_new_with_label("Load Logs");
	g_signal_connect(button, "clicked",G_CALLBACK (gui_window_logview_reload), NULL);
	gtk_grid_attach(GTK_GRID(gui_window_logview_grid), button, 0, 1, 2, 1);

}
