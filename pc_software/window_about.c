/*
 * window_about.c
 *
 *  Created on: Jun 3, 2020
 *      Author: michael
 */

#include "unified_header.h"

GtkWidget* about_grid;
GtkWidget* about_grid_mcuinfo_frame;
GtkWidget* about_grid_mcuinfo_frame_grid;
GtkWidget* about_grid_pcsoftware_info;
GtkWidget* about_grid_pcsoftware_info_grid;

GtkWidget* about_grid_mcuinfo_label_manufacturer=NULL;
GtkWidget* about_grid_mcuinfo_label_device=NULL;
GtkWidget* about_grid_mcuinfo_label_softwareversion=NULL;
GtkWidget* about_grid_mcuinfo_label_uniqueid=NULL;

GtkWidget* about_grid_button_update;

char* strnmem(char *s){
	char *cpy;
	cpy=malloc(strlen(s)+1);
	strcpy(cpy,s);
	return cpy;
}


void gui_window_label(GtkWidget* l, char *str){
	if(l){
		//char *old=gtk_label_get_label(GTK_LABEL(l));
		//strcpy(old,str);
		gtk_label_set_label(GTK_LABEL(l), str);
		free(str);
	}else{
		printf("gui_window_label:we got something wrong\n");
	}
}

gboolean gui_window_label_manufacturer_updatefree(char *str){
	gui_window_label(about_grid_mcuinfo_label_manufacturer,str);
	return G_SOURCE_REMOVE;	
}
gboolean gui_window_label_device_updatefree(char *str){
	gui_window_label(about_grid_mcuinfo_label_device,str);
	return G_SOURCE_REMOVE;	
}
gboolean gui_window_label_softwareversion_updatefree(char *str){
	gui_window_label(about_grid_mcuinfo_label_softwareversion,str);
	return G_SOURCE_REMOVE;
}

gboolean gui_window_label_uniqueid_updatefree(char *str){
	gui_window_label(about_grid_mcuinfo_label_uniqueid,str);
	return G_SOURCE_REMOVE;	
}


void gui_window_about_device_update_request(void){
	monitor_master_device_information();
	monitor_master_uniqueid();
}


void gui_window_about(void){
	about_grid=gtk_grid_new();
    gtk_widget_set_vexpand (about_grid, TRUE);
	gtk_widget_set_hexpand (about_grid, TRUE);
	gtk_grid_set_column_spacing(GTK_GRID(about_grid), 20);
	gtk_grid_set_row_spacing(GTK_GRID(about_grid), 20);

	about_grid_mcuinfo_frame=gtk_frame_new ("MCU Information");
	about_grid_mcuinfo_frame_grid=gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(about_grid_mcuinfo_frame_grid), 6);
	gtk_grid_set_row_spacing(GTK_GRID(about_grid_mcuinfo_frame_grid), 6);

	//place frame to first element
    gtk_container_add(GTK_CONTAINER(about_grid_mcuinfo_frame), about_grid_mcuinfo_frame_grid);
    int v=1;
	gtk_grid_attach(GTK_GRID(about_grid),about_grid_mcuinfo_frame,1,1,1,1);
	gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),about_grid_mcuinfo_label_manufacturer=gtk_label_new(strnmem("                         Manufacturer not set")),2,v,1,1);
	gtk_label_set_xalign(GTK_LABEL(about_grid_mcuinfo_label_manufacturer), 1); //align to right
	gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),gtk_label_new("Manufacturer"),1,v++,1,1);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),about_grid_mcuinfo_label_device=gtk_label_new(strnmem("                                Device not set")),2,v,1,1);
    gtk_label_set_xalign(GTK_LABEL(about_grid_mcuinfo_label_device), 1); //align to right
    gtk_widget_set_hexpand (about_grid_mcuinfo_label_device, TRUE);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),gtk_label_new("Device"),1,v++,1,1);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),about_grid_mcuinfo_label_softwareversion=gtk_label_new(strnmem("                             Software Version not set")),2,v,1,1);
    gtk_label_set_xalign(GTK_LABEL(about_grid_mcuinfo_label_softwareversion), 1); //align to right
    gtk_widget_set_hexpand (about_grid_mcuinfo_label_softwareversion, TRUE);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),gtk_label_new("Software Version"),1,v++,1,1);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),about_grid_mcuinfo_label_uniqueid=gtk_label_new(strnmem("                                  Unique ID not set")),2,v,1,1);
    gtk_label_set_xalign(GTK_LABEL(about_grid_mcuinfo_label_uniqueid), 1); //align to right
    gtk_widget_set_hexpand (about_grid_mcuinfo_label_uniqueid, TRUE);
    gtk_grid_attach(GTK_GRID(about_grid_mcuinfo_frame_grid),gtk_label_new("Unique ID"),1,v++,1,1);
	gtk_widget_set_hexpand (about_grid_mcuinfo_frame_grid, TRUE);

	//MCU TRACER PC SOFTWARE INFORMATION
	about_grid_pcsoftware_info=gtk_frame_new ("PC Software Information");
	about_grid_pcsoftware_info_grid=gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(about_grid_pcsoftware_info_grid), 20);
	gtk_grid_set_row_spacing(GTK_GRID(about_grid_pcsoftware_info_grid), 6);
	gtk_grid_attach(GTK_GRID(about_grid),about_grid_pcsoftware_info,1,2,1,1); //left, top

    gtk_container_add(GTK_CONTAINER(about_grid_pcsoftware_info), about_grid_pcsoftware_info_grid);

    gtk_grid_attach(GTK_GRID(about_grid_pcsoftware_info_grid),gtk_label_new("Application Name"),1,1,1,1);
    GtkWidget* l;
    gtk_grid_attach(GTK_GRID(about_grid_pcsoftware_info_grid),l=gtk_label_new(MCU_TRACER_NAME),2,1,1,1);
    gtk_label_set_xalign(GTK_LABEL(l), 1); //align to right
    gtk_widget_set_hexpand (l, TRUE);
    gtk_grid_attach(GTK_GRID(about_grid_pcsoftware_info_grid),gtk_label_new("Software Version"),1,2,1,1);
    char *softwareversion=malloc(200);
    sprintf(softwareversion, "%i.%i",SOFTWARE_VERSION_MAJOR,SOFTWARE_VERSION_MINOR);
    gtk_grid_attach(GTK_GRID(about_grid_pcsoftware_info_grid),l=gtk_label_new(softwareversion),2,2,1,1);
    gtk_label_set_xalign(GTK_LABEL(l), 1); //align to right
    gtk_widget_set_hexpand (l, TRUE);
    free(softwareversion);

    gtk_grid_attach(GTK_GRID(about_grid),about_grid_button_update=gtk_button_new_with_label("Update MCU Information"), 1, 3, 1, 1);
	g_signal_connect (about_grid_button_update, "clicked",G_CALLBACK (gui_window_about_device_update_request), NULL);
}
