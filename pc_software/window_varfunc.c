/*
 * window_varfunc.c
 *
 *  Created on: Mar 11, 2022
 *      Author: michael
 */

#include "unified_header.h"

//variable view

GtkWidget     *varfunc_vartreeview_listbox=NULL;
GtkWidget     *varfunc_vartreeview_scroll=NULL;
GtkListStore  *varfunc_vartreeview_store=NULL;

struct set_variables *varfunc_widgets_new;

//Inits variable view


void gui_var_int_add(struct set_variables *mydd){
	GtkWidget *innerrowbox;
	innerrowbox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0); //orientation, spacing
	mydd->label_widget=gtk_label_new(mydd->label);
	gtk_label_set_width_chars(GTK_LABEL(mydd->label_widget),20);
	gtk_widget_set_halign (GTK_WIDGET(mydd->label_widget), GTK_ALIGN_END);
	//gtk_widget_set_hexpand (label,1);
	gtk_label_set_xalign(GTK_LABEL(mydd->label_widget),1);
	gtk_container_add(GTK_CONTAINER(innerrowbox), mydd->label_widget);

	//create new entry widget
	mydd->data_widget=gtk_entry_new();
	//deactivate, if not writable
	if((mydd->rw)>0){
		gtk_widget_set_sensitive (mydd->data_widget, FALSE);
	}

	//set callback on enter
	g_signal_connect (mydd->data_widget, "activate",  G_CALLBACK (cb_gui_var_data_changed_text), NULL);
	//set callback on esc
	//g_signal_connect (mydd->data_widget, "state-changed",  G_CALLBACK (cb_gui_var_data_noenter), NULL);

	gtk_widget_set_hexpand (mydd->data_widget,1);
	gtk_entry_set_width_chars(GTK_ENTRY(mydd->data_widget),10);

	gtk_widget_set_halign(mydd->data_widget,GTK_ALIGN_START);
	gtk_container_add(GTK_CONTAINER(innerrowbox), mydd->data_widget);
	gtk_container_add(GTK_CONTAINER(varfunc_vartreeview_listbox),innerrowbox);
}

void gui_var_bool_add(struct set_variables *mydd){
	GtkWidget *innerrowbox;
	innerrowbox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0); //orientation, spacing
	mydd->label_widget=gtk_label_new(mydd->label);
	gtk_label_set_width_chars(GTK_LABEL(mydd->label_widget),20);
	gtk_widget_set_halign (GTK_WIDGET(mydd->label_widget), GTK_ALIGN_END);
	//gtk_widget_set_hexpand (label,1);
	gtk_label_set_xalign(GTK_LABEL(mydd->label_widget),1);
	gtk_container_add(GTK_CONTAINER(innerrowbox), mydd->label_widget);

	mydd->data_widget=gtk_switch_new();

	//gtk_widget_set_size_request(mydd->data_widget,2000,200);

	//check if we are allowed to write
	if((mydd->rw)>0){
		gtk_widget_set_sensitive (mydd->data_widget, FALSE);
	}
	//g_signal_connect (mydd->data_widget, "state-set",  G_CALLBACK (cb_gui_var_data_switch_pressed), mydd->data_widget);
	g_signal_connect (mydd->data_widget, "notify::active",  G_CALLBACK (cb_gui_var_data_switch_pressed), mydd->data_widget);

	//printf("created switch %i\n",mydd->data_widget);

	gtk_widget_set_halign(mydd->data_widget,GTK_ALIGN_START);
	gtk_widget_set_hexpand (mydd->data_widget,1);
	//add switch to row
	//gtk_box_pack_start(GTK_BOX(innerrowbox),innerswitch,1,1,0);
	gtk_container_add(GTK_CONTAINER(innerrowbox), mydd->data_widget);
	//add element to grid
	gtk_widget_set_halign(innerrowbox,GTK_ALIGN_FILL);
	gtk_container_add(GTK_CONTAINER(varfunc_vartreeview_listbox),innerrowbox);
}


void gui_var_init (void){
	//create a new listbox
	varfunc_vartreeview_listbox=gtk_list_box_new();
	GtkWidget *listbox;
	GtkWidget *innerrowbox;
	//create listbox
	listbox=gtk_list_box_row_new();

	//gui_var_bool_add("dummy entry");

	//make window scrollable (if required)
	varfunc_vartreeview_scroll=gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (varfunc_vartreeview_scroll),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(varfunc_vartreeview_scroll),varfunc_vartreeview_listbox);
	//gtk_container_add(GTK_CONTAINER(varfunc_vartreeview_container), varfunc_vartreeview_scroll);

}

gboolean gui_var_data_init(struct set_variables *mydd){
	if(varfunc_widgets_new) free(varfunc_widgets_new);
	varfunc_widgets_new=mydd;

	//destroy old listbox if existent
	if(varfunc_vartreeview_listbox){
		//must be removed before destoryed, otherwise not working
		gtk_container_remove(GTK_CONTAINER(varfunc_vartreeview_scroll),varfunc_vartreeview_listbox);
		//there comes an error, if belows line is used.
		//gtk_widget_destroy(varfunc_vartreeview_listbox);
	}
	//and create a new one
	varfunc_vartreeview_listbox=gtk_list_box_new();

	//struct set_variables *mydd;
    if(mydd==NULL){
		printf("data acquisition failed\n");
		//return;
	}

    gint loop=0;
    while(mydd[loop].type){
		if(mydd[loop].type==1){
			gui_var_int_add(&mydd[loop]);
		}else if(mydd[loop].type==2){
			gui_var_int_add(&mydd[loop]);
		}else if(mydd[loop].type ==3){
			gui_var_bool_add(&mydd[loop]);
		}
		loop=loop+1;
	}

    gtk_container_add(GTK_CONTAINER(varfunc_vartreeview_scroll),varfunc_vartreeview_listbox);
	gtk_widget_show_all(varfunc_vartreeview_scroll);
	return G_SOURCE_REMOVE;
}


gboolean gui_var_data_update(uint32_t *datastream){
	//Updates the values in the specific input types
	struct set_variables *vars_widgets;
	vars_widgets=varfunc_widgets_new; //local working copy to easy iterating
    gint loop=0;

    while(vars_widgets[loop].type){
		vars_widgets[loop].data_l=datastream[loop];
		set_single_var_t update;
		update.addr=loop;
		update.val=vars_widgets[loop].data_l;
		gui_var_data_single_update(&update,0); //do not force update

		loop=loop+1;
	}
	//free(datastream);
	return G_SOURCE_REMOVE;
}
/*
 * old code:
char str[30];
if(vars_widgets[loop].type==1){
	snprintf(str,30, "%i", vars_widgets[loop].data_l);
	//do not overwrite, when user is editing
	if(gtk_widget_is_focus (GTK_WIDGET(vars_widgets[loop].data_widget))==FALSE){
		gtk_entry_set_text(GTK_ENTRY(vars_widgets[loop].data_widget),str);
	}
}else if(vars_widgets[loop].type==2){
	snprintf(str,30, "%lf", vars_widgets[loop].data_f);
	//do not overwrite, when user is editing
	if(gtk_widget_is_focus (GTK_WIDGET(vars_widgets[loop].data_widget))==FALSE){
		gtk_entry_set_text(GTK_ENTRY(vars_widgets[loop].data_widget),str);
	}
}else if(vars_widgets[loop].type==3){
	//mydd[loop].data_widget=gtk_check_button_new ();
	//gboolean active=(mydd[0].data_l!=0);
	gboolean active=TRUE;
	if(vars_widgets[loop].data_l==0){
		active=FALSE;
	}
	//g_signal_handlers_block_by_func(vars_widgets[loop].data_widget,callback_set_variables_changed,NULL);
	gtk_switch_set_active(GTK_SWITCH(vars_widgets[loop].data_widget),active);
	gtk_switch_set_state(GTK_SWITCH(vars_widgets[loop].data_widget),active);
	//g_signal_handlers_unblock_by_func(vars_widgets[loop].data_widget,callback_set_variables_changed,NULL);
}else if(vars_widgets[loop].type==4){
	snprintf(str,30, "%u", vars_widgets[loop].data_l);
	//do not overwrite, when user is editing
	if(gtk_widget_is_focus (GTK_WIDGET(vars_widgets[loop].data_widget))==FALSE){
		gtk_entry_set_text(GTK_ENTRY(vars_widgets[loop].data_widget),str);
	}
}
*/

void gui_var_data_update_widget_disp(uint32_t updateid, int forceupdate){
	struct set_variables *mydd;
	mydd=varfunc_widgets_new;
	char str[30];
	if(mydd[updateid].type==1){
		snprintf(str, 30,"%i", mydd[updateid].data_l);
		if(gtk_widget_is_focus (GTK_WIDGET(mydd[updateid].data_widget))==FALSE
				|| forceupdate>0){
			gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
		}
	}else if(mydd[updateid].type==2){
		snprintf(str, 30, "%lf", mydd[updateid].data_f);
		if(gtk_widget_is_focus (GTK_WIDGET(mydd[updateid].data_widget))==FALSE
				|| forceupdate>0){
			gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
		}
	}else if(mydd[updateid].type==3){
		gboolean active=TRUE;
		if(mydd[updateid].data_l==0){
			active=FALSE;
		}
		if(gtk_widget_is_focus (GTK_WIDGET(mydd[updateid].data_widget))==FALSE
				|| forceupdate>0){
			gtk_switch_set_active(GTK_SWITCH(mydd[updateid].data_widget),active);
			gtk_switch_set_state(GTK_SWITCH(mydd[updateid].data_widget),active);
		}

	}else if(mydd[updateid].type==4){
		if(gtk_widget_is_focus (GTK_WIDGET(mydd[updateid].data_widget))==FALSE
				|| forceupdate>0){
			snprintf(str, 30, "%u", mydd[updateid].data_l);
			gtk_entry_set_text(GTK_ENTRY(mydd[updateid].data_widget),str);
		}
	}
}

gboolean gui_var_check_idforexistence(int checkid){
	struct set_variables *mydd;
	mydd=varfunc_widgets_new;
	int loop=0;
	int foundinsertid=0;
	while(mydd[loop].type){
		if(loop==checkid){
			return 1;
		}
		loop++;
	}
	return 0;
}

gboolean gui_var_data_single_update(set_single_var_t *data, int forceupdate){
	int updateid=data->addr;
	struct set_variables *mydd;
	mydd=varfunc_widgets_new;

	if(gui_var_check_idforexistence(updateid)==0){
		printf("WARNING: id %i during update not existend.\n",updateid);
		return G_SOURCE_REMOVE;
	}

	mydd[updateid].data_l=data->val;
	//printf("addr:%i|data:%i\n",data->addr,data->val);
	gui_var_data_update_widget_disp(updateid,forceupdate);
	return G_SOURCE_REMOVE;
}


//changed variables
gboolean cb_gui_var_data_changed_text(GtkWidget *widget, gpointer   data ){
	//printf("We had a text enter.\n");
	//fflush(stdout);
	struct set_variables *mydd;
	gint loop=0;
    mydd=varfunc_widgets_new;
    while(mydd[loop].type){
		//printf("loop %i (%i==%i)",loop,set_variables_data[loop].data_widget,widget);
		if(mydd[loop].data_widget==widget){
			//printf("We received: %s\n",gtk_entry_get_text(GTK_ENTRY(mydd[loop].data_widget)));
			if(mydd[loop].type==1){
				mydd[loop].data_l=atoi(gtk_entry_get_text(GTK_ENTRY(mydd[loop].data_widget)));
				//printf("entered: %i\n",set_variables_data[loop].data_l);
				monitor_master_write_var(loop,mydd[loop].data_l);
			}else if(mydd[loop].type==2){
				mydd[loop].data_f=atof(gtk_entry_get_text(GTK_ENTRY(mydd[loop].data_widget)));
				//printf("writing %lf\n",mydd[loop].data_f);
				//must be transfered as a int, other wise this fails.
				monitor_master_write_var(loop,mydd[loop].data_l);
			}else if(mydd[loop].type==4){
				mydd[loop].data_l=strtoul(gtk_entry_get_text(GTK_ENTRY(mydd[loop].data_widget)),NULL,0);
				//printf("entered: %u\n",set_variables_data[loop].data_l);
				monitor_master_write_var(loop,mydd[loop].data_l);
			}else{
				printf("Unknown filetype %i in __FUNCTION__", mydd[loop].type);
			}
			//transfer data to mcu

			return G_SOURCE_REMOVE;
		}
		loop=loop+1;
	}
	printf("No widget for the text enter found.\n");
	fflush(stdout);
}


//If an element is entered or left, it's value its set to the orignal value.
gboolean cb_gui_var_data_noenter(GtkWidget *widget, gpointer   usrdata ){
	//printf("noenter condition\n");
    gint loop=0;
    while(varfunc_widgets_new[loop].type){
		if(varfunc_widgets_new[loop].data_widget==widget){
			gui_var_data_update_widget_disp(loop,0);
			return G_SOURCE_REMOVE;
		}
		loop=loop+1;
	}
    printf("Warning: Widget in noenter condition not found.\n");
    fflush(stdout);
    return G_SOURCE_REMOVE;
}

//if a switch is pressed, this callback is issued
gboolean cb_gui_var_data_switch_pressed(GtkWidget *widget, gpointer   usrdata ){
	//printf("switch pressed.\n");
	//fflush(stdout);
    gint loop=0;
    while(varfunc_widgets_new[loop].type){
    	//printf("loop %i: %i=%i\n",loop,varfunc_widgets_new[loop].data_widget,widget);
		if(varfunc_widgets_new[loop].data_widget==widget){
			//printf("%s was changed...\n",set_variables_data[loop].label);
			//Set string to orginal
			char str[30];
			if(varfunc_widgets_new[loop].type==3){
				//Its a switch
				gboolean status;
				status=gtk_switch_get_active(GTK_SWITCH(varfunc_widgets_new[loop].data_widget));
				int32_t intstatus=1;
				if(status==0){
					intstatus=0;
				}
				varfunc_widgets_new[loop].data_l=intstatus;
				//printf("Updated switch %s with value %i %i %i\n",varfunc_widgets_new[loop].label,varfunc_widgets_new[loop].data_l,status,intstatus);
				//fflush(stdout);
				monitor_master_write_var(loop,varfunc_widgets_new[loop].data_l);
			}else{
				printf("The emitted signal %s is not a switch in __FUNCTION__", varfunc_widgets_new[loop].label);
			}
			return G_SOURCE_REMOVE;
		}
		loop=loop+1;
	}
    printf("Could not updated switch, widget not found.\n");
    fflush(stdout);
    return G_SOURCE_REMOVE;
}


