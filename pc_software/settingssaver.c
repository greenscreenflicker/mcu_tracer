#include "unified_header.h"

#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */

#else											/* Windows */	
	#include <shlobj.h> 						//for getting PATH of home directory
#endif

GKeyFile *key_file;
settingsaver_connection_t *settings_connection;


void settingsaver_get_home_directory(char* buf){
	#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
		struct passwd *pw = getpwuid(getuid());
		const char *homedir = pw->pw_dir;
		strcpy(buf,homedir);
	#else 											/* Windows */
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, buf))) {
			
		} else {
			printf("SettingsSaver: couldnot get home path");
		}
	#endif
}

void settingssaver_filename(char* filename){
	char homedir[1002];
	settingsaver_get_home_directory(&homedir[0]);
	homedir[1001]='0';
	snprintf(filename,400,"%s/mcutracer.ini",&homedir[0]);
}

void settingssaver_load(void){
	//printf("SettingsSaver Load: Loading settings\n");
	gboolean loadstat;
	GError *error=NULL;
	settings_connection=calloc(sizeof(settingsaver_connection_t),1);
	
	char filename[400];
	settingssaver_filename(&filename[0]);
	//printf("SettingsSaver Load: load from '%s'\n",filename);
	
	key_file=g_key_file_new ();
	loadstat=g_key_file_load_from_file (key_file,
                           filename,
						 G_KEY_FILE_KEEP_COMMENTS | 
						  G_KEY_FILE_KEEP_TRANSLATIONS,
                           &error);
                         
    if(!loadstat){ 
		printf("SettingsSaver Load:%s\n", error->message);
	}else{
		//printf("we can load\n");
		//we could load file
		settings_connection->comport = g_key_file_get_integer(key_file,
                                           "connection",
                                           "comport",
                                           &error);
		settings_connection->baud = g_key_file_get_integer(key_file,
                                           "connection",
                                           "baud",
                                           &error);
										   
	}
	fflush(stdout);
}

void settingssaver_store(void){
	GError *error=NULL;
	char filename[400];
	settingssaver_filename(&filename[0]);
	
	//printf("SettingsSaver Store: Storing settings\n");
	//printf("SettingsSaver Store: save to '%s'\n",filename);
	if(!key_file){
		g_debug("SettingsSaver Store: Key file cannot be loaded");
		return;
	}
	g_key_file_set_integer (key_file,
                        "connection",
                        "comport",
                        settings_connection->comport);
	g_key_file_set_integer (key_file,
                        "connection",
                        "baud",
                        settings_connection->baud);
	
	#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
		gboolean storestat;
		storestat=g_key_file_save_to_file (key_file,
								 filename,
								 &error);
							 
		if(!storestat){ 
			printf("SettingsSaver Store:%s", error->message);
		}

	#else 											/* Windows */
		gsize len;
		FILE *fp;
		fp = fopen( filename, "w");
		if (fp == NULL){
			printf("SettingsSaver Store: File cannot be opened");
		}
		const void *a = g_key_file_to_data(key_file, &len, &error);
		if (error != NULL){
			printf("SettingsSaver Store:%s", error->message);
		}
		fwrite(a, 1, len, fp);
		fclose(fp);
	#endif
	
}
