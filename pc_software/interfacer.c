#include "unified_header.h"

int _comport=-1;
int _run_rec_thread;

GMutex *lock_rs232; 
GMutex *lock_paritycheckbit;
GMutex *lock_rec_thread;




uint16_t xor_checksum(uint16_t *data, int length){
	//
	uint16_t checksum=0;
	int n;
	for(n=0;n<length;n++){
		checksum=checksum^(*data);
		data++;
		//printf("checksum is:%X",checksum);
	}
	return checksum;
}

unsigned char xor_checksum2(unsigned char *data, int length){
	//
	unsigned char checksum=0;
	int n;
	for(n=0;n<length;n++){
		checksum=checksum^(*data);
		data++;
		//printf("checksum is:%X",checksum);
	}
	return checksum;
}


int monitor_master_find_startbyte(unsigned char* buf, int len){
	if(len<0){
		printf("monitor_master_find_startbyte length %i\n",len);
		return -1;
	}
	int i;
	for(i=0; i < len; i++)
	{
		if(buf[i]==MCU_TRACER_STARTBYTE){
			//printf("found startbyte at %i\n",i);
			return i;
		}
	}
	return -1; //noting found
}

int monitor_master_find_end(uint8_t *data, int length, int skip){
    uint8_t checksum=0;
    int n;
    int pos=0;
    if(length<3) return -1;
    for(n=0;n<length;n++){
        checksum=checksum^(*data);
        data++;
        pos++;
        if(!checksum){
            if(!skip){
                return pos;
            }
            skip--;
        }

        //printf("checksum is:%X",checksum);
    }
    //we failed to find an end
    return -1;
}

//returns position on sucess, -1 on fault
int monitor_master_find_end_startbyte(uint8_t *data, int length, int skip){
    uint8_t checksum=0;
    int pos;
    if(length<3) return -1;
    for(pos=0;pos<length;){
        checksum=checksum^(*data);
        data++;
        pos++;
        if(checksum) continue; //checksum is non-zero, so there can't be a match
        if((pos+1)==length){
            //we are the the end of the string
        	//when we shouldn't skip something, it can be at the end of tit
        	if(!skip) return length;
        	//we should skip something, but there's nothing left, that's an error
        	return -1;
        }
        if((*data)==MCU_TRACER_STARTBYTE){
            //the next byte is a startbyte, so there is a high propability the string will be good
            //if the next byte woudn't be a startbyte, we can be sure, that it's not valid
            if(pos<3) continue;
            if(!skip){
            	return pos;
            }else{
            	skip--;
            	continue;
            }
        }
        if(!skip){
            return pos;
        }
        skip--;
    }
    //we failed to find an end
    return -1;
}

    
int monitor_master_check_parity(unsigned char* buf, int len){
	int startbyte=monitor_master_find_startbyte(buf,len);
	if(startbyte<0) return 0; //startbyte not found
	unsigned char checksum=xor_checksum2(buf+startbyte,len-startbyte);
	if(checksum==0){
		//printf("integrity: no error detected");
		return 1;
	}else{
		//printf("integrity: error detected");
		return 0;
	}
}



void print_hex_data(unsigned char* buf, int len){
	int i;
	printf("[%i]:",len);
	for(i=0; i < len; i++){
		printf("%x(%c)-",buf[i],buf[i]);
	}
	printf("\n");
	fflush(stdout);
 }
void print_hex_data_simple(unsigned char* buf, int len){
	int i;
	printf("[%i]:",len);
	for(i=0; i < len; i++){
		printf("%x-",buf[i]);
	}
	printf("\n");
	fflush(stdout);
 }





///////////////////////////////////////////////////////
//interfacer for threads
int monitor_parity_is_working;
int recieve_debug_window_active=0;
int monitor_rs232_rec_terminate=0;
int monitor_rs232_req_terminate=2;
pthread_mutex_t plock_rs232= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t plock_rs232_parity_check= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t plock_rs232_terminate= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t plock_rs232_req= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t plock_rec_debug_window= PTHREAD_MUTEX_INITIALIZER;
pthread_t th_rec, th_req;

//sends data to the mcu, run from gtk thread
int monitor_test_connection(int number, int baud){
	 int i, n,
	  cport_nr=number,        
	  bdrate=baud;      

	unsigned char buf[4096];
	#if PRINT_RS232_DATA==1
	printf("up (com:%i b:%i)\n",cport_nr,bdrate);
	#endif
	char mode[]={'8','N','1',0};


	if(RS232_OpenComport(cport_nr, bdrate, mode)){
		printf("Can not open comport\n");
		return(-1);
	}
	//Save comport number
	_comport=number;
      
   
	pthread_t tr, tw;
	//casting is needed to avoid warning, though not beautiful.
	 pthread_create(&th_rec,NULL,(void * (*)(void *))monitor_recieve_thread,NULL);
   
	char connection_str[]={MCU_TRACER_STARTBYTE,0,MCU_TRACER_STARTBYTE};
	int parityok=0;
	
	i=1000;
	while((i--)>0){
		if((i%100)==0){
			pthread_mutex_lock(&plock_rs232);
			RS232_SendBuf(_comport,connection_str,3);
			pthread_mutex_unlock(&plock_rs232);
		}
		/*
		g_mutex_lock (lock_rs232);
		RS232_SendBuf(_comport,connection_str,3);
		g_mutex_unlock (lock_rs232);
		
		g_mutex_lock (lock_paritycheckbit);
		parityok=monitor_parity_is_working;
		g_mutex_unlock (lock_paritycheckbit);
		*/
		usleep(1000);
		pthread_mutex_lock(&plock_rs232_parity_check);
		parityok=monitor_parity_is_working;
		pthread_mutex_unlock(&plock_rs232_parity_check);
		if(parityok==1) return 1; //report our success
	}
	
	//monitor_recieve_thread_terminate();
	monitor_recieve_terminate();
	fflush(stdout);
	RS232_CloseComport(_comport);
	_comport=-1;
	return(-2);
}

int monitor_master_send(unsigned char *data, int len){
	unsigned char *buf=malloc(len+2);

	
	buf[0]=MCU_TRACER_STARTBYTE;
	memcpy(&buf[1],data,len);
	buf[1+len]=xor_checksum2(data,len)^MCU_TRACER_STARTBYTE;
	#if PRINT_RS232_DATA==1
	printf("Send:");
	print_hex_data(&buf[0],len+2);
	printf("\n");
	#endif
	//printf("\n");
	pthread_mutex_lock(&plock_rs232);
	int ret=RS232_SendBuf(_comport,buf,len+2);
	pthread_mutex_unlock(&plock_rs232);
	free(buf);
	return ret;
}

void monitor_master_req_init(void){
	//requests init data;
	unsigned char data[]={1};
	monitor_master_send(data,1);
}

void monitor_master_req_data(void){
	//requests data stream of all data;
	unsigned char data[]={2};
	monitor_master_send(data,1);
}

void monitor_master_req_log(void){
	//requests data stream of all data;
	unsigned char data[]={ORDER_FD_LOG};
	monitor_master_send(data,1);
}


void monitor_master_write_var(uint16_t num, int32_t val){
	//writes value to the mcu
	unsigned char data[7]={3,1,0,0,0,0,0};
	data[1]=(unsigned char)(num>>8);
	data[2]=(unsigned char)(num);
	data[3]=(unsigned char)(val>>8*3);
	data[4]=(unsigned char)(val>>8*2);
	data[5]=(unsigned char)(val>>8*1);
	data[6]=(unsigned char)(val>>8*0);
	monitor_master_send(data,7);
}

void monitor_master_func_data(void){
	unsigned char data[]={0x08};
	monitor_master_send(data,1);
}


void monitor_master_func_exec(uint8_t id){
	unsigned char data[2]={0x09,id};
	monitor_master_send(data,2);
}

//requests supplier information
void monitor_master_device_information(void){
	unsigned char data[]={ORDER_F0_SUPPLIER_INFO};
	monitor_master_send(data,1);
	//printf("Supplier Info requested.\n");
}

//requests unique id
void monitor_master_uniqueid(void){
	unsigned char data[]={ORDER_F1_UNIQUE_ID};
	monitor_master_send(data,1);
}


void monitor_master_emergency(void){
	//requests data stream of all data;
	unsigned char data[]={0xFF};
	monitor_master_send(data,1);
}


void monitor_master_req_frequently(void){
	//
	int request_terminate=0;
	uint32_t device_data_update=0;

	while(request_terminate==0){
		usleep(100000);
		monitor_master_req_data();
		pthread_mutex_lock(&plock_rs232_req);
		request_terminate=monitor_rs232_req_terminate;
		pthread_mutex_unlock(&plock_rs232_req);
	}
	pthread_mutex_lock(&plock_rs232_req);
	monitor_rs232_req_terminate=2;
	pthread_mutex_unlock(&plock_rs232_req);
	//printf("reqthread terminated\n");
}

void monitor_master_req_frequently_enable(void){
	int request_terminate;
	pthread_mutex_lock(&plock_rs232_req);
	request_terminate=monitor_rs232_req_terminate;
	pthread_mutex_unlock(&plock_rs232_req);
	if(request_terminate==2){
		monitor_rs232_req_terminate=0;
		pthread_create(&th_req,NULL,(void * (*)(void *))monitor_master_req_frequently,NULL);
		//printf("reqthread created\n");
	}
}

void monitor_master_req_frequently_disable(void){
	pthread_mutex_lock(&plock_rs232_req);
	monitor_rs232_req_terminate=1;
	pthread_mutex_unlock(&plock_rs232_req);
}


void monitor_recieve_terminate(void){
	pthread_mutex_lock(&plock_rs232_terminate);
	monitor_rs232_rec_terminate=1;
	pthread_mutex_unlock(&plock_rs232_terminate);
}

void monitor_master_allow_decoding(void){
	pthread_mutex_lock(&plock_rec_debug_window);
	recieve_debug_window_active=1;
	pthread_mutex_unlock(&plock_rec_debug_window);
}

unsigned char workingbuffer[WORKING_BUFFER_SIZE];
unsigned char iterater[WORKING_BUFFER_SIZE];
int portionierer_current_buffer_pos=0;

//returns 1 on success
//-1: more data to come
//-2: Data can be destroyed
int portionierer_process(unsigned char *data, int len){
	//printf("i would process this:");
	//print_hex_data(data, len);
	if(len<3){
		printf("Portionierer len<3: Here must be something wrong\n");
		return -2;
	}
	int parity=xor_checksum2(&data[0],len);
	if(parity){
		//parity non-equal one means error
		printf("DECODE:");
		print_hex_data_simple(&data[0],len);
		printf("-Paritity %i\n",parity);
	}else{
		return monitor_master_decode_string(data,len);
	}
}


int monitor_master_check_parity_dump(unsigned char* buf, int len){
	unsigned char checksum=xor_checksum2(buf,len);
	if(!checksum){
		//printf("integrity: no error detected");
		return 1;
	}else{
		//printf("integrity: 0x%x expected but is 0x%x\n",checksum,buf[len-1]);
		return 0;
	}
}

int monitor_master_check_parity_following_bit(unsigned char* buf, int len){
	if(buf[len]==0xA5){
		return monitor_master_check_parity_dump(buf,len);
	}else{
		return 0;
	}
}
void portionierer2_move_mem(unsigned char *start,int pos){
	memcpy(&iterater[0],&workingbuffer[pos],portionierer_current_buffer_pos-pos);
	memcpy(&workingbuffer[0],&iterater[0],portionierer_current_buffer_pos-pos);
	portionierer_current_buffer_pos=portionierer_current_buffer_pos-pos;
}

void monitor_master_portionierer2(unsigned char *data, int len){
	//printf("Buf");
	//print_hex_data_simple(&data[0],len);
		
	//Copy data in our area, so we can iterate over it
	memcpy(&workingbuffer[portionierer_current_buffer_pos],data,len);
	portionierer_current_buffer_pos=portionierer_current_buffer_pos+len;
	int startpos,endpos,endpos_ret;
	
	int countstartbytes;

	//printf("\n\nbuffer:");
	//print_hex_data(&workingbuffer[0],portionierer_current_buffer_pos);	
	PORTIONERER_CONTINUE_PROCESS:
	countstartbytes=0;
	//looking for startbyte
	startpos=monitor_master_find_startbyte(&workingbuffer[0],portionierer_current_buffer_pos);
	if(startpos==-1){
		//no data was found, we delete all
		//printf("Portionier: We only got garbage-deleting it\n");
		portionierer_current_buffer_pos=0;
		return;
	}
	

	endpos=startpos+1;
	while(1){
		endpos_ret=monitor_master_find_startbyte(&workingbuffer[endpos],portionierer_current_buffer_pos-endpos);
		if(endpos_ret==-1){
			//we have no data more to come
			int ret;
			ret=monitor_master_check_parity_dump(&workingbuffer[startpos],portionierer_current_buffer_pos-startpos);
			if(ret){
				//we can use result;
				///printf("Portionierer: This last part is valid.\n");
				int could_proccess;
				could_proccess=portionierer_process(&workingbuffer[startpos],portionierer_current_buffer_pos-startpos);
				if(could_proccess){
					//free buffer
					portionierer_current_buffer_pos=0;
					//printf("processing successfull\n");
				}else{
					//printf("processing not successfull\n");
				}
				return;
			}else{
				//result is unusable, maybe there's more to come;
				//wait for a later call to come more
				//printf("Portionier: Nothing more to process\n");
				return;
			}
		}else{
			endpos_ret=endpos+endpos_ret;
			//printf("Portionier: We have a possible datastring inbetween %i-%i\nDatastring:",startpos,endpos_ret);
			//print_hex_data(&workingbuffer[startpos],endpos_ret-startpos);
			int ret;
			ret=monitor_master_check_parity_following_bit(&workingbuffer[startpos],endpos_ret-startpos);
			if(ret){
				//printf("Portionier: String valid\n");
				while(1){
					ret=portionierer_process(&workingbuffer[startpos],endpos_ret-startpos);
					if(ret){
						//printf("processing successfull2\n");
						break;
					}
					printf("processing NOT successfull2 - we should try a longer string\n");
					int len=endpos_ret-startpos;
					int charsleft=endpos_ret-startpos-len;
					if(charsleft<1){
						printf("no more chars left\n");
						return;
					}
					int nextstartbyte=monitor_master_find_startbyte(&workingbuffer[startpos+len],endpos_ret-startpos-len);
					if(nextstartbyte>-1){
						printf("next time try decoding this:\n");
						print_hex_data(&workingbuffer[startpos],endpos_ret-startpos+nextstartbyte);
					}
					break;
				}
				//remove string from data
				portionierer2_move_mem(&workingbuffer[endpos_ret],endpos_ret);
				//printf("->new data buffer looks like this\nworker:");
				//print_hex_data(&workingbuffer[0],portionierer_current_buffer_pos);
				//check if we have more data to process
				goto PORTIONERER_CONTINUE_PROCESS;
				//return;				
			}else{
				//printf("Portionier: Looking for next string\n");
				endpos=endpos_ret+1;
				
				if(countstartbytes>5){
					//printf("Portionier: we possibly have wrong integrity bit\n");
					//lets find second start byte
					startpos=monitor_master_find_startbyte(&workingbuffer[0],portionierer_current_buffer_pos);
					startpos=monitor_master_find_startbyte(&workingbuffer[startpos+1],portionierer_current_buffer_pos-startpos)+startpos+1;
					portionierer2_move_mem(&workingbuffer[startpos],startpos);
					//printf("(%i)new data buffer looks like this\ncrop:",startpos);
					//print_hex_data(&workingbuffer[0],portionierer_current_buffer_pos);
					//check if we have more data to process
					goto PORTIONERER_CONTINUE_PROCESS;					
				}
				countstartbytes=countstartbytes+1;
			}
		}
	}
	
}

void mcu_tracer_rec_submit(uint8_t* buf, uint32_t len){
	//Copy data in our area, so we can iterate over it
	uint32_t remaining_size=WORKING_BUFFER_SIZE-portionierer_current_buffer_pos-1;
	//printf("Buf");
	//print_hex_data_simple(&buf[0],len);
	if(remaining_size>(portionierer_current_buffer_pos+len)){
		memcpy(&workingbuffer[portionierer_current_buffer_pos],buf,len);
		portionierer_current_buffer_pos=portionierer_current_buffer_pos+len;
	}else{
        memcpy(&workingbuffer[portionierer_current_buffer_pos],buf,remaining_size);
        portionierer_current_buffer_pos=portionierer_current_buffer_pos+remaining_size;
	}
	//printf("Totalbuf:");
	//print_hex_data_simple(&workingbuffer[0],portionierer_current_buffer_pos);
}

//delicate function, handle with respect
//laststartbyte must be set by every continue if variables should be saved
void mcu_tracer_rec_portionierer_process3(unsigned char *data, int len){
	//Copy data in our area, so we can iterate over it
	mcu_tracer_rec_submit(data,len);
	
	int startpos_abs,endpos_abs;


	//nothing to do, return (we need at least 3 bytes, so we have a valid message to process
	if(portionierer_current_buffer_pos<3){
	    //we have too little data (<3 bytes) so processing it doesn't make sense.
	    return;
	}

	startpos_abs=0;
	int skip_find_end=0; //indicates how many start bytes should be skipped in the end search
	int laststartbyte=-1; //-1 = no save
	while(1){
		//printf("\n");
	    //looking for startbyte - function accepts negative length and returns than -1, so the remaining buffer has not to be checked
	    int startpos_rel=monitor_master_find_startbyte(&workingbuffer[startpos_abs],portionierer_current_buffer_pos-startpos_abs);
	    if(startpos_rel==-1){
	        //no startbyte was found, we delete all
	        if(laststartbyte==-1){
	            portionierer_current_buffer_pos=0;
	            //printf("buffer was cleared, no startbyte\n");
	        }else{
	            int len=portionierer_current_buffer_pos-laststartbyte;
	            if(len>WORKING_PREVENTIVE_FLUSH){
	                //recieved string is too large, we need to reset
	                portionierer_current_buffer_pos=0;
	                printf("buffer was cleared, string too large. Potential Portionierer 3 Error\n");
	            }else{
	                //copy so that the startbyte is the first in the mem
					//printf("bufferreduce-before:\n");
					//print_hex_data_simple(&workingbuffer[0],portionierer_current_buffer_pos);
					memcpy(&workingbuffer[0],&workingbuffer[laststartbyte],len);
                    portionierer_current_buffer_pos=len;
                    //printf("bufferreduce-after:\n");
					//print_hex_data_simple(&workingbuffer[0],portionierer_current_buffer_pos);
	            }
	        }
	        //printf("laststartbyte:%i",laststartbyte);
	        return;
	    }

	    startpos_abs=startpos_abs+startpos_rel;
	    int endpos_rel;
	    
	    //optimisation implemted: It only finds now zero checksums with a startbyte following (or it's the last element)
	    int decodelen=portionierer_current_buffer_pos-startpos_abs;
	    endpos_rel=monitor_master_find_end_startbyte(&workingbuffer[startpos_abs],portionierer_current_buffer_pos-startpos_abs,skip_find_end);
	    if(skip_find_end>100){
	    	printf("skip_find_end unusually high. potential error: skip_find_end=%i-endpos_rel=%i",skip_find_end,endpos_rel);
	    }
	    if(endpos_rel==-1){
	        //we increment the starting position by one, to hope to find a second start byte, where it makes more sense
	        laststartbyte=startpos_abs;
	        startpos_abs++;
	        skip_find_end=0;
	        continue;
	    }
	    endpos_abs=endpos_rel+startpos_abs;
        //the string is the last one, so we can decode it
	    int len=endpos_abs-startpos_abs;
	    
		
        int ret2=portionierer_process(&workingbuffer[startpos_abs],len);
        //if(ret2<0){
			//printf("Buf:\n");
			//print_hex_data_simple(&workingbuffer[0], portionierer_current_buffer_pos);
			//printf("proccess:\n");
			//print_hex_data_simple(&workingbuffer[startpos_abs],len);
			//int stop=monitor_master_find_end_startbyte(&workingbuffer[startpos_abs],portionierer_current_buffer_pos-startpos_abs,skip_find_end);
			//printf("Startpos %i-Endpos: %i=Len %i - Decodelen %i - ChecksumProcess %i -Function %i - Startbyte func %i Stopbyte: %i\n",startpos_abs,endpos_abs,len,decodelen,xor_checksum2(&workingbuffer[startpos_abs],len),ret2,stop);
		//}
		
        if(ret2==-1){
			//there is more data to come (-1 returned)
        	//printf("more data to come\n");
			skip_find_end++;
			//printf("skip_find_end=%i",skip_find_end);
			laststartbyte=startpos_abs;
			//printf("Buf:\n");
			//print_hex_data_simple(&workingbuffer[0], portionierer_current_buffer_pos);
			continue;
        }else{
            //data can be destroyed or has been used
            startpos_abs++;
            skip_find_end=0;
            laststartbyte=-1;
            continue;
        }
        return;
	}
}


void * monitor_recieve_thread(void){
	//recieves data;
	unsigned char buf[4096];
	int parityok=0;
	int terminate=0;
	pthread_mutex_lock(&plock_rs232_terminate);
	monitor_rs232_rec_terminate=0;
	pthread_mutex_unlock(&plock_rs232_terminate);
	while(terminate==0){
		pthread_mutex_lock(&plock_rs232);
		int n = RS232_PollComport(_comport, buf, 4095);
		pthread_mutex_unlock(&plock_rs232);
		if(n > 0){
			buf[n] = 0; 
			#if PRINT_RS232_DATA==1
			printf("recieved:");
			print_hex_data(&buf[0],n);
			#endif
			
			int may_we_decode;
			pthread_mutex_lock(&plock_rec_debug_window);
			may_we_decode=recieve_debug_window_active;
			pthread_mutex_unlock(&plock_rec_debug_window);
			if(may_we_decode==1){
				//
				//monitor_master_decode_string(&buf[0],n);
				//monitor_master_portionierer2(&buf[0],n);
				mcu_tracer_rec_portionierer_process3(&buf[0],n);
				
			}else{
				parityok=monitor_master_check_parity(&buf[0],n);
				pthread_mutex_lock(&plock_rs232_parity_check);
				monitor_parity_is_working=parityok;
				pthread_mutex_unlock(&plock_rs232_parity_check);
			}
			
		}
		usleep(1000);
		
		//check if we're allowed to decode our recieved string

		
		
		pthread_mutex_lock(&plock_rs232_terminate);
		terminate=monitor_rs232_rec_terminate;
		pthread_mutex_unlock(&plock_rs232_terminate);
	}        
	

	//printf("rec thread terminated\n");
}

//returns -2 if string is crap
//-1 if string needs to be longer
// 1 on success
int monitor_master_decode_string(unsigned char* buf, int len){
	static int numofelements = 0;
	int startbyte=monitor_master_find_startbyte(buf, len);
	if(startbyte!=0){
		//Error
		printf("startbyte was not found at the right place. not a legic messsage. Ignoring.\n");
		return -2; //data is crap
	}
	if(len<3) return -1;
	int order=buf[startbyte+1];
	if(order==ORDER_01_INIT){
		//check for validity
		//check last elements (1 terminator, 0 indicator for terminating element, 0 (readwrite)
		char terminator_check[4] = {1,0,0,1};
		if(memcmp(&terminator_check[0],&buf[len-5],4)!=0 || len<6){
			return -1; //more data to come
			printf("Order1: Termination string incomplete");
		}
		struct set_variables *mydd;
		mydd=calloc(sizeof(set_var_t),len/3); //generously overestimate number of elements we will need
		int decodepos=startbyte+2;
		int element=0;
		char variablename[100];
		do{
			int type=buf[decodepos++];
			if(type==0) break;
			int rw=buf[decodepos++];
			int copy=0;
			while(buf[decodepos]!=1){
				if(decodepos>len){
					printf("Order1: error copying stringname\n");
					return -2;
				}
				variablename[copy++]=buf[decodepos++];
			}
			variablename[copy]=0;
			//printf("Variable '%s', type:%i RW:%i element:%i\n",variablename,type,rw,element);
			
			//copy stringname
			if(g_utf8_validate(variablename,-1,NULL)){
				strncpy(mydd[element].label,variablename,MAX_LABEL_LENGTH);
			}else{
				strncpy(mydd[element].label,"non valid string",MAX_LABEL_LENGTH);
			}
			mydd[element].type=type;
			mydd[element].rw=rw;
			mydd[element].data_l=0;
			element=element+1;
			//force update to gtk
			decodepos++;
        
		}while(1);
		if(element<1){
			printf("Order1: Element size <1. Ignoring string.\n");
			return 0;
		}
		numofelements = element;
		element=element+1;
		strncpy(mydd[element].label,"empty",MAX_LABEL_LENGTH);
		mydd[element].type=0;
		mydd[element].rw=0;
		mydd[element].data_l=0;
		//say_hello();
		//variables_window_update();
		//important to use calloc
		
		inject_call((GSourceFunc)variables_window_update, mydd);
		return 1;
	}else if(order==ORDER_02_UPDATE_ALL_VAL){
		int elementstodecode=(len-3)/4;
		//printf("we have %i elements to decode; should be %i\n",elementstodecode, numofelements);
		if (elementstodecode < numofelements){
			//string is not complete;
			return -1; // mp
		}else if(elementstodecode > numofelements){
			printf("Order 2: (Update all values) with %i elements, but should be %i\n",elementstodecode, numofelements);
			print_hex_data_simple(buf, len);
			return -2;
		}
		uint32_t *data=calloc(sizeof(uint32_t),elementstodecode+1);
		int decodepos=startbyte+2;
		int i;
		for(i=0;i<elementstodecode;i++){
			data[i]=(
				(buf[2+i*4]<<(8*3))+
				(buf[3+i*4]<<(8*2))+
				(buf[4+i*4]<<(8*1))+
				(buf[5+i*4]<<(8*0)));
			//printf("%i:%i(%x-%x-%x-%x)\n",i,data[i],buf[2+i*4],buf[3+i*4],buf[4+i*4],buf[5+i*4]);
		}
		inject_call((GSourceFunc)variables_window_update_vars, data);
		return 1;
		//
	}else if(order==ORDER_03_UPDATE_SIG_VAL){
		//updating single variable+
		if(len==(3+6)){
			set_single_var_t *singlevar;
			singlevar=malloc(sizeof(set_single_var_t));
			singlevar->addr=(buf[2]<<8)+(buf[3]<<0);
			singlevar->val=(buf[4]<<8*3)+(buf[5]<<8*2)+(buf[6]<<8*1)+(buf[7]<<8*0);
			//senprintf("addr:%i|data:%i\n",singlevar->addr,singlevar->val);
			inject_call((GSourceFunc)variables_window_update_single_var, singlevar);
			return 1;
		}else{
			printf("Order3: Single value update - wrong length\n");
			if(len>9){
				return -2; //data is crap
			}else{
				return -1;
			}
		}
	}else if(order==ORDER_08_FUNC_INIT){
		//function init reply
		char terminator_check[3] = {1,0,1};
		if(memcmp(&terminator_check[0],&buf[len-4],3)!=0 || len<6){
//			printf("Order08-manufacturer:byte did not match\nbuf:");
//			print_hex_data(&buf[0],len);
//			printf("checked:");
//			print_hex_data(&buf[len-4],3);
			return -1; //more data to come
		}
		
		mcu_func_t *my_mcu_func=calloc(sizeof(mcu_func_t),len/4+1);
		int countfunc=0;
		char *msg;
		int decodepos=startbyte+2;
		int lastsucessfulldecode=0;
		do{
			my_mcu_func[countfunc].id=buf[decodepos++];
			char *msg=my_mcu_func[countfunc].name;
			int copy=0;
			while(buf[decodepos]!=1){
				if(decodepos>len || (decodepos-lastsucessfulldecode)>(FUNC_NAME_LENGTH-1)){
					print_hex_data(&buf[0],len);
					printf("Order8: error copying stringname %c (decodepos: %i)\n",buf[decodepos],decodepos);
					return 0;
				}
				msg[copy++]=buf[decodepos++];
			}
			msg[copy]=0; //terminator

			//check string integrity
			if(!g_utf8_validate(my_mcu_func[countfunc].name,-1,NULL)){
				//in case string is invalid, prevent errors from gtk.
				strncpy(my_mcu_func[countfunc].name,"non valid function string",FUNC_NAME_LENGTH);
			}
			lastsucessfulldecode=decodepos;
			decodepos++;
			//printf("Func %i:%sDecode:%i\n",my_mcu_func[countfunc].id,msg,decodepos);
			if(my_mcu_func[countfunc].id==0) break;
		
			countfunc++;
		}while(countfunc<254);
		//transfer data
		inject_call((GSourceFunc)FuncOnMCU_update, my_mcu_func);
		return 1;
	}else if(order==ORDER_09_FUNC_REP){
		if(len<5){
			return -1;
		}
		if(len>5){
			return -2;
		}
		printf("Function response recieved.\n");
		int decodepos=startbyte+2;
		int function=buf[decodepos++];
		int funcstatus=buf[decodepos++];
		
		if(!(funcstatus==1)){
			timeout_reset_of_fail(function);
			func_report_execution_fail(function,funcstatus);
		}else{
			//we can reactivate call
			func_reset_register_callback(function);
		}
		return 1;
	}else if(order==ORDER_FE_MSG){
		if(buf[len-2]!=1){
			//look for terminator
			if(len<500){
				return -1;//maybe there comes some usefull data
			}else{
				return -2;//probably crap
			}
		}
		
		//We recieved a msg from MCU
		char *msg=malloc(sizeof(char)*1000);
		int decodepos=startbyte+2;
		int copy=0;
		while(buf[decodepos]!=1){
			if(decodepos>len){
				printf("OrderFE:error copying message\n");
				free(msg);
				return -1;
			}
			msg[copy++]=buf[decodepos++];
		}
		msg[copy]=0;
		if(!g_utf8_validate(msg,-1,NULL)){
			printf("OrderFE:msg not utf8 compliant\n");
			free(msg);
			return -2;
		}
			
		//printf("MCU told '%s'\n",msg);

		inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
		return 1;
	}else if(order==ORDER_FF_EMERGENY){
		if(len>3){
			return -2;
		}
		char *msg=malloc(sizeof(char)*MAX_MSG_LENGTH);
		strncpy(msg,"MCU send emergency code",MAX_MSG_LENGTH);
		inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
		return 1;
	}else if(order==ORDER_00_PING){
		if(len>3){
			return -2;
		}
		//ping, not yet implemented
		return 1;
	}else if(order==ORDER_F0_SUPPLIER_INFO){
		if(len<7){
			return -1;
		}
		if(buf[len-2]!=1){
			//printf("OrderF0-manufacturer:byte did not match");
			//print_hex_data(&buf[len-2],1);
			return -1;
		}else{
			//printf("OrderF0-manufacturer:byte match");
		}
		int decodepos=startbyte+2;
		//updates supplier information to gui
		uint8_t sw_major=buf[decodepos++];
		uint8_t sw_minor=buf[decodepos++];
		char* sw_string=malloc(len*sizeof(char));
		sprintf(sw_string,"%i.%i",sw_major,sw_minor);
		char* manufacturer=malloc(len*sizeof(char));
		int copy=0;
		while(buf[decodepos]!=1){
			if(decodepos>len){
				printf("OrderF0-manufacturer:error copying stringname (1)\n");
				print_hex_data(&buf[0],len);
				free(sw_string);
				free(manufacturer);
				return -2;
			}
			manufacturer[copy++]=buf[decodepos++];
		}
		manufacturer[copy++]=0;
		//check string integrity
		if(!g_utf8_validate(manufacturer,-1,NULL)){
			//in case string is invalid, prevent errors from gtk.
			printf("OrderF0-manufacturer:invalid utf8 string (1)\n");
			print_hex_data(&buf[0],len);
			free(sw_string);
			free(manufacturer);
			return -2;
		}

		char* devicename=malloc(len*sizeof(char));
		copy=0;
		decodepos++; //advance one char
		while(buf[decodepos]!=1){
			if(decodepos>len){
				printf("OrderF0-manufacturer:error copying stringname (2)\n");
				print_hex_data(&buf[0],len);
				free(devicename);
				free(sw_string);
				return -2;
			}
			devicename[copy++]=buf[decodepos++];
		}
		devicename[copy++]=0;
		//check string integrity
		if(!g_utf8_validate(devicename,-1,NULL)){
			printf("OrderF0-manufacturer:not utf8 compliant\n");
			free(devicename);
			free(sw_string);
			return -2;
		}

		//printf("Transmitting %s|%s|%s",manufacturer,devicename,sw_string);
		inject_call((GSourceFunc)gui_window_label_manufacturer_updatefree, manufacturer);
		inject_call((GSourceFunc)gui_window_label_device_updatefree, devicename);
		inject_call((GSourceFunc)gui_window_label_softwareversion_updatefree,sw_string);
		return 1;
	}else if(order==ORDER_F1_UNIQUE_ID){
		int expectedlen=3*4+3;
		if(len<(expectedlen)){
			printf("ORDER_F1_UNIQUE_ID: expected %i elements, however recieved %i",(expectedlen),len);
			print_hex_data_simple(buf, len);
			return -1;
		}
		if(len>(expectedlen)){
			return -2;
		}
		//processes answer on unique id
		int decodepos=startbyte+2;
		uint32_t uniqueid[3];
		uniqueid[0] =(buf[decodepos++]<<(8*3));
		uniqueid[0]+=(buf[decodepos++]<<(8*2));
		uniqueid[0]+=(buf[decodepos++]<<(8*1));
		uniqueid[0]+=(buf[decodepos++]<<(8*0));
		uniqueid[1] =(buf[decodepos++]<<(8*3));
		uniqueid[1]+=(buf[decodepos++]<<(8*2));
		uniqueid[1]+=(buf[decodepos++]<<(8*1));
		uniqueid[1]+=(buf[decodepos++]<<(8*0));
		uniqueid[2] =(buf[decodepos++]<<(8*3));
		uniqueid[2]+=(buf[decodepos++]<<(8*2));
		uniqueid[2]+=(buf[decodepos++]<<(8*1));
		uniqueid[2]+=(buf[decodepos++]<<(8*0));
		//printf("copy ok\n");
		fflush(stdout);
		char *uniqueidstr=malloc(100);
		sprintf(uniqueidstr,"%04X-%04X-%04X",uniqueid[0],uniqueid[1],uniqueid[2]);
		inject_call((GSourceFunc)gui_window_label_uniqueid_updatefree, uniqueidstr);
		return 1;
	}else if(order==ORDER_FD_LOG){
		//print_hex_data_simple(buf, len);
		//fflush(stdout);
		//Loads logs
		int decodepos=startbyte+2;
		logview_t* log=malloc(sizeof(logview_t));
		int size=sizeof(set_var_t)*(len/4);
		set_var_t* mydd=malloc(size);
		memset(mydd, 0, size);
		uint64_t time;
		log->time =((uint64_t)buf[decodepos++]<<(8*7));
		log->time+=((uint64_t)buf[decodepos++]<<(8*6));
		log->time+=((uint64_t)buf[decodepos++]<<(8*5));
		log->time+=((uint64_t)buf[decodepos++]<<(8*4));
		log->time+=(buf[decodepos++]<<(8*3));
		log->time+=(buf[decodepos++]<<(8*2));
		log->time+=(buf[decodepos++]<<(8*1));
		log->time+=(buf[decodepos++]<<(8*0));
		char *msg=malloc(len);
		int copy=0;
		while(buf[decodepos]!=1){
			if(decodepos>len){
				printf("OrderFD-msg:error copying stringname\n");
				free(log);
				free(mydd);
				return -2;
			}
			log->msg[copy++]=buf[decodepos++];
		}
		log->msg[copy++]=0;
		//check string integrity
		if(!g_utf8_validate(log->msg,-1,NULL)){
			//in case string is invalid, prevent errors from gtk.
			strncpy(log->msg,"non valid function string",LOG_MSG_MAX_LENGTH);
			free(log);
			free(mydd);
			return -2; //return -2 to indicate crap
		}

		decodepos++;
		struct set_variables *globaldata;
		globaldata=varfunc_widgets; //local working copy to easy my life
		int e=0;
		while(globaldata[e].type){
			strncpy(mydd[e].label,globaldata[e].label,MAX_LABEL_LENGTH); //copy label
			uint32_t data;
			data =(buf[decodepos++]<<(8*3));
			data+=(buf[decodepos++]<<(8*2));
			data+=(buf[decodepos++]<<(8*1));
			data+=(buf[decodepos++]<<(8*0));
			mydd[e].data_u=data;
			mydd[e].type=globaldata[e].type;
			e++;
		}
		mydd[e].type=0;//terminator
		log->variables=mydd;
		inject_call((GSourceFunc)gui_window_logview_attach_msg,log);
		return 1;
	}else{
		char *msg=malloc(sizeof(char)*1000);
		sprintf(msg,"Order 0x%x unkown, ignoring msg.",order);
		inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
		return -2;
	}
	return 1;
}


void monitor_rs232_disconnect(void){
	monitor_recieve_terminate();
	monitor_master_req_frequently_disable();
	if(_comport>=0){
		RS232_CloseComport(_comport);
	}else{
		printf("Cannot close an not opened comport.\n");
	}
}


