#include "tusb.h"

/**
 * @brief Get USB serial number string.
 * 
 * @param desc_str1 String descriptor from index 1.
 * @param max_chars Maximum number of charachters allowed.
 * @return Number of characters in string.
 */
static inline size_t board_usb_get_serial(uint16_t desc_str1[], size_t max_chars)
{
    uint8_t uid[16] TU_ATTR_ALIGNED(4);
    size_t  uid_len;

    // Fixed serial string is 01234567889ABCDEF, change to your needs
    uint32_t* uid32 = (uint32_t*) (uintptr_t) uid;
    uid32[0]        = 0x67452301;
    uid32[1]        = 0xEFCDAB89;
    uid_len         = 8;

    if(uid_len > max_chars / 2)
        uid_len = max_chars / 2;

    for(size_t i = 0; i < uid_len; i++) {
        for(size_t j = 0; j < 2; j++) {
            const char    nibble_to_hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            uint8_t const nibble            = (uid[i] >> (j * 4)) & 0xf;
            desc_str1[i * 2 + (1 - j)]      = nibble_to_hex[nibble]; // UTF-16-LE
        }
    }

    return 2 * uid_len;
}

/**
 * @brief USB product identifier.
 * @note A combination of interfaces must have a unique product id, since a PC will save the device driver after the first plug.
 *       Same VID/PID with different interface e.g MSC (first), then CDC (later) will possibly cause a system error on the PC.
 */
#define USB_PID 0x0014
/// USB vendor identifier.
#define USB_VID 0xCafe
/// BUSB specification release number in binary-coded decimal.
#define USB_BCD 0x0200

//--------------------------------------------------------------------+
// Device Descriptors
//--------------------------------------------------------------------+

/// TinyUSB device descriptor
tusb_desc_device_t const desc_device = { .bLength         = sizeof(tusb_desc_device_t),
                                         .bDescriptorType = TUSB_DESC_DEVICE,
                                         .bcdUSB          = USB_BCD,

                                         // Use Interface Association Descriptor (IAD) for CDC
                                         // As required by USB Specs IAD's subclass must be common class (2) and protocol must be IAD (1)
                                         .bDeviceClass    = TUSB_CLASS_MISC,
                                         .bDeviceSubClass = MISC_SUBCLASS_COMMON,
                                         .bDeviceProtocol = MISC_PROTOCOL_IAD,
                                         .bMaxPacketSize0 = CFG_TUD_ENDPOINT0_SIZE,

                                         .idVendor  = USB_VID,
                                         .idProduct = USB_PID,
                                         .bcdDevice = 0x0100,

                                         .iManufacturer = 0x01,
                                         .iProduct      = 0x02,
                                         .iSerialNumber = 0x03,

                                         .bNumConfigurations = 0x01 };

/**
 * @brief Invoked for "get device descriptor" request.
 *
 * @return Pointer to requested descriptor.
 */
uint8_t const* tud_descriptor_device_cb(void)
{
    return (uint8_t const*) &desc_device;
}

//--------------------------------------------------------------------+
// Configuration Descriptor
//--------------------------------------------------------------------+

enum
{
    ITF_NUM_CDC_0 = 0,
    ITF_NUM_CDC_0_DATA,
    ITF_NUM_TOTAL
};

#define CONFIG_TOTAL_LEN (TUD_CONFIG_DESC_LEN + CFG_TUD_CDC * TUD_CDC_DESC_LEN)

#define EPNUM_CDC_0_NOTIF 0x81
#define EPNUM_CDC_0_OUT   0x02
#define EPNUM_CDC_0_IN    0x82

/// Device fullspeed configuration
uint8_t const desc_fs_configuration[] = {
    // Config number, interface count, string index, total length, attribute, power in mA
    TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, 0, CONFIG_TOTAL_LEN, 0x00, 100),

    // 1st CDC: Interface number, string index, EP notification address and size, EP data address (out, in) and size
    TUD_CDC_DESCRIPTOR(ITF_NUM_CDC_0, 4, EPNUM_CDC_0_NOTIF, 8, EPNUM_CDC_0_OUT, EPNUM_CDC_0_IN, 64),
};

#if TUD_OPT_HIGH_SPEED
// Per USB specs: high speed capable device must report device_qualifier and other_speed_configuration

/// Device highspeed configuration
uint8_t const desc_hs_configuration[] = {
    // Config number, interface count, string index, total length, attribute, power in mA
    TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, 0, CONFIG_TOTAL_LEN, 0x00, 100),

    // 1st CDC: Interface number, string index, EP notification address and size, EP data address (out, in) and size
    TUD_CDC_DESCRIPTOR(ITF_NUM_CDC_0, 4, EPNUM_CDC_0_NOTIF, 8, EPNUM_CDC_0_OUT, EPNUM_CDC_0_IN, 512),
};

/**
 * @brief Device qualifier.
 *
 * Mostly similar to device descriptor since we don't change configuration based on speed.
 */
tusb_desc_device_qualifier_t const desc_device_qualifier = { .bLength         = sizeof(tusb_desc_device_t),
                                                             .bDescriptorType = TUSB_DESC_DEVICE,
                                                             .bcdUSB          = USB_BCD,

                                                             .bDeviceClass    = TUSB_CLASS_MISC,
                                                             .bDeviceSubClass = MISC_SUBCLASS_COMMON,
                                                             .bDeviceProtocol = MISC_PROTOCOL_IAD,

                                                             .bMaxPacketSize0    = CFG_TUD_ENDPOINT0_SIZE,
                                                             .bNumConfigurations = 0x01,
                                                             .bReserved          = 0x00 };

/**
 * @brief Invoked for "get device qualifier descriptor" request.
 * 
 * The device qualifier descriptor describes information about a high-speed capable device that would
 * change if the device were operating at the other speed. If not highspeed capable stall this request.
 *
 * @return Pointer to descriptor.
 */
uint8_t const* tud_descriptor_device_qualifier_cb(void)
{
    return (uint8_t const*) &desc_device_qualifier;
}

/**
 * @brief Invoked for "get other seed configuration descriptor" request.
 * 
 * Configuration descriptor in the other speed, e.g if high speed then this is for full speed and vice versa.
 *
 * @return Pointer to descriptor.
 */
uint8_t const* tud_descriptor_other_speed_configuration_cb(uint8_t index)
{
    (void) index;

    // If link speed is high return fullspeed config, and vice versa
    return (tud_speed_get() == TUSB_SPEED_HIGH) ? desc_fs_configuration : desc_hs_configuration;
}

#endif /* TUD_OPT_HIGH_SPEED */

/**
 * @brief Handle "get configuration descriptor" request.
 * @param index Index of descriptor being requested.
 *
 * @return Pointer to descriptor.
 */
uint8_t const* tud_descriptor_configuration_cb(uint8_t index)
{
    (void) index;

#if TUD_OPT_HIGH_SPEED
    // Although we are highspeed, host may be fullspeed.
    return (tud_speed_get() == TUSB_SPEED_HIGH) ? desc_hs_configuration : desc_fs_configuration;
#else
    return desc_fs_configuration;
#endif
}

//--------------------------------------------------------------------+
// String Descriptors
//--------------------------------------------------------------------+

/// String descriptor indices.
enum
{
    STRID_LANGID = 0,
    STRID_MANUFACTURER,
    STRID_PRODUCT,
    STRID_SERIAL,
};

/// Array of pointer to the string descriptors.
char const* string_desc_arr[] = {
    (const char[]){ 0x09, 0x04 }, // 0: is supported language is English (0x0409)
    "TinyUSB",                    // 1: Manufacturer
    "TinyUSB Device",             // 2: Product
    NULL,                         // 3: Serials will use unique ID if possible
    "TinyUSB CDC",                // 4: CDC Interface
};

/// Buffer used ny tud_descriptor_string_cb to return string descriptors.
static uint16_t _desc_str[32 + 1];

/**
 * @brief Invoked for a "get string descriptor" request.
 * @param index Index of the string descriptor being requested.
 * @param langid Language ID requested by USB host.
 *
 * @return Pointer to the requested descriptor.
 */
uint16_t const* tud_descriptor_string_cb(uint8_t index, uint16_t langid)
{
    (void) langid;
    size_t chr_count;

    switch(index) {
        case STRID_LANGID:
            memcpy(&_desc_str[1], string_desc_arr[0], 2);
            chr_count = 1;
            break;

        case STRID_SERIAL:
            chr_count = board_usb_get_serial(_desc_str + 1, 32);
            break;

        default:
            // Note: the 0xEE index string is a Microsoft OS 1.0 descriptor
            // https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/microsoft-defined-usb-descriptors

            if(!(index < sizeof(string_desc_arr) / sizeof(string_desc_arr[0])))
                return NULL;

            const char* str = string_desc_arr[index];

            // Cap at max char
            chr_count              = strlen(str);
            size_t const max_count = sizeof(_desc_str) / sizeof(_desc_str[0]) - 1; // -1 for string type
            if(chr_count > max_count)
                chr_count = max_count;

            // Convert ASCII string into UTF-16
            for(size_t i = 0; i < chr_count; i++) {
                _desc_str[1 + i] = str[i];
            }
            break;
    }

    // First byte is length (including header), second byte is string type
    _desc_str[0] = (uint16_t) ((TUSB_DESC_STRING << 8) | (2 * chr_count + 2));

    return _desc_str;
}
