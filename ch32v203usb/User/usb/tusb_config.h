#ifndef TUSB_CONFIG_H
#define TUSB_CONFIG_H

#define CFG_TUSB_OS (OPT_OS_FREERTOS)

#define CFG_TUSB_RHPORT0_MODE  (OPT_MODE_DEVICE | OPT_MODE_LOW_SPEED)
#define CFG_TUD_ENDPOINT0_SIZE (64)

#define CFG_TUD_AUDIO  (0)
#define CFG_TUD_HID    (0)
#define CFG_TUD_CDC    (1)
#define CFG_TUD_MSC    (0)
#define CFG_TUD_MIDI   (0)
#define CFG_TUD_VENDOR (0)

// Workaround to use STM32 USB driver which uses the same register set for USBD.
#define CFG_TUSB_MCU (OPT_MCU_STM32F1)
#define STM32F103xB

#define CFG_TUD_CDC_RX_BUFSIZE (TUD_OPT_HIGH_SPEED ? 512 : 64)
#define CFG_TUD_CDC_TX_BUFSIZE (TUD_OPT_HIGH_SPEED ? 512 : 64)
#define CFG_TUD_CDC_EP_BUFSIZE (TUD_OPT_HIGH_SPEED ? 512 : 64)

#endif /* TUSB_CONFIG_H */
