#include "unified.h"



/**
 * @brief Main program.
 */
int main(void)
{
    // System initialization
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    SystemCoreClockUpdate();

    //Starts the MCU Tracer Task
    mcu_tracer_tasks_init();
    vTaskStartScheduler();

    while(1);// should not reach here
}
