#ifndef SYSTEM_CH32V20X_H
#define SYSTEM_CH32V20X_H

#ifdef __cplusplus
extern "C" {
#endif

/// System clock frequency (core clock)
extern uint32_t SystemCoreClock;

/// Exported functions
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_CH32V20X_H */
