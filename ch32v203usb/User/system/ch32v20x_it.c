#include "tusb.h"

/// Interrupt handler declarations.
void NMI_Handler(void) __attribute__((interrupt()));
void HardFault_Handler(void) __attribute__((interrupt()));
void USB_HP_CAN1_TX_IRQHandler(void) __attribute__((interrupt()));
void USB_LP_CAN1_RX0_IRQHandler(void) __attribute__((interrupt()));
void USBWakeUp_IRQHandler(void) __attribute__((interrupt()));
void USBHD_IRQHandler(void) __attribute__((interrupt()));

/**
 * @brief Handle non maskable interrupt.
 */
void NMI_Handler(void)
{
    while(1) {
    }
}

/**
 * @brief Hard fault handler.
 */
void HardFault_Handler(void)
{
    while(1) {
    }
}

/**
 * @brief Handling function called for USB interrupts. Forward the interrupts to the TinyUSB device handler.
 */
static void usb_handler(void)
{
    tud_int_handler(0);
}

/**
 * @brief USB high priority interrupt handler.
 */
void USB_HP_CAN1_TX_IRQHandler(void)
{
    usb_handler();
}

/**
 * @brief USB low priority interrupt handler.
 */
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    usb_handler();
}

/**
 * @brief USB wake up interrupt handler.
 */
void USBWakeUp_IRQHandler(void)
{
    usb_handler();
}

/**
 * @brief USBHD interrupt handler. 
*/
void USBHD_IRQHandler(void)
{
    usb_handler();
}
