#include "FreeRTOS.h"
#include "task.h"

/**
 * @brief Needed to provide the memory for the FreeRTOS idle task when configSUPPORT_STATIC_ALLOCATION is 1.
 * 
 * @param ppxIdleTaskTCBBuffer Pointer to buffer for the control block.
 * @param ppxIdleTaskStackBuffer Pointer to buffer for the stack.
 * @param pulIdleTaskStackSize Pointer to variable which stores the size of the allocated stack buffer.
 */
void vApplicationGetIdleTaskMemory(StaticTask_t** ppxIdleTaskTCBBuffer, StackType_t** ppxIdleTaskStackBuffer, uint32_t* pulIdleTaskStackSize)
{
    // Create memories. Make them static to persist after function end.
    static StaticTask_t xIdleTaskTCB;
    static StackType_t  uxIdleTaskStack[configMINIMAL_STACK_SIZE];

    // Assign buffer to pointers.
    *ppxIdleTaskTCBBuffer   = &xIdleTaskTCB;
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;
    *pulIdleTaskStackSize   = configMINIMAL_STACK_SIZE;
}
