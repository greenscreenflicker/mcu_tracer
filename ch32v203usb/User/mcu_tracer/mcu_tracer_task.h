/*
 * mcu_tracer_task.h
 *
 *  Created on: 21 May 2024
 *      Author: mchhe
 */

#ifndef USER_MCU_TRACER_MCU_TRACER_TASK_H_
#define USER_MCU_TRACER_MCU_TRACER_TASK_H_

void mcu_tracer_freertos_init(void);

void mcu_tracer_freertos_task(void* args);
void usb_task(void* args);
int mcu_tracer_tasks_init(void);
void freertos_mcutracer_process(void* args);

#endif /* USER_MCU_TRACER_MCU_TRACER_TASK_H_ */
