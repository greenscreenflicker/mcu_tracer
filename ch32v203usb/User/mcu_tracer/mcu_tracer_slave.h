/*
 * mcu_tracer_slave.h
 *
 *  Created on: May 20, 2020
 *      Author: Michael
 */

#ifndef MCU_TRACER_SLAVE_H_
#define MCU_TRACER_SLAVE_H_

void mcutracer_slave_ping(void);
void mcu_tracer_func_init(void);
void mcu_tracer_func_execute(uint8_t id);

#endif /* MCU_TRACER_SLAVE_H_ */
