#include "unified.h"

// FreeRTOS static stacks
StaticTask_t xTaskBufferCDC;
StackType_t  xStackCDC[CDC_STACK_SIZE];
StaticTask_t xTaskBufferUSB;
StackType_t  xStackUSB[USB_STACK_SIZE];
/**
 * @brief Initialize peripheral clocks.
 */
static void usb_clock_init(void)
{
    // Make sure FreeRTOS has the correct clock
    configASSERT(SystemCoreClock == configCPU_CLOCK_HZ);

    // Set the USB clock divider based on the core clock frequency
    switch(SystemCoreClock) {
        case 48000000:
            RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_Div1);
            break;
        case 96000000:
            RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_Div2);
            break;
        case 144000000:
            RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_Div3);
            break;
        default:
            __disable_irq();
            while(1) {
            }
            break;
    }

    // Enable necessary clocks
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_OTG_FS, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
}

/**
 * @brief Main program.
 */
int mcu_tracer_tasks_init(void){
    // System initialization
    usb_clock_init();
    // Create FreeRTOS tasks and start scheduler.
    // The USB tasks has priority over the CDC handling task.
    xTaskCreateStatic(freertos_mcutracer_process,   // Function that implements the task.
                      "mcu_tracer",                 // Text name for the task.
                      CDC_STACK_SIZE,               // Number of indexes in the xStack array.
                      NULL,                         // Parameter passed into the task.
                      TASK_PRIORTIY_MCUTRACER,      // Priority at which the task is created.
                      xStackCDC,                    // Array to use as the task's stack.
                      &xTaskBufferCDC);             // Variable to hold the task's data structure.

    xTaskCreateStatic(usb_task,                     // Function that implements the task.
                      "usb",                        // Text name for the task.
                      USB_STACK_SIZE,               // Number of indexes in the xStack array.
                      NULL,                         // Parameter passed into the task.
                      TASK_PRIORTIY_USB,            // Priority at which the task is created.
                      xStackUSB,                    // Array to use as the task's stack.
                      &xTaskBufferUSB);             // Variable to hold the task's data structure.

}

//#define INTERACTIVE_COM

/**
 * @brief Function handling USB tasks by calling TinyUSB tud_task.
 *
 * @param args Unused.
 */
void usb_task(void* args)
{
    tusb_init();
    while(true) {
        //when TUD will run for infinite time and pauses itself.
        //low risk for starving of lower priority tasks
        tud_task();
    }
    vTaskDelete(NULL);
}

/**
 * @brief Function handling the serial CDC interface.
 *
 * @param args Unused.
 */
void freertos_mcutracer_process(void* args)
{
    (void) args;
    TickType_t xLastWakeTime;

    while(true) {
        mcu_tracer_tick();
        vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(100));
    }

    vTaskDelete(NULL);
}

/**
 * @brief Invoked when device is mounted.
 */
void tud_mount_cb(void)
{
}

/**
 * Invoked when device is unmounted.
 */
void tud_umount_cb(void)
{
}

/**
 * @brief Invoked when USB is suspended.
 *
 * Within 7ms, device must draw an average of current less than 2.5 mA from bus.
 *
 * @param remote_wakeup_en True if host allows remote wakeup.
 */
void tud_suspend_cb(bool remote_wakeup_en)
{
    (void) remote_wakeup_en;
}

/**
 * @brief Invoked when USB is resumed.
 */
void tud_resume_cb(void)
{
}

/**
 * @brief Invoked when line state changed e.g connected/disconnected.
 *
 * @param itf CDC interface number.
 * @param drt Data terminal ready.
 * @param rts Request to send.
 */
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts)
{
    (void) itf;
    (void) rts;

    if(dtr) {
        // Terminal connected
    } else {
        // Terminal disconnected
    }
}

/**
 * @brief Invoked when CDC interface received data from host.
 *
 * @param itf CDC interface number.
 */
void tud_cdc_rx_cb(uint8_t itf)
{
    (void) itf;
}
