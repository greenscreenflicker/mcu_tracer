/*
 * mcu_tracer_slave.c
 *
 *  Created on: May 20, 2020
 *      Author: Michael
 */

#include "unified.h"
//new version

void mcutracer_slave_ping_reply(void){
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(0);
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

void mcu_tracer_slave_emergency_reply(void){
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(0xFF);
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

void mcu_tracer_init_reply(void){
	int i;
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(1); //Order type 1: Sending init
	for(i=0; i<MCU_TRACER_MAXSETVARIABLES; i++){
		//check if we have a terminator =>if, break
		if(mcutracer_variables[i].type==0) break;
		mcu_tracer_write_serial(mcutracer_variables[i].type);
		mcu_tracer_write_serial(mcutracer_variables[i].rw);
		mcu_tracer_write_string((uint8_t*)mcutracer_variables[i].label);
	}
	//sending terminating byte
	mcu_tracer_write_serial(0); //last byte (terminator)
	mcu_tracer_write_serial(0); //read only
	mcu_tracer_write_serial(1); //send byte terminator

	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

//Sends actual values to pc
void mcu_tracer_vals_reply(void){
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_2_TRANSFER_ALL_VALUES); //order two, we transfer variables
	uint8_t i;
	for(i=0; i<MCU_TRACER_MAXSETVARIABLES; i++){
		//check if we have a terminator =>if, break
		if(mcutracer_variables[i].type==0) break;
		uint32_t data;
		data=*(mcutracer_variables[i].data_l);
		mcu_tracer_write_serial(data>>(3*8));
		mcu_tracer_write_serial(data>>(2*8));
		mcu_tracer_write_serial(data>>(1*8));
		mcu_tracer_write_serial(data>>(0*8));
	}
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

//Updates the value in the register
const char Data_type_not_yet_supported[]="Data type not yet supported";
void mcu_tracer_update_reply(uint16_t addr, int32_t val){
	if(addr>MCU_TRACER_MAXSETVARIABLES){
		#if MCU_TRACER_STRICT_DEBUGGING>0
		asm("bkpt");
		#endif
		//error, we do not have this addr
		return;
	}

	int32_t* ptr=mcutracer_variables[addr].data_l;
	if(!ptr){
		//there was an attemd to write to an empty address
		#if MCU_TRACER_STRICT_DEBUGGING>0
		asm("bkpt");
		#endif
		//error, we do not have this addr
		return;
	}

	if(mcutracer_variables[addr].rw==MCU_TRACER_READWRITE){
	    //variable may be written
        if(mcutracer_variables[addr].type==1||mcutracer_variables[addr].type==3){
            //Integer
            // int32_t *toupdate;
            //we set a max value
            if(val>mcutracer_variables[addr].data_setmax_i){
                val=mcutracer_variables[addr].data_setmax_i;
            }
            //we set a min value
            if(val<mcutracer_variables[addr].data_setmin_i){
                val=mcutracer_variables[addr].data_setmin_i;
            }
            *(mcutracer_variables[addr].data_l)=val;
#if MCU_TRACER_FLOAT_SUPPORT==1
        }else if(mcutracer_variables[addr].type==2){
            union_int_float_t dataconv;
            dataconv.i=val;
            float val_f=dataconv.f;

            if((val_f)>mcutracer_variables[addr].data_setmax_f){
                val_f=mcutracer_variables[addr].data_setmax_f;
            }
            if((val_f)<mcutracer_variables[addr].data_setmin_f){
                val_f=mcutracer_variables[addr].data_setmin_f;
            }
            *(mcutracer_variables[addr].data_f)=val_f;
#endif
        }else{
            mcu_tracer_msg(Data_type_not_yet_supported);
            return;
        }
        //callback
        if(mcutracer_variables[addr].callback){
            //we have an callback to execute
            mcutracer_variables[addr].callback();
        }
	}

	//Reply
	int32_t valreadback=*(mcutracer_variables[addr].data_l);
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_3_UPDATE_SINGLE_VALUE); //order 3, we transfer single value
	mcu_tracer_write_serial(addr>>(1*8));
	mcu_tracer_write_serial(addr>>(0*8));
	mcu_tracer_write_serial(valreadback>>(3*8));
	mcu_tracer_write_serial(valreadback>>(2*8));
	mcu_tracer_write_serial(valreadback>>(1*8));
	mcu_tracer_write_serial(valreadback>>(0*8));
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

void mcu_tracer_req_single_val_reply(uint16_t addr){
    if(addr>MCU_TRACER_MAXSETVARIABLES){
        #if MCU_TRACER_STRICT_DEBUGGING>0
        asm("bkpt");
        #endif
        //error, we do not have this addr
        return;
    }
    int32_t* ptr=mcutracer_variables[addr].data_l;
    if(!ptr){
        //there was an attemd to write to an empty address
        #if MCU_TRACER_STRICT_DEBUGGING>0
        asm("bkpt");
        #endif
        //error, we do not have this addr
        return;
    }
    int32_t valreadback=*(mcutracer_variables[addr].data_l);

    mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
    mcu_tracer_write_serial(ORDER_4_REQ_SINGLE_VALUE); //order 3, we transfer single value
    mcu_tracer_write_serial(addr>>(1*8));
    mcu_tracer_write_serial(addr>>(0*8));
    mcu_tracer_write_serial(valreadback>>(3*8));
    mcu_tracer_write_serial(valreadback>>(2*8));
    mcu_tracer_write_serial(valreadback>>(1*8));
    mcu_tracer_write_serial(valreadback>>(0*8));
    mcu_tracer_write_checksum();
}

void mcu_tracer_func_init_reply(void){
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_8_INIT_FUNCTION); //Reply with order code
	uint8_t fipo=0;
	while(mcutracer_functions[fipo].func_ptr){
		//send data
		if(fipo>254) break;
		mcu_tracer_write_serial(fipo+1);
		mcu_tracer_write_string((uint8_t*)mcutracer_functions[fipo].label);
		fipo++;
	}
	mcu_tracer_write_serial(0);//send last element indicator
	mcu_tracer_write_serial(1);//empty string
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}

void mcu_tracer_func_execute_reply(uint8_t id){
	int sucess=0;
	if(id>0){
		//check id;
		uint8_t internal;
		internal=id-1;
		if(!(internal>MCU_TRACER_NUM_FUNCTIONS)){
			if(mcutracer_functions[internal].func_ptr){
				(mcutracer_functions[internal].func_ptr)();
				sucess=1;
			}
		}
	}
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_9_EXEC_FUNC);
	mcu_tracer_write_serial(id);
	mcu_tracer_write_serial(sucess);
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}
/*//Supported as of VERSION 3
Order 0xF0: Supplier Inforation */
void mcu_tracer_supply_information_reply(void){

	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_F0_SUPPLIER_INFO);
	//-Software Version Major (1 bytes)
	mcu_tracer_write_serial(firstpage_version_major);
	mcu_tracer_write_serial(firstpage_version_minor);
	mcu_tracer_write_string((uint8_t *) &firstpage_manufacturer[0]);
	mcu_tracer_write_string((uint8_t *) &firstpage_description[0]);

	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();

}

void mcu_tracer_uniqueid_reply(void){
//	mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_F1_UNIQUE_ID);
	//-Software Version Major (1 bytes)
	mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid0());
	mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid1());
	mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid2());
	mcu_tracer_write_checksum();
//	mcu_tracer_send_mutex_giveback();
}

//1 means success
//-1 means data is missing
//-2 means, data can be destroyed
int mcutracer_slave_decode_string(unsigned char* buf, int len){
	int order=buf[1];
	if(len<3) return -1;
	if(order==ORDER_1_INIT){
		if(len>3){
			return -2;
		}
		mcu_tracer_init_reply();
		mcu_tracer_appspec_initcomplete_callback();
	}else if(order==ORDER_2_TRANSFER_ALL_VALUES){
		if(len>3){
			return -2;
		}
		//make callback when finished
		mcu_tracer_vals_reply();
		mcu_tracer_appspec_allupdate_callback();
	}else if(order==ORDER_3_UPDATE_SINGLE_VALUE){
	    if(len<9){
	        return -1;
	    }
		if(len>9){
			return -2;
		}
		uint32_t val=(buf[4]<<(8*3))+ (buf[5]<<(8*2)) + (buf[6]<<(8*1)) + (buf[7]<<(8*0));
		uint16_t addr=(buf[2]<<(8))+buf[3];
		mcu_tracer_update_reply(addr, val);
	}else if(order==ORDER_4_REQ_SINGLE_VALUE){
	    if(len<5) return -1; //too short, maybe more comes
	    if(len>5) return -2; //too long, discard
	    uint16_t addr=(buf[2]<<(8))+buf[3];
	    mcu_tracer_req_single_val_reply(addr);
	}else if(order==ORDER_8_INIT_FUNCTION){
	    if(len<3) return -1;
		if(len>3) return -2;
		//execute callback
		mcu_tracer_func_init_reply();
		mcu_tracer_appspec_function_init_callback();
	}else if(order==ORDER_9_EXEC_FUNC){
		if(len<4){
			return -1;
		}
        if(len>4){
            return -2;
        }
		uint8_t funcid=buf[2];
		mcu_tracer_func_execute_reply(funcid);
	}else if(order==0xFE){
//			//We recieved a msg from MCU
//			char *msg=malloc(sizeof(char)*1000);
//			int decodepos=startbyte+2;
//			int copy=0;
//			while(buf[decodepos]!=1){
//				if(decodepos>len){
//					printf("OrderFE:error copying stringname\n");
//					return;
//				}
//				msg[copy++]=buf[decodepos++];
//			}
//			msg[copy]=0;
//			//printf("MCU told '%s'\n",msg);
//
//			inject_call((GSourceFunc)gui_msg_center_add_msg, msg);
	}else if(order==0xFF){
		if(len!=3){
			return -2;
		}
		mcu_tracer_emergency_callback();
		mcu_tracer_slave_emergency_reply();
	}else if(order==0){
		if(len>3){
			return -2;
		}
		mcu_tracer_pingcallback();
		mcutracer_slave_ping_reply();
	}else if (order==ORDER_F0_SUPPLIER_INFO){
		if(len>3){
			return -2;
		}
		mcu_tracer_supply_information_reply();
	}else if (order==ORDER_F1_UNIQUE_ID){
		if(len>3){
			return -2;
		}
		mcu_tracer_uniqueid_reply();
	}
	#if MCU_TRACER_ENABLE_VARSAVE>0
	else if (order==ORDER_FD_LOG){
		if(len>3){
			return -2;
		}
		mcu_tracer_log_reply();
	}
	#endif
	else{
	    //order number unkown, we can destroy msg
	    mcu_tracer_msg("MCU: OrderID not supported.");
	    return -2;
	}

	return 1;
}
