/*
 * mcu_tracer_core.c
 *
 *  Created on: May 14, 2020
 *      Author: Michael
 */

#include "unified.h"

//global variables
//mcu_tracer_var_t 	mcutracer_variables[MCU_TRACER_MAXSETVARIABLES];
//mcu_tracer_func_t 	mcutracer_functions[MCU_TRACER_NUM_FUNCTIONS];
uint16_t 			mcutracer_rec_buffer_pos;
uint8_t				mcutracer_rec_buffer[MCU_TRACER_RX_BUF_SIZE];
uint16_t 			mcu_tracer_number_of_slave_variables;
//uint8_t 			mcutracer_send_buf[2][MCU_TRACER_TX_BUF_SIZE];
uint8_t 			txpingpong=0;
//uint16_t			mcutracer_send_bufpos=0;
uint8_t 			mcu_tracer_txchecksum=0;
//volatile uint8_t	mcu_tracer_mutex_send=0;

void mcutracer_sendbuffer_flush(void){
    tud_cdc_write_flush();
}

void mcu_tracer_write_serial(char data){
	mcu_tracer_txchecksum=mcu_tracer_txchecksum^data;
	tud_cdc_write_char(data); //write data to usb directly (no send buffer)
}

void mcu_tracer_write_2byte_var(int32_t var){
	mcu_tracer_write_serial((var>>(8*1))&0xFF);
	mcu_tracer_write_serial((var>>(8*0))&0xFF);
}

void mcu_tracer_write_4byte_var(int32_t var){
	mcu_tracer_write_serial((var>>(8*3))&0xFF);
	mcu_tracer_write_serial((var>>(8*2))&0xFF);
	mcu_tracer_write_serial((var>>(8*1))&0xFF);
	mcu_tracer_write_serial((var>>(8*0))&0xFF);
}

void mcu_tracer_write_8byte_var(int64_t var){
	mcu_tracer_write_serial((var>>(8*7))&0xFF);
	mcu_tracer_write_serial((var>>(8*6))&0xFF);
	mcu_tracer_write_serial((var>>(8*5))&0xFF);
	mcu_tracer_write_serial((var>>(8*4))&0xFF);
	mcu_tracer_write_serial((var>>(8*3))&0xFF);
	mcu_tracer_write_serial((var>>(8*2))&0xFF);
	mcu_tracer_write_serial((var>>(8*1))&0xFF);
	mcu_tracer_write_serial((var>>(8*0))&0xFF);
}

void mcu_tracer_write_checksum(void){
	mcu_tracer_write_serial(mcu_tracer_txchecksum);
	mcu_tracer_txchecksum=0;
}


uint8_t mcutracer_checksum(uint8_t *data, int length){
	uint8_t checksum=0;
	int n;
	for(n=0;n<length;n++){
		checksum=checksum^(*data);
		data++;
		//printf("checksum is:%X",checksum);
	}
	return checksum;
}

int mcutracer_find_startbyte(uint8_t* buf, int len){
	int i;
	for(i=0; i < len; i++)
	{
		if(buf[i]==MCU_TRACER_STARTBYTE){
			//printf("found startbyte at %i\n",i);
			return i;
		}
	}
	return -1; //noting found
}

int mcutracer_find_end(uint8_t *data, int length, int skip){
    uint8_t checksum=0;
    int n;
    int pos=0;
    if(length<3) return -1;
    for(n=0;n<length;n++){
        checksum=checksum^(*data);
        data++;
        pos++;
        if(!checksum){
            if(!skip){
                return pos;
            }
            skip--;
        }

        //printf("checksum is:%X",checksum);
    }
    //we failed to find an end
    return -1;
}

int mcutracer_find_end_startbyte(uint8_t *data, int length, int skip){
    uint8_t checksum=0;
    int pos;
    if(length<3) return -1;
    for(pos=0;pos<length;){
        checksum=checksum^(*data);
        data++;
        pos++;
        if(checksum) continue; //checksum is non-zero, so there can't be a match
        if((pos+1)==length){
            //we are the the end of the string
            //when we shouldn't skip something, it can be at the end of tit
            if(!skip) return length;
            //we should skip something, but there's nothing left, that's an error
            return -1;

        }
        if((*data)==MCU_TRACER_STARTBYTE){
            //the next byte is a startbyte, so there is a high propability the string will be good
            //if the next byte woudn't be a startbyte, we can be sure, that it's not valid
            if(!skip){
                return pos;
            }else{
                skip--;
                continue;
            }
        }
        if(!skip){
            return pos;
        }
        skip--;
    }
    //we failed to find an end
    return -1;
}

int mcutracer_find_end_next_is_startbyte(uint8_t *data, int length){
    uint8_t checksum=0;
    int n;
    int pos=0;
    if(length<3) return -1;
    for(n=0;n<length;n++){
        checksum=checksum^(*data);
        data++;
        if(!checksum){
            if(*data==MCU_TRACER_STARTBYTE){
                return pos;
            }
        }
        length++;
        //printf("checksum is:%X",checksum);
    }
    //we failed to find an end
    return -1;
}

int mcutracer_check_parity(uint8_t* buf, int len){
	uint16_t startbyte=mcutracer_find_startbyte(buf,len);
	if(startbyte<0) return 0; //startbyte not found
	uint8_t checksum=mcutracer_checksum((buf+startbyte),len-startbyte);
	if(checksum==0){
		//printf("integrity: no error detected");
		return 1;
	}else{
		//printf("integrity: error detected");
#ifdef STRICT_DEBUGGING
		asm("bkpt");
#endif
		return 0;
	}
}

void mcu_tracer_write_string(uint8_t* data){
   while( *data != '\0' ){
      mcu_tracer_write_serial(*data++ );
   }
   mcu_tracer_write_serial(1);
}


void mcu_tracer_msg(const char* msg){
	//mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(0xFE);
	mcu_tracer_write_string((uint8_t*)msg);
	mcu_tracer_write_checksum();
	//mcu_tracer_send_mutex_giveback();
}



//Finds the element id of a task by human readable name
uint8_t mcu_tracer_find_variable_by_name(char *name, uint8_t *id){
	uint8_t element=0;
	while(element<(MCU_TRACER_MAXSETVARIABLES-1)){
		if(strcmp(&mcutracer_variables[element].label[0],name)==0){
			*id=element;
			return 1;
		}
		element++;
	}
	*id=0;
	return 0;
}

/*

void mcu_tracer_bind_variable_int32(uint8_t id, int32_t *var_ptr){
	if(id<(MCU_TRACER_MAXSETVARIABLES-1)){
		mcutracer_variables[id].data_l=var_ptr;
	}else{
#ifdef STRICT_DEBUGGING
		asm("bkpt");
#endif
	}
}

void mcu_tracer_bind_variable_float32(uint8_t id, float *var_ptr){
	if(id<(MCU_TRACER_MAXSETVARIABLES-1)){
		mcutracer_variables[id].data_f=var_ptr;
	}else{
#ifdef STRICT_DEBUGGING
		asm("bkpt");
#endif
	}
}

void mcu_tracer_bind_callback(uint8_t id, void (*callback)(void)){
	if(id<(MCU_TRACER_MAXSETVARIABLES-1) && (mcutracer_variables[id].data_l)){
		mcutracer_variables[id].callback=callback;
	}else{
#ifdef STRICT_DEBUGGING
		asm("bkpt");
#endif
	}
}*/


uint8_t mcu_tracer_find_funcid_by_name(char *name, uint8_t *funcid){
	uint8_t element=0;
	while(element<(MCU_TRACER_NUM_FUNCTIONS)){
		if(strcmp(&mcutracer_functions[element].label[0],name)==0){
			*funcid=(element+1);
			return 1;
		}
		element++;
	}
	*funcid=0;
	return 0;
}


// portionierer
void mcu_tracer_portionierer_reset(void){
	mcutracer_rec_buffer_pos=0;
}

int mcu_tracer_find_first_startbyte(unsigned char* buf, int32_t len){
	int i;
	for(i=0; i < len; i++)
	{
		if(buf[i]==MCU_TRACER_STARTBYTE){
			//printf("found startbyte at %i\n",i);
			return i;
		}
	}
	return -1; //noting found
}


//returns one if result okay, when wrong, zero
int mcutracer_check_parity_dump(unsigned char* buf, int len){
	if(len<1) return 0;
	unsigned char checksum=mcutracer_checksum(buf,len);
	if(!checksum){
		//printf("integrity: no error detected");
		return 1;
	}else{
		//printf("integrity: 0x%x expected but is 0x%x\n",checksum,buf[len-1]);
#if STRICT_DEBUGGING>0
		asm("bkpt");
		if(mcutracer_checksum(buf,len)){
			asm("nop");
		}
#endif
		return 0;
	}
}

int mcutracer_decode_string_precheck(unsigned char *data, int len){
	return mcutracer_slave_decode_string(data,len);
}

void mcutracer_memcpy(unsigned char *start,int pos){
	int length=mcutracer_rec_buffer_pos-pos;
	uint32_t upcount=0;
	while(length--){
		mcutracer_rec_buffer[upcount]=mcutracer_rec_buffer[pos+upcount];
		upcount++;
	}
	mcutracer_rec_buffer_pos=mcutracer_rec_buffer_pos-pos;
}

uint32_t mcu_tracer_rec_buf_remaining_size(void){
    return MCU_TRACER_RX_BUF_SIZE-mcutracer_rec_buffer_pos-1;
}

void mcu_tracer_rec_submit(uint8_t* buf, uint32_t len){
	//Copy data in our area, so we can iterate over it
	uint32_t remaining_size=mcu_tracer_rec_buf_remaining_size();
	if(remaining_size>(mcutracer_rec_buffer_pos+len)){
		memcpy(&mcutracer_rec_buffer[mcutracer_rec_buffer_pos],buf,len);
		mcutracer_rec_buffer_pos=mcutracer_rec_buffer_pos+len;
	}else{
        memcpy(&mcutracer_rec_buffer[mcutracer_rec_buffer_pos],buf,remaining_size);
        mcutracer_rec_buffer_pos=mcutracer_rec_buffer_pos+remaining_size;
	}
}

//delicate function, handle with respect
//laststartbyte must be set by every continue if variables should be saved
void mcu_tracer_rec_portionierer_process(void){
	int startpos_abs,endpos_abs;


	//nothing to do, return (we need at least 3 bytes, so we have a valid message to process
	if(mcutracer_rec_buffer_pos<3){
	    //we have too little data (<3 bytes) so processing it doesn't make sense.
	    return;
	}

	startpos_abs=0;
	int skip_find_end=0; //indicates how many start bytes should be skipped in the end search
	int laststartbyte=0; //0 = no save
	while(1){
	    //looking for startbyte - function accepts negative length and returns than -1, so the remaining buffer has not to be checked
	    int startpos_rel=mcu_tracer_find_first_startbyte(&mcutracer_rec_buffer[startpos_abs],mcutracer_rec_buffer_pos-startpos_abs);
	    if(startpos_rel==-1){
	        //no startbyte was found, we delete all
	        if(laststartbyte==0){
	            mcutracer_rec_buffer_pos=0;
	        }else{
	            int len=mcutracer_rec_buffer_pos-laststartbyte;
	            if(len>MCU_TRACER_RX_FLUSH){
	                //recieved string is too large, we need to reset
	                mcutracer_rec_buffer_pos=0;
	            }else{
	                //copy so that the startbyte is the first in the mem
                    memcpy(&mcutracer_rec_buffer[0],&mcutracer_rec_buffer[laststartbyte],len);
                    mcutracer_rec_buffer_pos=len;
	            }
	        }
	        return;
	    }

	    startpos_abs=startpos_abs+startpos_rel;
	    int endpos_rel;
	    endpos_rel=mcutracer_find_end_startbyte(&mcutracer_rec_buffer[startpos_abs],mcutracer_rec_buffer_pos-startpos_abs,skip_find_end);
	    if(endpos_rel==-1){
	        //we increment the starting position by one, to hope to find a second start byte, where it makes more sense
	        laststartbyte=startpos_abs;
	        startpos_abs++;
	        skip_find_end=0;
	        continue;
	    }
	    endpos_abs=endpos_rel+startpos_abs;
        //the string is the last one, so we can decode it
	    int len=endpos_abs-startpos_abs;

        int ret2=mcutracer_decode_string_precheck(&mcutracer_rec_buffer[startpos_abs],len);
        if(ret2==-1){
            //there is more data to come
            //start a new search and increase skip by one
            skip_find_end++;
            laststartbyte=startpos_abs;
            continue;
        }else{
            //data can be destroyed or has been used
            startpos_abs=endpos_abs;
            skip_find_end=0;
            laststartbyte=0;
            continue;
        }
        return;

	}

}


void mcu_tracer_init(void){
    //mcu_tracer_fill_variables();
    //mcu_tracer_fill_func();
}

void mcu_tracer_tick(void){
    //check that cdc is online
    if(!tud_ready()) return;
    //tick the time
    mcu_tracer_time_tick();
    //Look if we have any data, to add to recieve buffer
    mcu_tracer_usart_pollfordata();
    //execute portionierer
    mcu_tracer_rec_portionierer_process();
    //flush sendbuffer, answers to master
    mcutracer_sendbuffer_flush();
}

//void mcu_tracer_send_mutex_get(void){
//	while(mcu_tracer_mutex_send){
//		osDelay(1);
//	}
//	if(mcu_tracer_mutex_send==0){
//		mcu_tracer_mutex_send=1;
//	}
//}
//
//void mcu_tracer_send_mutex_giveback(void){
//	mcu_tracer_mutex_send=0;
//}
