/*
 * mcu_tracer_master.h
 *
 *  Created on: 10.03.2020
 *      Author: Michael
 */

#ifndef MCU_TRACER_MASTER_H_
#define MCU_TRACER_MASTER_H_

#define MCU_TRACER_DATA_TYPE_INT 	1
#define MCU_TRACER_DATA_TYPE_FLOAT 	2
#define MCU_TRACER_DATA_TYPE_BOOL	3


#define MCU_TRACER_READ    			1
#define MCU_TRACER_READWRITE    	0

#define MCU_TRACER_LOADONPOWERUP_OFF 0
#define MCU_TRACER_LOADONPOWERUP_ON  1

//1 Turns on float support, zero turns it off to save memory
#define MCU_TRACER_FLOAT_SUPPORT 1



#define MCU_TRACER_STARTBYTE 						(0b10100101)
#define ORDER_0_PING 								(0b00000000)/* Check if available */
#define ORDER_1_INIT								(0b00000001)/* Init */
#define ORDER_2_TRANSFER_ALL_VALUES					(0b00000010)/* All values to transfer */
#define ORDER_3_UPDATE_SINGLE_VALUE					(0b00000011)/* Update value */
#define ORDER_4_REQ_SINGLE_VALUE					(0b00000100)/* Request single Value*/
#define ORDER_8_INIT_FUNCTION						(0b00001000)/* Init Functions */
#define ORDER_9_EXEC_FUNC  							(0b00001001)/* Execute Functions (no parameter) */
#define ORDER_F0_SUPPLIER_INFO  					(0xF0)		/* Supplier Information */
#define ORDER_F1_UNIQUE_ID   					   	(0xF1)		/* Unique ID */
#define ORDER_FD_LOG		   					   	(0xFD)		/* Execute Functions (no parameter) */
#define ORDER_0xFE									(0xFE)		/* Freeform Mesage from MCU to PC */
#define ORDER_0xFF									(0xFF)		/* Emergency Stop */

typedef struct set_variables{
	char label[MCU_TRACER_SETVARIABLES_LABEL_LENGTH];
	uint8_t type; //0 notused (terminator), 1 long, 2 float, 3 toogle, 4 unsigned long
	uint8_t rw;
	uint8_t loadonpowerup;
	union{
		int32_t *data_l;
		float *data_f;
	};
	union{
		int32_t data_setmin_i;
		float data_setmin_f;
	};
	union{
		int32_t data_setmax_i;
		float data_setmax_f;
	};
	void (*callback)(void);
} mcu_tracer_var_t;




typedef struct functions{
	char label[MCU_TRACER_FUNCTIONS_LABEL_LENGTH];
	void (*func_ptr)(void);
} mcu_tracer_func_t;

typedef union union_int_float {
   int32_t i;
   float f;
} union_int_float_t;

//Function prototypes
void mcu_tracer_write_2byte_var(int32_t var);
void mcu_tracer_write_4byte_var(int32_t var);
void mcu_tracer_write_8byte_var(int64_t var);
void mcutracer_sendbuffer_flush(void);
int mcutracer_slave_decode_string(unsigned char* buf, int len);
int mcutracer_decode_string_precheck(unsigned char *data, int len);
uint8_t mcutracer_checksum(uint8_t *data, int length);

uint8_t mcu_tracer_find_variable_by_name(char *name, uint8_t *id);
uint8_t mcu_tracer_find_funcid_by_name(char *name, uint8_t *id);

void mcu_tracer_bind_variable_int32(uint8_t id, int32_t *var_ptr);
void mcu_tracer_bind_variable_float32(uint8_t id, float *var_ptr);
void mcu_tracer_bind_callback(uint8_t id, void (*callback)(void));

void monitor_master_test(void);
void mcu_tracer_master_wait_tx_complete_os(void);
void mcu_tracer_master_attach_single_data_req(uint8_t* data, uint16_t* pos, uint16_t maxlen, uint16_t req_number);
void mcu_tracer_master_attach_simple_request_order(uint8_t* data, uint16_t* pos, uint16_t maxlen, uint8_t order);

void mcutracer_submit_string(uint8_t* buf, uint32_t len);
void mcu_tracer_rec_portionierer_process(void);
void mcu_tracer_rec_submit(uint8_t* buf, uint32_t len);
uint32_t mcu_tracer_rec_buf_remaining_size(void);

void mcu_tracer_write_serial(char data);
void mcu_tracer_write_checksum(void);
void mcu_tracer_write_string(uint8_t* data);

void mcu_tracer_msg(const char* msg);
void mcu_tracer_write_string(uint8_t* data);

//mutexes to avoid sendbuffer corruption
void mcu_tracer_send_mutex_get(void);
void mcu_tracer_send_mutex_giveback(void);

void mcu_tracer_write_4byte_var(int32_t var);


void mcu_tracer_init(void);
void mcu_tracer_tick(void);
// extern variables
extern const mcu_tracer_var_t		mcutracer_variables[MCU_TRACER_MAXSETVARIABLES];
extern const mcu_tracer_func_t 	    mcutracer_functions[MCU_TRACER_NUM_FUNCTIONS];
extern uint8_t             mcutracer_send_buf[2][MCU_TRACER_TX_BUF_SIZE];

/*
The Dataframe format is the following:

Master (PC->MCU)
Startbye (0b10100101 or 0xA5) + Order + Data + SUM(XOR)
Order0: Check if available
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 0000)
-SUM(XOR)
MCU->PC:
-Startbyte (1 Byte)
-Order (0b0000 0000)
-SUM(XOR)


Order 1: Init
Requests data from MCU: Type=0b0000 0001
Example: The datastreem looks like this
0b10100101 0b00000001 0xb10100100
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 0001)
-SUM(XOR)
MCU->PC:
-Startbyte (1 Byte)
-Order (0b0000 0001)
-Type. (1 Byte)
--Terminator 	0 (Last element, all other values need to be send but are ignored)
--Int    	1
--Float  	2
--Bool	 	3
-R/W (1 Byte)
--Read/Write	0
--Read only	1
-Desciptor (0 Terminated)
-SUM(XOR)


Order 2: All values to transfer
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 0010)
-SUM(XOR)
MCU->PC
-Startbyte(1 Byte)
-Order (0b0000 0010)
-Value (4 Bytes)
-SUM(XOR)


Order3: Update value
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 0011)
-Arrynumber (2 bytes)
-Value (4 Bytes)
-SUM(XOR)
MCU->PC
-Startbyte (1 Byte)
-Order (0b0000 0011)
-Arrynumber (2 bytes)
-Value (4 Bytes)
-SUM(XOR)

Order4: Single value request
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 0011)
-Arrynumber (2 bytes)
-SUM(XOR)

MCU->PC
-Startbyte (1 Byte)
-Order (0b0000 0011)
-Arrynumber (2 bytes)
-Value (4 Bytes)
-SUM(XOR)


Order 8: Init Functions
MCU->PC
-Startbyte (1 Byte)
-Order (0b0000 1000)
-SUM(XOR)
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 1000)
-ID (1 Byte)
--Terminator 	0 (Last element, all other values need to be send but are ignored)
--Function 1    	1
--Function 2  		2
--Function n	 	n
-Desciptor (0 Terminated)
-SUM(XOR)


Order 9: Execute Functions (no parameter)
PC->MCU
-Startbyte (1 Byte)
-Order (0b0000 1001)
-Function n (1 Byte)
-SUM(XOR)
MCU->PC
-Startbyte (1 Byte)
-Order (0b0000 1001)
-Function n (1 Byte)
-Function status (1 Byte)
--OK				1
--FAIL				not 1
-SUM(XOR)


Order: 0xFE: Freeform Mesage from MCU to PC
PC->MCU:
-No possibility to request.
MCU->PC
-Startbyte (1 Byte)
-Order (0xFE)
-Message
-Terminator(1)
-SUM(XOR)


Order: 0xFF: Emergency Stop
PC->MCU
-Startbyte (1 Byte)
-Order (0b1111 1111)
-SUM(XOR)
MCU->PC
-Startbyte (1 Byte)
-Order (0b1111 1111)
-SUM(XOR)

 */


#endif /* MCU_TRACER_MASTER_H_ */
