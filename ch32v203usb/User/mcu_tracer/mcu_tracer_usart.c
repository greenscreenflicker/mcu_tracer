/*
 * mcu_tracer_usart.c
 *
 *  Created on: May 20, 2020
 *      Author: Michael
 */

#include "unified.h"

uint8_t UASARTrecieve[2][UART_RECIEVESIZE];
uint8_t RxBufferpinpong=0;

void DMA_INIT(void)
{
    DMA_InitTypeDef DMA_InitStructure = {0};
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    mcutracer_send_buf[0][0]=0;
    DMA_DeInit(DMA1_Channel4);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART1->DATAR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)mcutracer_send_buf[0][0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = 1;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &DMA_InitStructure);

    DMA_DeInit(DMA1_Channel5);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART1->DATAR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)&UASARTrecieve[RxBufferpinpong][0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize =  sizeof(UASARTrecieve[0]);
    DMA_Init(DMA1_Channel5, &DMA_InitStructure);
}

/*********************************************************************
 * @fn      USARTx_CFG
 *
 * @brief   Initializes the USART1peripheral.
 *
 * @return  none
 */






void mcu_tracer_usart_pollfordata(void) {
	//no error, process data
    uint8_t buf[64];
    uint32_t remaining_size=mcu_tracer_rec_buf_remaining_size();
    if(remaining_size>64) remaining_size=64;
    uint8_t numbytes=tud_cdc_read(buf, remaining_size);
	if (numbytes) {
		uint8_t *bufferaddr = &buf[0];
		mcu_tracer_rec_submit(bufferaddr, numbytes);
		//tud_cdc_n_write_str(0, "Hello");
	}
}
