/*
 * mcu_tracer_i2cmem.h
 *
 *  Created on: Aug 31, 2020
 *      Author: Michael
 */

#ifndef INC_DPS_MCU_TRACER_MCU_TRACER_I2CMEM_H_
#define INC_DPS_MCU_TRACER_MCU_TRACER_I2CMEM_H_


void i2c_write(uint8_t address, uint32_t nBytes, uint8_t *data);
void i2c_write_nohandle(uint32_t nBytes, uint8_t *data);
void eeprom_write_addr_nohandle(uint32_t memaddr);
void eeprom_write_addr_handle(uint32_t memaddr);
void i2c_read(uint8_t address, uint32_t nBytes, uint8_t *data);
uint32_t eeprom_able_to_write_poll(void);
void eeprom_dma_init(void);
void eeprom_dma_read_datachunk(uint32_t memaddr, uint32_t size, uint8_t* data);
void eeprom_dma_write_datachunk(uint32_t memaddr, uint32_t size, uint8_t* data);
void eeprom_check4errorsANDreset(void);

#endif /* INC_DPS_MCU_TRACER_MCU_TRACER_I2CMEM_H_ */
