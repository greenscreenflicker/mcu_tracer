/*
 * mcu_tracer_unified.h
 *
 *  Created on: 18 Aug 2023
 *      Author: mchhe
 */

#ifndef USER_INC_MCU_TRACER_MCU_TRACER_UNIFIED_H_
#define USER_INC_MCU_TRACER_MCU_TRACER_UNIFIED_H_

#include "mcu_tracer/mcu_tracer_varsave.h"
#include "mcu_tracer/mcu_tracer_appspec.h"
#include "mcu_tracer/mcu_tracer_core.h"
#include "mcu_tracer/mcu_tracer_usart.h"
#include "mcu_tracer/mcu_tracer_i2cmem.h"
#include "mcu_tracer/mcu_tracer_task.h"

// FreeRTOS static stacks
#define CDC_STACK_SIZE 150
//minium stack size: 500
#define USB_STACK_SIZE 150

#define TASK_PRIORTIY_USB 2
#define TASK_PRIORTIY_MCUTRACER 1

#endif /* USER_INC_MCU_TRACER_MCU_TRACER_UNIFIED_H_ */
