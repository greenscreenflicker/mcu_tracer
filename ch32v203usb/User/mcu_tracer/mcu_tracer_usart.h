/*
 * mcu_tracer_usart.h
 *
 *  Created on: May 20, 2020
 *      Author: Michael
 */

#ifndef MCU_TRACER_USART_H_
#define MCU_TRACER_USART_H_


//Function Prototypes
void mcu_tracer_usart_init(void);
void mcu_tracer_usart_pollfordata(void);
void mcu_tracer_usart_send(const uint8_t *data, uint32_t len);
void mcu_tracer_usart_txdone_wait(void);
uint32_t mcu_tracer_usart_recieved_bytes(void);


#endif /* MCU_TRACER_USART_H_ */
