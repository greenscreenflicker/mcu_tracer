/*
 * mcu_tracer_varsave.c
 *
 *  Created on: 23.05.2020
 *      Author: Michael
 */

#include "unified.h"

/*
 * The variable structure looks the following:
 * 1. Page(128): Identification
 * 2. Page(128): Variables
 * 3. Page(128): Times with uint64_t (16x)
 * 4+2n+0. Page(128):
 * 	-Time: uint64_t
 * 	-Message: (120 chars)
 * 4+4n+1: Variable snapshot
 */

#if I2C_MEM_ENABLE > 0

const char eeprom_log_inited[]="Reset LOGs.";

void eeprom_write_portion(uint32_t memaddr, uint32_t size, uint8_t* data, uint32_t portion){
	uint32_t alreadysend=0;
	//eeprom_check4errorsANDreset();

	while(1){
		uint32_t remaining=size-alreadysend;
		uint32_t sendout=remaining;
		if(sendout>portion){
			sendout=portion;
		}
		while(!eeprom_able_to_write_poll()){
			vTaskDelay(1);
		}
		//osDelay(5);
		eeprom_dma_write_datachunk(memaddr+alreadysend, sendout, data+alreadysend);
		alreadysend=alreadysend+sendout;
		if(alreadysend>=size) return;
	}
}

//char str[64]; //debug

void eeprom_write_portion_check(uint32_t memaddr, uint32_t size, uint8_t* data, uint32_t portion){
	uint32_t alreadysend=0;
	//eeprom_check4errorsANDreset();
	uint8_t* check=pvPortMalloc(portion);
	while(!eeprom_able_to_write_poll()){
		vTaskDelay(1);
	}
	int retry=3;
	while(1){
		memset(check,data[0]+1,portion);
		//sprintf(str, "check address: %p", check); //debug
		//mcu_tracer_log_interrupt((char*) str); //debug
		if(check[0]!=(data[0]+1)){
			mcu_tracer_msg("eeprom write portion error");
			return;
		}
		uint32_t remaining=size-alreadysend;
		uint32_t sendout=remaining;
		if(sendout>portion){
			sendout=portion;
		}
		uint8_t *datawrite=data+alreadysend;
		//osDelay(5);
		eeprom_dma_write_datachunk(memaddr+alreadysend, sendout, datawrite);
		while(!eeprom_able_to_write_poll()){
			vTaskDelay(1);
		}
		eeprom_dma_read_datachunk(memaddr+alreadysend, sendout, check);
		if(memcmp(datawrite,check,sendout)==0){
			alreadysend=alreadysend+sendout;
			if(alreadysend>=size) {
				vPortFree(check);
				return;
			}
		}else{
			retry--;
			if(!retry){
				mcu_tracer_msg("i2C Memory Write error error");
				asm("bkpt");
			}
		}

	}
}


void eeprom_write_portion_checkbuffer(uint32_t memaddr, uint32_t size, uint8_t* data, uint8_t *cmp, uint32_t portion){
	uint32_t alreadysend=0;
	//eeprom_check4errorsANDreset();
	uint8_t* check=cmp;
	while(!eeprom_able_to_write_poll()){
		vTaskDelay(1);
	}
	int retry=3;
	while(1){
		memset(check,data[0]+1,portion);
		if(check[0]!=(data[0]+1)) asm("bkpt");
		uint32_t remaining=size-alreadysend;
		uint32_t sendout=remaining;
		if(sendout>portion){
			sendout=portion;
		}
		uint8_t *datawrite=data+alreadysend;
		//osDelay(5);
		eeprom_dma_write_datachunk(memaddr+alreadysend, sendout, datawrite);
		while(!eeprom_able_to_write_poll()){
			vTaskDelay(1);
		}
		eeprom_dma_read_datachunk(memaddr+alreadysend, sendout, check);
		if(memcmp(datawrite,check,sendout)==0){
			alreadysend=alreadysend+sendout;
			if(alreadysend>=size) return;
		}else{
			retry--;
			if(!retry){
				mcu_tracer_msg("i2C Memory Write error error");
				asm("bkpt");
			}
		}

	}
}


/*
void eeprom_write_i2c_mem(void){
	//transfer address and next data
	uint8_t data[2]={0,0xAA};
	i2c_write(I2C_EEPROM_I2CADDR,2,&data[0]);
}

void eeprom_read_i2c_mem(uint32_t addr, uint8_t len, uint8_t* data){
	uint8_t addrstr[1]={addr&0xFF};
	i2c_write(I2C_EEPROM_I2CADDR,1,&addrstr[0]);
	i2c_read(I2C_EEPROM_I2CADDR, len, data);
}
*/



void eeprom_firstpage_check(void){

	//if this function keeps making problems, uncommend this in head4
	//configASSERT( ( pxLink->xBlockSize & xBlockAllocatedBit ) != 0 );
	//configASSERT( pxLink->pxNextFreeBlock == NULL );

	i2cmem_first_page_t* reference=pvPortMalloc(sizeof(i2cmem_first_page_t));
	if(!reference) asm("bkpt");
	i2cmem_first_page_t* cmp=pvPortMalloc(sizeof(i2cmem_first_page_t));
	if(!cmp) asm("bkpt");
	mcutracer_firstpage_information_fill(reference);

	assert_param(sizeof(i2cmem_first_page_t)<129);
	eeprom_dma_read_datachunk(0, sizeof(i2cmem_first_page_t), (uint8_t*) cmp);
	int result=memcmp(reference,cmp,sizeof(i2cmem_first_page_t));

	if(result==0){
		//equal, the eeprom is well known
		//load variables from eeprom, as we already have configured the memory
		mcu_tracer_eeprom_varload();
		vPortFree(cmp); //no need for compare variable anymore
		vPortFree(reference);
	}else{
		//not equal (we have a new memory)
		mcu_tracer_blank_pages(2,I2C_EEPROM_PAGES-2);
		mcu_tracer_eeprom_varstore();
		//write new identifier at last, so we make sure previous steps were successful.
		eeprom_write_portion_check(I2C_EEPROM_1PAGE_ADDR, sizeof(i2cmem_first_page_t), (uint8_t*) reference, I2C_EEPROM_PAGEWRITE_SIZE);
		vPortFree(cmp); //no need for compare variable anymore
		vPortFree(reference);
	}
	mcu_tracer_time_load();
	mcu_tracer_log_load_current_logpos();
	if(result){
		mcu_tracer_log((char*)&eeprom_log_inited[0]);
	}

}

void mcu_tracer_eeprom_init(void){
#if I2C_MEM_ENABLE>0
	eeprom_dma_init();
	eeprom_firstpage_check();
#endif
}

void mcu_tracer_copy_secondpage(i2cmem_second_page_t *sec){
	if(!sec) asm("bkpt");
	uint32_t i=0;
	assert_param(MCU_TRACER_VARIABLES<33);
	memset(sec,0,sizeof(i2cmem_second_page_t));
	while(i<MCU_TRACER_VARIABLES){
		if(mcutracer_variables[i].data_l){
			(sec->data[i])=*(mcutracer_variables[i].data_l);
		}else{
			return;
		}
		i++;
	}
}

void mcu_tracer_load_vars_from_second_page(i2cmem_second_page_t *sec){
	if(!sec) asm("bkpt");
	uint32_t i=0;
	assert_param(MCU_TRACER_VARIABLES<33);
	while(i<MCU_TRACER_VARIABLES){
		if(mcutracer_variables[i].data_l){
			if(mcutracer_variables[i].loadonpowerup){
				*(mcutracer_variables[i].data_l)=sec->data[i];
			}
		}else{
			return;
		}
		i++;
	}
}

void mcu_tracer_eeprom_varload(void){
	i2cmem_second_page_t* second=pvPortMalloc(sizeof(i2cmem_second_page_t));
	if(!second) asm("bkpt");
	eeprom_dma_read_datachunk(I2C_EEPROM_2PAGE_ADDR, I2C_EEPROM_PAGESIZE, (uint8_t*) second);
	mcu_tracer_load_vars_from_second_page(second); //load variables from eeprom
	vPortFree(second);
}

const char logvarstore_msg[]="Variables stored";

void mcu_tracer_eeprom_varstore(void){
	i2cmem_second_page_t* second=pvPortMalloc(I2C_EEPROM_PAGESIZE);
	if(!second) asm("bkpt");
	mcu_tracer_copy_secondpage(second); //load variables from eeprom
	eeprom_write_portion_check(I2C_EEPROM_2PAGE_ADDR,I2C_EEPROM_PAGESIZE,(uint8_t*) second, I2C_EEPROM_PAGEWRITE_SIZE);
	vPortFree(second);
	mcu_tracer_log((char*)&logvarstore_msg[0]);
}

uint8_t _resetdata[64],_cmpdata[64];
void mcu_tracer_blank_pages(uint32_t startpage, uint32_t numpages){
	uint8_t* data=&_resetdata[0];

	data=pvPortMalloc(I2C_EEPROM_PAGESIZE);
	if(!data) asm("bkpt");
	memset(data,0,I2C_EEPROM_PAGESIZE);

	assert_param(I2C_EEPROM_PAGESIZE%I2C_EEPROM_PAGEWRITE_SIZE == 0);
	uint32_t memaddr=startpage*I2C_EEPROM_PAGESIZE;
	uint32_t endaddr=(startpage+numpages)*I2C_EEPROM_PAGESIZE;
	while(memaddr<endaddr){
		eeprom_write_portion_checkbuffer(memaddr, I2C_EEPROM_PAGESIZE, data, &_cmpdata[0], I2C_EEPROM_PAGEWRITE_SIZE);
		memaddr+=I2C_EEPROM_PAGEWRITE_SIZE;
	}
	vPortFree(data);
}

void mcu_tracer_time_load_thirdpage(i2cmem_third_page_t* p){
	uint8_t imax=I2C_EEPROM_PAGESIZE/8;
	uint8_t i=0;
	uint64_t maxtime=0;
	uint8_t eeprompos=imax-1;
	while(i<imax){
		if(p->time[i]>maxtime){
			maxtime=p->time[i];
			eeprompos=i;
		}
		i++;
	}
	//save variables to global variables
	mcu_tracer_time_eeprom_pos=eeprompos;
	mcu_tracer_time=maxtime;
	mcu_tracer_time_to_automatic_store=I2C_EEPROM_TICKS_TO_AUTOMATIC_STORE;
}

void mcu_tracer_time_load(void){
	i2cmem_third_page_t* timeload;
	timeload=pvPortMalloc(sizeof(i2cmem_third_page_t));
	eeprom_dma_read_datachunk(I2C_EEPROM_3PAGE_ADDR, I2C_EEPROM_PAGESIZE,(uint8_t*) timeload);
	mcu_tracer_time_load_thirdpage(timeload);
	vPortFree(timeload);
}

void mcu_tracer_time_store(void){
#if I2C_MEM_ENABLE>0
	mcu_tracer_time_eeprom_pos++;
	mcu_tracer_time_eeprom_pos=mcu_tracer_time_eeprom_pos & (I2C_EEPROM_PAGESIZE/8-1);
	uint32_t addr=I2C_EEPROM_3PAGE_ADDR+mcu_tracer_time_eeprom_pos*8;
	eeprom_write_portion_check(addr,8,(uint8_t*) &mcu_tracer_time, I2C_EEPROM_PAGEWRITE_SIZE);
#endif
}

void mcu_tracer_time_automatic_store(void){
#if I2C_MEM_ENABLE>0
	if(mcu_tracer_time_to_automatic_store>0) mcu_tracer_time_to_automatic_store--;
	if(mcu_tracer_time_to_automatic_store>I2C_EEPROM_TICKS_TO_AUTOMATIC_STORE){
		asm("bkpt");
	}
	if(mcu_tracer_time_to_automatic_store==0){
		mcu_tracer_time_store();
		mcu_tracer_time_to_automatic_store=I2C_EEPROM_TICKS_TO_AUTOMATIC_STORE;
	}
#endif
}


//Logmessages
volatile uint8_t mcu_tracer_log_mutex;
i2cmem_logmsg_t mcu_tracer_logmsg;
i2cmem_logmsg_t mcu_tracer_logmsg_int;
uint32_t mcu_tracer_logposition=0;

void mcu_tracer_logpos_inc(uint32_t *logpos){
	(*logpos)++;
	if((*logpos)>=EEPROM_LOGENTRIES)(*logpos)=0; //overrun
}
void mcu_tracer_log_mutex_aquire(void){
	while(mcu_tracer_log_mutex){
		osDelay(1);
	}
	mcu_tracer_log_mutex=1;
}

void mcu_tracer_log_mutex_giveback(void){
	mcu_tracer_log_mutex=0;
}

void mcu_tracer_log_fill(i2cmem_logmsg_t *log, uint8_t *msg){
	log->time=mcu_tracer_time;
	if(!(log->time)){
		log->time=1;
	}
	strlcpy(((char*) &log->log[0]),((char*) &msg[0]),I2CMEM_LOGMSG_LENGTH);
	mcu_tracer_copy_secondpage((i2cmem_second_page_t*) log->data);
}


void mcu_tracer_log_only2eeprom_write(uint8_t* msg){
	//may not be called directly!
	mcu_tracer_log_fill(&mcu_tracer_logmsg,msg);
	mcu_tracer_logpos_inc(&mcu_tracer_logposition);
	uint32_t eeprom_addr=EEPROM_LOG_ADDR(mcu_tracer_logposition);
	eeprom_write_portion_check(eeprom_addr, EEPROM_LOGSIZE,(uint8_t*) &mcu_tracer_logmsg, I2C_EEPROM_PAGEWRITE_SIZE);
}

void mcu_tracer_logint2eeprom(i2cmem_logmsg_t* lm){
	mcu_tracer_logpos_inc(&mcu_tracer_logposition);
	uint32_t eeprom_addr=EEPROM_LOG_ADDR(mcu_tracer_logposition);
	eeprom_write_portion_check(eeprom_addr, EEPROM_LOGSIZE,(uint8_t*) lm, I2C_EEPROM_PAGEWRITE_SIZE);
}

void mcu_tracer_log_load_current_logpos(void){
	uint32_t logpos=0;
	uint32_t logpossave=EEPROM_LOGENTRIES-1;
	uint64_t timemax=0;

	uint64_t timeload;

	uint32_t eeprom_addr;
	while(logpos<EEPROM_LOGENTRIES){
		eeprom_addr=EEPROM_LOG_ADDR(logpos);
		eeprom_dma_read_datachunk(eeprom_addr, sizeof(uint64_t),(uint8_t*) &timeload);
		if(timeload>timemax){
			timemax=timeload;
			logpossave=logpos;
		}else{
			break;
		}
		logpos++;
	}
	mcu_tracer_logposition=logpossave;
}

uint32_t mcu_tracer_log_load_firsttime(void){
	uint32_t logpos=0;
	uint64_t timeload=0;
	uint64_t mintime=0xFFFFFFFFFFFFFFFF;
	uint32_t posret=0;
	while(logpos<EEPROM_LOGENTRIES){
		eeprom_dma_read_datachunk(EEPROM_LOG_ADDR(logpos), sizeof(uint64_t),(uint8_t*) &timeload);
		if(timeload==0){
			return 0;
		}
		if(timeload<mintime){
			mintime=timeload;
			posret=logpos;
		}
		logpos++;
	}
	return posret;
}

void mcu_tracer_log(const char* msg){
#if I2C_MEM_ENABLE>0
	mcu_tracer_log_mutex_aquire();
	mcu_tracer_log_only2eeprom_write((uint8_t*)msg);
	mcu_tracer_msg(msg);
	mcu_tracer_log_mutex_giveback();
#else
	asm("bkpt");
#endif
}


void mcu_tracer_log_interrupt(const char* msg){
	if(0==mcu_tracer_logmsg_int.log[0]){
		mcu_tracer_log_fill(&mcu_tracer_logmsg_int,(uint8_t*)msg);
	}
}

void mcu_tracer_logbuf_submit(void){
	if(0!=mcu_tracer_logmsg_int.log[0]){
		mcu_tracer_log_mutex_aquire();
		mcu_tracer_msg((char*)mcu_tracer_logmsg_int.log);
		mcu_tracer_logint2eeprom(&mcu_tracer_logmsg_int);
		mcu_tracer_logmsg_int.log[0]=0; //mark as free;
		mcu_tracer_log_mutex_giveback();
	}
}

const char logtest_msg[]="Dummy EEPROM-Logtest.";
void mcu_tracer_log_test(void){
	mcu_tracer_log((char*)&logtest_msg[0]);
}

uint8_t mcu_tracer_single_log_write(i2cmem_logmsg_t* l){
	if(!(l->time)) return 0;
	mcu_tracer_send_mutex_get();
	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
	mcu_tracer_write_serial(ORDER_FD_LOG);
	mcu_tracer_write_8byte_var(l->time);
	mcu_tracer_write_string(l->log);
	uint32_t i;
	for(i=0; i<MCU_TRACER_MAXSETVARIABLES; i++){
		//check if we have a terminator =>if, break
		if(mcutracer_variables[i].type==0) break;
		mcu_tracer_write_4byte_var(l->data[i]);
	}
	mcu_tracer_write_checksum();
	mcu_tracer_send_mutex_giveback();
	return 1;
}
void mcu_tracer_log_eeprom_read(i2cmem_logmsg_t* l, uint16_t pos){
	uint32_t eeprom_addr=EEPROM_LOG_ADDR(pos);
	eeprom_dma_read_datachunk(eeprom_addr,     128,(uint8_t*) l);
	eeprom_dma_read_datachunk(eeprom_addr+128, 128,((uint8_t*) l)+128);
}

//To test: A5 fd 58
void mcu_tracer_log_reply(void){
	i2cmem_logmsg_t* l;
	l=pvPortMalloc(sizeof(i2cmem_logmsg_t));
	uint32_t id=mcu_tracer_log_load_firsttime();
	uint8_t cont;
	uint32_t iterations=0;
	do{
		mcu_tracer_log_eeprom_read(l, id);
		cont=mcu_tracer_single_log_write(l);
		mcu_tracer_logpos_inc(&id);
		iterations++;
		if(iterations>=EEPROM_LOGENTRIES){
			break;
		}
	}while(cont);
	vPortFree(l);
}

#endif

uint64_t mcu_tracer_time=0;
uint8_t  mcu_tracer_time_eeprom_pos=0;
uint32_t mcu_tracer_time_to_automatic_store=0;

void mcu_tracer_time_tick(void){
	mcu_tracer_time++;
	#if I2C_MEM_ENABLE > 0
	mcu_tracer_time_automatic_store();
	#endif
}
