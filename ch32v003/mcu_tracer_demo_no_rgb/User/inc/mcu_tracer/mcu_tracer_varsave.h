/*
 * mcu_tracer_varsave.h
 *
 *  Created on: 23.05.2020
 *      Author: Michael
 */

#ifndef MCU_TRACER_VARSAVE_H_
#define MCU_TRACER_VARSAVE_H_

#if I2C_MEM_ENABLE > 0
#define I2C_EEPROM_PAGESIZE 128
#define EEPROM_PAGE_SIZE I2C_EEPROM_PAGESIZE

#define I2CMEM_MANUFACTURER_LENGTH 48
#define I2CMEM_DESCRIPTION_LENGTH 32
#define I2CMEM_DATE_LENGTH 12
#define I2CMEM_TIME_LENGTH 9
#define I2CMEM_LOGMSG_LENGTH (I2C_EEPROM_PAGESIZE-8)

#define EEPROM_LOGSIZE (2*(EEPROM_PAGE_SIZE))
#define EEPROM_LOGENTRIES (((EEPROM_NUM_PAGES-4)*EEPROM_PAGE_SIZE)/(EEPROM_LOGSIZE))

#define EEPROM_LOG_ADDR(lognum) ((lognum)*EEPROM_LOGSIZE+I2C_EEPROM_LOGPAGE_ADDR)

uint32_t eeprom_able_to_write_poll(void);
void mcu_tracer_eeprom_init(void);
void mcu_tracer_eeprom_varload(void);
void mcu_tracer_eeprom_varstore(void);
void mcu_tracer_blank_pages(uint32_t startpage, uint32_t numpages);
void mcu_tracer_time_load(void);
void mcu_tracer_time_automatic_store(void);
void mcu_tracer_log_load_current_logpos(void);
void mcu_tracer_log_test(void);
void mcu_tracer_log_reply(void);
void mcu_tracer_log(const char* msg);
void mcu_tracer_log_interrupt(const char* msg);
void mcu_tracer_logbuf_submit(void);
void mcu_tracer_log_interrupt(const char* msg);
void eeprom_write_portion_checkbuffer(uint32_t memaddr, uint32_t size, uint8_t* data, uint8_t *cmp, uint32_t portion);

typedef struct i2cmem_first{
	char manufacturer[I2CMEM_MANUFACTURER_LENGTH]; 		//"Digital Power Systems (DPS), Karlsruhe, Germany"
	char description[I2CMEM_DESCRIPTION_LENGTH];  		//"DPS-HP-480"
	uint32_t uniqueid[3];  		//Unique Chip ID
	char date[I2CMEM_DATE_LENGTH];         		//https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
	char time[I2CMEM_TIME_LENGTH];  				//https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
	uint8_t version_major;
	uint8_t version_minor;
	uint16_t pages;
} i2cmem_first_page_t;

typedef struct i2cmem_second{
	int32_t data[I2C_EEPROM_PAGESIZE/4]; //4*32
} i2cmem_second_page_t;

typedef struct i2cmem_third{
	uint64_t time[I2C_EEPROM_PAGESIZE/8]; //8*16=128
} i2cmem_third_page_t;


typedef struct i2cmem_log{
	uint64_t time;
	uint8_t log[I2CMEM_LOGMSG_LENGTH];
	int32_t data[I2C_EEPROM_PAGESIZE/4]; //4*32
} i2cmem_logmsg_t;
#endif

void mcu_tracer_time_tick(void);
extern uint64_t mcu_tracer_time;

#endif /* MCU_TRACER_VARSAVE_H_ */
