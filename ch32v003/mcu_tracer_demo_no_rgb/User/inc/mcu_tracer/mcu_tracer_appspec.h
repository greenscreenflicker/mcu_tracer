/*
 * mcu_tracer_appspec.h
 *
 *  Created on: 15.05.2020
 *      Author: Michael
 */

#ifndef MCU_TRACER_APPSPEC_H_
#define MCU_TRACER_APPSPEC_H_


//Configuration
#define MCU_TRACER_VARIABLES 32


#define I2C_MEM_ENABLE 0

#if I2C_MEM_ENABLE > 0
#define I2C_MCU4EEPROM I2C2
#define I2C_EEPROM_I2CADDR 0b10100000
#define I2C_EEPROM_PAGEWRITE_SIZE 32
#define MCU_TRACER_ENABLE_VARSAVE (1)
#define I2C_DMA_CHANNEL LL_DMA_CHANNEL_3
//INIT STRUCT
#define I2C_EEPROM_1PAGE_ADDR (I2C_EEPROM_PAGESIZE*0)
//VARIABLES
#define I2C_EEPROM_2PAGE_ADDR (I2C_EEPROM_PAGESIZE*1)
//TIME
#define I2C_EEPROM_3PAGE_ADDR (I2C_EEPROM_PAGESIZE*2)
//FREE
#define I2C_EEPROM_4PAGE_ADDR (I2C_EEPROM_PAGESIZE*3)
//LOG
#define I2C_EEPROM_LOGPAGE_ADDR (I2C_EEPROM_PAGESIZE*4)

#define I2C_EEPROM_PAGES 64
#define I2C_EEPROM_TICKS_TO_AUTOMATIC_STORE    (3000)
#else
#define MCU_TRACER_ENABLE_VARSAVE 0
#endif

#define UART_RECIEVESIZE 						(50)
#define MCU_TRACER_MAXSETVARIABLES 				(10)
#define MCU_TRACER_LOGVARIABLES 				(0)
#define MCU_TRACER_SETVARIABLES_LABEL_LENGTH    (16)
#define MCU_TRACER_FUNCTIONS_LABEL_LENGTH       (10)
#define MCU_TRACER_NUM_FUNCTIONS                (5)
#define MCU_TRACER_RX_BUF_SIZE                 (50)
#define MCU_TRACER_RX_FLUSH                     (5)
#define MCU_TRACER_TX_BUF_SIZE                 (50)

#if MCU_TRACER_LOGVARIABLES>32
#error "LOG VARIABLES ARE LIMITED TO 32"
#endif

#define MCU_TRACER_STRICT_DEBUGGING 0

//Function Prototypes
void mcu_tracer_appspec_initcomplete_callback(void);
void mcu_tracer_appspec_allupdate_callback(void);
void mcu_tracer_appspec_function_init_callback(void);
void mcutracer_rec_task_worker(void);
void mcu_tracer_pingcallback(void);
void mcu_tracer_emergency_callback(void);
void mcu_tracer_fill_variables(void);
void mcu_tracer_fill_func(void);

#if I2C_MEM_ENABLE >0
void mcu_tracer_copy_secondpage(i2cmem_second_page_t *sec);
#endif

uint32_t mcu_tracer_uniqueid0(void);
uint32_t mcu_tracer_uniqueid1(void);
uint32_t mcu_tracer_uniqueid2(void);

//eeprom


//global variables

extern const char firstpage_manufacturer[];
extern const char firstpage_description[];
extern const char firstpage_builddate[12];
extern const char firstpage_buildtime[8];
extern const int32_t firstpage_version_major;
extern const int32_t firstpage_version_minor;


#endif /* MCU_TRACER_APPSPEC_H_ */
