/*
 * taskcaller_app.h
 *
 *  Created on: 18 Aug 2023
 *      Author: mchhe
 */

#ifndef USER_INC_TASKCALLER_TASKCALLER_APP_H_
#define USER_INC_TASKCALLER_TASKCALLER_APP_H_

void taskcaller_fill(void);

#endif /* USER_INC_TASKCALLER_TASKCALLER_APP_H_ */
