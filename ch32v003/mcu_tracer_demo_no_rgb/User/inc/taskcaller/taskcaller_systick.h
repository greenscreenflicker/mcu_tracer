/*
 * taskcaller_systick.h
 *
 *  Created on: 15 May 2023
 *      Author: mchhe
 */

#ifndef TASKCALLER_INC_TASKCALLER_SYSTICK_H_
#define TASKCALLER_INC_TASKCALLER_SYSTICK_H_

//Details can be found at:
//QingKeV2_Processor_Manual-1.PDF from wch

//Software interrupt trigger enable (SWI).
#define STK_SWIE    (1<<31)
//Auto-reload Count enable bit.
#define STK_STRE    (1<<3)
//Counter clock source selection bit.
#define STK_STCLK   (1<<2)
//Counter interrupt enable control bit.
#define STK_STIE    (1<<1)
//System counter enable control bit.
#define STK_STE     (1<<0)

void taskcaller_init(void);
void SysTick_Reset_Flag(void);
void SysTick_Set_Time(uint32_t time);
void SysTick_Handler(void) __attribute__((interrupt("WCH-Interrupt-fast")));



#endif /* TASKCALLER_INC_TASKCALLER_SYSTICK_H_ */
