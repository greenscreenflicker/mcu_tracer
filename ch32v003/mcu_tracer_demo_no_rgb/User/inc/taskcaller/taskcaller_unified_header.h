/*
 * taskcaller_unified_header.h
 *
 *  Created on: 6 May 2023
 *      Author: mchhe
 */

#ifndef TASKCALLER_INC_TASKCALLER_UNIFIED_HEADER_H_
#define TASKCALLER_INC_TASKCALLER_UNIFIED_HEADER_H_

#include <ch32v00x.h>
#include <stdio.h>
#include <stdint.h>
#include <inc/taskcaller/taskcaller_core.h>
#include <inc/taskcaller/taskcaller_systick.h>
#include "inc/taskcaller/taskcaller_app.h"

//Function Prototypes
void taskcaller_init(void);

#endif /* TASKCALLER_INC_TASKCALLER_UNIFIED_HEADER_H_ */
