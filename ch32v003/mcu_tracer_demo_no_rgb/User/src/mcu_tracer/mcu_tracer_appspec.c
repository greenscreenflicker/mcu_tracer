/*
 * mcu_tracer_appspec.c
 *
 *  Created on: 18.03.2020
 *      Author: Michael
 *
 *      Implements application specific routines, required for the MCU_TRACER Master Device
 */
#include "unified.h"

//standard functions
const char mcu_tracer_time_in_variables_label[] = "SysTicks";



const char debug_dummfunction_msg[] = "Dummy Function successfully executed";
void mcu_tracer_dummyfunc(void) {
	mcu_tracer_msg(&debug_dummfunction_msg[0]);
}

void mcu_tracer_fill_variables(void) {
	uint16_t fill = 0;

	mcutracer_variables[fill].type = MCU_TRACER_DATA_TYPE_INT;
	mcutracer_variables[fill].rw = MCU_TRACER_READ;
	mcutracer_variables[fill].data_l = (int32_t*) &mcu_tracer_time;
	strlcpy(mcutracer_variables[fill].label,
			&mcu_tracer_time_in_variables_label[0],
			MCU_TRACER_SETVARIABLES_LABEL_LENGTH);
	mcutracer_variables[fill].callback = NULL;
	fill++;
}

void mcu_tracer_fill_func(void) {
	int fipo = 0;
	strlcpy(mcutracer_functions[fipo].label, "Dummy Function",
	MCU_TRACER_FUNCTIONS_LABEL_LENGTH);
	mcutracer_functions[fipo].func_ptr = mcu_tracer_dummyfunc;
	fipo++;

}

//Callback called, when the initialisation is complete
void mcu_tracer_appspec_initcomplete_callback(void) {
	//mcu_tracer_find_variable_by_name((char*) &mcu_tracer_appsec_output_voltage[0], &id)
	//mcu_tracer_bind_variable_float32(id, &update_display_voltage);
	//mcu_tracer_bind_callback(id, display_voltage_avg);
	//mcu_tracer_initcompleted=MCU_TRACER_REQ_STATE_MASCHINE_INIT_FUNC;
	//mcu_tracer_req_task_fast_update_trigger();
	//must be set to one after init is completed successfully
	//counter_reinit=0;
}

void mcu_tracer_appspec_function_init_callback(void) {
	//mcu_tracer_initcompleted=MCU_TRACER_REQ_STATE_MASCHINE_CONTINOUS_VAR_REQ;
	//mcu_tracer_req_task_fast_update_trigger();
	//counter_reinit=0;
}

//callback is called, when all values are updated.
void mcu_tracer_appspec_allupdate_callback(void) {
	//counter_no_update=0;
	//counter_reinit=0;
//	mcu_tracer_req_task_fast_update_trigger();
}

void mcu_tracer_appspec_custom_updates(uint8_t *data, uint16_t *pos,
		uint16_t maxlen) {
	//not required for slave
}

void mcu_tracer_pingcallback(void) {
	//
}

void mcu_tracer_emergency_callback(void) {
	//shutdown();
}

//ToDo: Add CH32V003 uniqute IDS
uint32_t mcu_tracer_uniqueid0(void) {
	return *(uint32_t*)0x1FFFF7E8;
}
uint32_t mcu_tracer_uniqueid1(void) {
	return *(uint32_t*)(0x1FFFF7EC);
}
uint32_t mcu_tracer_uniqueid2(void) {
	return *(uint32_t*)(0x1FFFF7F0);
}

void mcutracer_userfunc(void) {
	//
}


//typedef struct i2cmem_first{
//	char manufacturer[I2CMEM_MANUFACTURER_LENGTH]; 		//"Digital Power Systems (DPS), Karlsruhe, Germany"
//	char description[I2CMEM_DESCRIPTION_LENGTH];  		//"DPS-HP-480"
//	uint32_t uniqueid[3];  		//Unique Chip ID
//	char date[I2CMEM_DATE_LENGTH];         		//https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
//	char time[I2CMEM_TIME_LENGTH];  				//https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
//	uint8_t version_major;
//	uint8_t version_minor;
//	uint64_t timesaved;
//} i2cmem_first_page_t;
const char firstpage_manufacturer[] =
		"MCU TRACER DEMONSTRATOR MANUFACTURER (mainted by Digital Power Systems GmbH, Karlsruhe, Germany)";
const char firstpage_description[] = "MCU_TRACER_DEMONSTARTOR";
const char firstpage_builddate[12] = __DATE__;
const char firstpage_buildtime[8] = __TIME__;
const int32_t firstpage_version_major = 0;
const int32_t firstpage_version_minor = 0;
