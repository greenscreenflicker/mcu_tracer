/*
 * tastcaller_core.c
 *
 *  Created on: 5 May 2023
 *      Author: mchhe
 */


#include "unified.h"


#define TC_MAX_TASKS 10

// Declare a task list and the number of tasks in the list
task_t _tc_task_list[TC_MAX_TASKS];
uint16_t _tc_num_tasks = 0;

// Add a task to the task list
void tc_task_add(task_t new_task) {
    // Check if there is space in the task list
    if (_tc_num_tasks >= TC_MAX_TASKS) {
        printf("Error: maximum number of tasks exceeded.\n");
        return;
    }
    // Add the new task to the end of the task list
    _tc_task_list[_tc_num_tasks] = new_task;
    _tc_num_tasks++;

    // Sort tasks by priority (insertion sort)
    for (int i = 1; i < _tc_num_tasks; i++) {
        task_t temp_task = _tc_task_list[i];
        int j = i - 1;
        while (j >= 0 && _tc_task_list[j].priority > temp_task.priority) {
            _tc_task_list[j + 1] = _tc_task_list[j];
            j--;
        }
        _tc_task_list[j + 1] = temp_task;
    }
    //Make sure last task's function is a null pointer
}

// Deletes a task from the task list by index
void tc_task_delete(int task_index) {
    // Check if the task index is valid
    if (!tc_taskid_valid(task_index)) {
        printf("Error: invalid task index.\n");
        return;
    }
    // Shift tasks after the removed task to fill the gap
    for (int i = task_index; i < _tc_num_tasks - 1; i++) {
        _tc_task_list[i] = _tc_task_list[i + 1];
    }
    _tc_num_tasks--;
}

uint32_t tc_taskid_valid(uint16_t task_index){
    if (task_index >= _tc_num_tasks) {
        return 1;
    }
    return 0;
}

void tc_task_priority_modify(uint16_t task_index, unsigned int new_priority) {
    // Check if the task index is valid
    if (!tc_taskid_valid(task_index)) {
        printf("Error: invalid task index.\n");
        return;
    }

    // Get the current task
    task_t modified_task = _tc_task_list[task_index];

    // Update the task's priority
    modified_task.priority = new_priority;

    // Bubble up the modified task to its correct position in the sorted list
    while (task_index > 0 && _tc_task_list[task_index - 1].priority > modified_task.priority) {
        // Swap the modified task with the preceding task
        _tc_task_list[task_index] = _tc_task_list[task_index - 1];
        task_index--;
    }
    // Put the modified task in its correct position in the sorted list
    _tc_task_list[task_index] = modified_task;
}

int tc_get_lowest_time(void) {
    int lowest_time_remaining = _tc_task_list[0].time_remaining;
    for (int i = 1; i < _tc_num_tasks; i++) {
        if (_tc_task_list[i].time_remaining < lowest_time_remaining) {
            lowest_time_remaining = _tc_task_list[i].time_remaining;
        }
    }
    return lowest_time_remaining;
}

int tc_get_min_time_remaining_with_priority(unsigned int priority) {
    int min_time_remaining = INT32_MAX;
    for (int i = 0; i < _tc_num_tasks; i++) {
        if (_tc_task_list[i].priority >= priority) {
            if (_tc_task_list[i].time_remaining < min_time_remaining) {
                min_time_remaining = _tc_task_list[i].time_remaining;
            }
        }
    }
    return min_time_remaining;
}

void tc_task_delete_after_execution(uint16_t taskid){
    if (!tc_taskid_valid(taskid)) {
        printf("Error: invalid task index.\n");
    }
    _tc_task_list[taskid].interval=0;
}

int tc_is_task_repetitive(uint16_t taskid){
    if (!tc_taskid_valid(taskid)) {
        printf("Error:TaskRepetitive: invalid task index.\n");
        return -1;
    }
    return tc_is_task_repetitive_nocheck(taskid);
}

int tc_is_task_repetitive_nocheck(uint16_t taskid){
    return (_tc_task_list[taskid].interval > 0);
}

int tc_is_task_overdue(uint16_t taskid){
    if (!tc_taskid_valid(taskid)) {
        printf("Error:TaskOverdue: invalid task index.\n");
        return -1;
    }
    return tc_is_task_overdue_nocheck(taskid);
}

int tc_is_task_overdue_nocheck(uint16_t taskid){
    return (_tc_task_list[taskid].time_remaining <= 0);
}

uint16_t tc_get_taskid_by_functionpointer(void (*function)()){
    // Loop through all tasks
    for (int i = 0; i < _tc_num_tasks; i++) {
        if((_tc_task_list[i].function)==function) return i;
    }
    //if we cannot find the task, mark as invalid
    return INT16_MAX;
}

// Check the tasks for execution
void tc_worker(uint32_t passed_time) {
    // Loop through all tasks
    for (int i = 0; i < _tc_num_tasks; i++) {
        // subtract passed time from execution_at
        _tc_task_list[i].time_remaining-=passed_time;
        if (tc_is_task_overdue_nocheck(i)) {
            // Execute task's function
            _tc_task_list[i].function();
            // If task's interval is greater than 0, reschedule the task
            if (tc_is_task_repetitive_nocheck(i)){
                _tc_task_list[i].time_remaining += _tc_task_list[i].interval;
            }
            // If task's interval is 0, remove the task from the list
            else {
                tc_task_delete(i);
                // Decrement i to account for removed task
                i--;
            }
        }
    }
}
