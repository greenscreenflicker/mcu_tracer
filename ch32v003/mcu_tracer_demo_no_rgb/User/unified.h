/*
 * unified.h
 *
 *  Created on: 22 Jul 2023
 *      Author: mchhe
 */

#ifndef USER_UNIFIED_H_
#define USER_UNIFIED_H_

#include <string.h>
#include "debug.h"
#include "inc/mcu_tracer/uart.h"
#include "inc/taskcaller/taskcaller_unified_header.h"
#include "inc/mcu_tracer/mcu_tracer_unified.h"
#endif /* USER_UNIFIED_H_ */
