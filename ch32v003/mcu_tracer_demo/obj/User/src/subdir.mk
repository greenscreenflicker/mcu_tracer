################################################################################
# MRS Version: {"version":"1.8.5","date":"2023/05/22"}
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../User/src/rgbled.c 

OBJS += \
./User/src/rgbled.o 

C_DEPS += \
./User/src/rgbled.d 


# Each subdirectory must supply rules for building sources it contributes
User/src/%.o: ../User/src/%.c
	@	@	riscv-none-embed-gcc -march=rv32ecxw -mabi=ilp32e -msmall-data-limit=0 -msave-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -Wunused -Wuninitialized  -g -I"C:\Users\mchhe\OneDrive\Dokumente\git\mcu_tracer\ch32v003\mcu_tracer_demo\Debug" -I"C:\Users\mchhe\OneDrive\Dokumente\git\mcu_tracer\ch32v003\mcu_tracer_demo\Core" -I"C:\Users\mchhe\OneDrive\Dokumente\git\mcu_tracer\ch32v003\mcu_tracer_demo\User" -I"C:\Users\mchhe\OneDrive\Dokumente\git\mcu_tracer\ch32v003\mcu_tracer_demo\Peripheral\inc" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

