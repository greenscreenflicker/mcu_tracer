/*
 * rgbled.h
 *
 *  Created on: 22 Jul 2023
 *      Author: mchhe
 */

#ifndef USER_INC_RGBLED_H_
#define USER_INC_RGBLED_H_

void RGB_SIMPLE_TEST(void);
void RGB_init(void);

void RGB_LED_SET_RED(uint32_t intensity);
void RGB_LED_SET_BLUE(uint32_t intensity);
void RGB_LED_SET_GREEN(uint32_t intensity);
void RGB_MCUTRACER_LED(void);

extern int32_t mcu_tracer_RGB[3];
#endif /* USER_INC_RGBLED_H_ */
