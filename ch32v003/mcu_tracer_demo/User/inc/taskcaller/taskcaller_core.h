/*
 * taskcaller_core.h
 *
 *  Created on: 6 May 2023
 *      Author: mchhe
 */

#ifndef TASKCALLER_INC_TASKCALLER_CORE_H_
#define TASKCALLER_INC_TASKCALLER_CORE_H_

/*
 * tastcaller_core.c
 *
 *  Created on: 5 May 2023
 *      Author: mchhe
 */


#include <stdio.h>
#include <stdint.h>

#define TC_MAX_TASKS 10

// Define the task_t struct to hold task information
typedef struct {
    void (*function)(); // Pointer to the task function
    uint32_t interval; // Time between task executions
    int32_t time_remaining; // Time to execute the task (negative for tasks that should have executed in the past)
    uint_fast8_t priority; // Priority of the task (lower value = higher priority)
} task_t;


// Add a task to the task list
void tc_task_add(task_t new_task);

// Deletes a task from the task list by index
void tc_task_delete(int task_index);
void tc_task_priority_modify(uint16_t task_index, unsigned int new_priority);
int tc_get_lowest_time(void);
int tc_get_min_time_remaining_with_priority(unsigned int priority);
uint32_t tc_taskid_valid(uint16_t task_index);
int tc_is_task_repetitive(uint16_t taskid);
int tc_is_task_repetitive_nocheck(uint16_t taskid);

int tc_is_task_overdue(uint16_t taskid);
int tc_is_task_overdue_nocheck(uint16_t taskid);
// Check the tasks for execution
void tc_worker(uint32_t passed_time);


#endif /* TASKCALLER_INC_TASKCALLER_CORE_H_ */
