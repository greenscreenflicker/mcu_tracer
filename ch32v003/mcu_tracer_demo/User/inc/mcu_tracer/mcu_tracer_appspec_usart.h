/*
 * mcu_tracer_appspec_usart.h
 *
 *  Created on: May 19, 2022
 *      Author: mchhe
 */

#ifndef INC_DPS_MCU_TRACER_MCU_TRACER_APPSPEC_USART_H_
#define INC_DPS_MCU_TRACER_MCU_TRACER_APPSPEC_USART_H_

void mcu_tracer_appspec_usart_init(void);

#endif /* INC_DPS_MCU_TRACER_MCU_TRACER_APPSPEC_USART_H_ */
