/*
 * uart.h
 *
 *  Created on: 4 Jun 2023
 *      Author: mchhe
 */

#ifndef USER_DPS_UART_H_
#define USER_DPS_UART_H_

void usart_tx_dma(const uint8_t *data, uint32_t size);

#define size(a)    (sizeof(a) / sizeof(*(a)))

#endif /* USER_DPS_UART_H_ */
