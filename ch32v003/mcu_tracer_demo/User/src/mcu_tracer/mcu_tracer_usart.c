/*
 * mcu_tracer_usart.c
 *
 *  Created on: May 20, 2020
 *      Author: Michael
 */

#include "unified.h"

uint8_t UASARTrecieve[2][UART_RECIEVESIZE];
uint8_t RxBufferpinpong=0;

void DMA_INIT(void)
{
    DMA_InitTypeDef DMA_InitStructure = {0};
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    mcutracer_send_buf[0][0]=0;
    DMA_DeInit(DMA1_Channel4);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART1->DATAR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)mcutracer_send_buf[0][0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = 1;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &DMA_InitStructure);

    DMA_DeInit(DMA1_Channel5);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART1->DATAR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)&UASARTrecieve[RxBufferpinpong][0];
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize =  sizeof(UASARTrecieve[0]);
    DMA_Init(DMA1_Channel5, &DMA_InitStructure);
}

/*********************************************************************
 * @fn      USARTx_CFG
 *
 * @brief   Initializes the USART1peripheral.
 *
 * @return  none
 */
void USARTx_CFG(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure = {0};
    USART_InitTypeDef USART_InitStructure = {0};

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_USART1, ENABLE);

    /* USART1 TX-->PC0   RX-->PC1 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

    //PIN Mapping
    RCC->APB2PCENR |= RCC_AFIOEN;
    //enable uart 2 (pin remap)
    AFIO->PCFR1 |= (1<<21)|(1<<2);

    USART_Init(USART1, &USART_InitStructure);

    DMA_Cmd(DMA1_Channel4, ENABLE); /* USART1 Tx */
    DMA_Cmd(DMA1_Channel5, ENABLE); /* USART1 Rx */

    USART_Cmd(USART1, ENABLE);
}

void usart_tx_dma_str(const char* str){
    usart_tx_dma(&str[0],strlen(str)+1);
}

void usart_tx_dma(const uint8_t *data, uint32_t size){
    //wait until transfer is complete
    while(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET); /* Wait until USART1 TX DMA1 Transfer Complete */
    //clear flag (so that this transfer will be completed, before next transfer comes
    DMA_ClearFlag(DMA1_FLAG_TC4);
    //Set source addr
    DMA1_Channel4->MADDR = (u32)(&data[0]);
    //set size and activate transfer by it.
    DMA1_Channel4->CNTR = size;
}

void mcu_tracer_usart_init(void) {
    DMA_INIT();
    USARTx_CFG(); /* USART1 */
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);

}

uint32_t mcu_tracer_usart_recieved_bytes(void) {
    return (sizeof(UASARTrecieve[0])-DMA1_Channel5->CNTR);
}
//uint8_t RxBufferpinpong=0;
uint8_t* mcu_tracer_rx_pingpong_buf(uint32_t* len){
    DMA_ClearFlag(DMA1_FLAG_TC5);
    //Set source addr
    uint8_t* retaddr;
    retaddr=(uint8_t*)DMA1_Channel5->MADDR;
    *(len)=sizeof(UASARTrecieve[0])-DMA1_Channel5->CNTR;
    DMA1_Channel5->MADDR = (u32)(&UASARTrecieve[RxBufferpinpong][0]);
    RxBufferpinpong^=1;
    //set size and activate transfer by it.
    DMA1_Channel5->CNTR = sizeof(UASARTrecieve[0]);
    return retaddr;
}

void mcu_tracer_usart_pollfordata(void) {
	//checks that data is not overrun and then resets the dma
    /*if (DMA_GetFlagStatus(DMA1_FLAG_TC5) == SET) {
		mcu_tracer_usart_init();
		DMA_ClearFlag(DMA1_FLAG_TC5);
		return;
	}
	 ToDo: Extend error checking
	if (LL_USART_IsActiveFlag_NE(USART_UNIT)
			|| LL_USART_IsActiveFlag_ORE(USART_UNIT)
			|| LL_USART_IsActiveFlag_PE(USART_UNIT)) {
		//we have an error
		LL_USART_ClearFlag_NE(USART_UNIT);
		LL_USART_ClearFlag_ORE(USART_UNIT);
		LL_USART_ClearFlag_PE(USART_UNIT);
		mcu_tracer_usart_init();
		return;
	}*/

	//no error, process data
	uint32_t numbytes = mcu_tracer_usart_recieved_bytes();
	if (numbytes) {
		uint8_t *bufferaddr = mcu_tracer_rx_pingpong_buf(&numbytes);
		mcu_tracer_rec_submit(bufferaddr, numbytes);
	}

}

/// SEND SECTION
void mcu_tracer_usart_send(const uint8_t *data, uint32_t len){
    //wait until transfer is complete
    while(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET); /* Wait until USART1 TX DMA1 Transfer Complete */
    //clear flag (so that this transfer will be completed, before next transfer comes
    DMA_ClearFlag(DMA1_FLAG_TC4);
    //Set source addr
    DMA1_Channel4->MADDR = (u32)(&data[0]);
    //set size and activate transfer by it.
    DMA1_Channel4->CNTR = len;
}


void mcu_tracer_usart_txdone_wait(void) {
    //wait until transfer is complete
    while(DMA_GetFlagStatus(DMA1_FLAG_TC4) == RESET); /* Wait until USART1 TX DMA1 Transfer Complete */
}

