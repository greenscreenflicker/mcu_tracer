/*
 * mcu_tracer_eeprom.c
 *
 *  Created on: Aug 31, 2020
 *      Author: Michael
 */

#include "unified.h"

/*
 * The variable structure looks the following:
 * 1. Page(128): Identification
 * 2. Page(128): Variables
 * 3. Page(128): Times with uint64_t (16x)
 * 4+2n+0. Page(128):
 * 	-Time: uint64_t
 * 	-Message: (120 chars)
 * 4+4n+1: Variable snapshot
 */

#if I2C_MEM_ENABLE > 0

void i2c_write(uint8_t address, uint32_t nBytes, uint8_t *data){
	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
    LL_DMA_DisableChannel(I2C_DMA, I2C_DMA_CHANNEL);
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, address, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)nBytes, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);

	while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM))
	{

		if(LL_I2C_IsActiveFlag_TXIS(I2C_MCU4EEPROM))
		{

			LL_I2C_TransmitData8(I2C_MCU4EEPROM, (*data++));

		}
	}

	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
}

void i2c_write_nohandle(uint32_t nBytes, uint8_t *data){
	while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM)){
		if(LL_I2C_IsActiveFlag_TXIS(I2C_MCU4EEPROM)){
			LL_I2C_TransmitData8(I2C_MCU4EEPROM, (*data++));
			nBytes--;
			if(nBytes==0) {
				while(!(LL_I2C_IsActiveFlag_TXIS(I2C_MCU4EEPROM)));
				return;
			}
		}
	}
}


void eeprom_write_addr_nohandle(uint32_t memaddr){
	#if I2C_EEPROM_PAGES<3
	uint8_t addr;
	addr=(memaddr)&0xFF;
	i2c_write_nohandle(1,&addr);
	#else
	uint8_t addr[2];
	addr[0]=(memaddr>>8)&0xFF;
	addr[1]=(memaddr>>0)&0xFF;
	i2c_write_nohandle(2,(uint8_t *)&addr);
    #endif
}

void eeprom_write_addr_handle(uint32_t memaddr){
	while(!eeprom_able_to_write_poll()){
		osDelay(1);
	}
	#if I2C_EEPROM_PAGES<3
	uint8_t addr;
	addr=(memaddr)&0xFF;
	i2c_write(I2C_EEPROM_I2CADDR,1,&addr);
	#else
	uint8_t addr[2];
	addr[0]=(memaddr>>8)&0xFF;
	addr[1]=(memaddr>>0)&0xFF;
	i2c_write(I2C_EEPROM_I2CADDR,2,(uint8_t *)&addr);
    #endif
}

void i2c_read(uint8_t address, uint32_t nBytes, uint8_t *data){

	uint32_t i=0;
	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
    LL_DMA_DisableChannel(I2C_DMA, I2C_DMA_CHANNEL);
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, address, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)nBytes, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);

	while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM)) {
		/* Receive data (RXNE flag raised) */
		/* Check RXNE flag value in ISR register */
		if(LL_I2C_IsActiveFlag_RXNE(I2C_MCU4EEPROM))
		{
			/* Read character in Receive Data register.
  	  	  	  RXNE flag is cleared by reading data in RXDR register */
			data[i++] = LL_I2C_ReceiveData8(I2C_MCU4EEPROM);
		}
	}
	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);

}

uint32_t eeprom_able_to_write_poll(void){
	//check if we have errors, if so reset
	eeprom_check4errorsANDreset();

	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
	LL_I2C_ClearFlag_NACK(I2C_MCU4EEPROM);
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, I2C_EEPROM_I2CADDR, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)0, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);
	while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM)){
		//
	}
	if(LL_I2C_IsActiveFlag_NACK(I2C_MCU4EEPROM)){
		return 0;
	}
	return 1;
}

void eepromi2c_init(void)
{
  LL_I2C_InitTypeDef I2C_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  // this part is still not parameterized and has to be hard-coded every time
  LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_PCLK1);

  LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);


  // all code below is parameterized and can be configured via the macros in mcu_tracer_appspec.h
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(I2C_CLOCK);

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  /** I2C Initialization
  */
  I2C_InitStruct.PeripheralMode = LL_I2C_MODE_I2C;
  I2C_InitStruct.Timing = 0x00801D6E;
  I2C_InitStruct.AnalogFilter = LL_I2C_ANALOGFILTER_ENABLE;
  I2C_InitStruct.DigitalFilter = 4;
  I2C_InitStruct.OwnAddress1 = 0;
  I2C_InitStruct.TypeAcknowledge = LL_I2C_ACK;
  I2C_InitStruct.OwnAddrSize = LL_I2C_OWNADDRESS1_7BIT;
  LL_I2C_Init(I2C_MCU4EEPROM, &I2C_InitStruct);
  LL_I2C_EnableAutoEndMode(I2C_MCU4EEPROM);
  LL_I2C_SetOwnAddress2(I2C_MCU4EEPROM, 0, LL_I2C_OWNADDRESS2_NOMASK);
  LL_I2C_DisableOwnAddress2(I2C_MCU4EEPROM);
  LL_I2C_DisableGeneralCall(I2C_MCU4EEPROM);
  LL_I2C_EnableClockStretching(I2C_MCU4EEPROM);
  /** I2C Fast mode Plus enable
  */
  LL_SYSCFG_EnableFastModePlus(I2C_FASTMODE);
}

void eeprom_dma_init(void){
	eepromi2c_init();
	LL_I2C_EnableDMAReq_RX(I2C_MCU4EEPROM);
	LL_I2C_EnableDMAReq_TX(I2C_MCU4EEPROM);

	LL_DMA_SetChannelPriorityLevel(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_PRIORITY_LOW);
	LL_DMA_SetMode(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_MODE_NORMAL);
	LL_DMA_SetPeriphIncMode(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_PERIPH_NOINCREMENT);
	LL_DMA_SetMemoryIncMode(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphSize(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_PDATAALIGN_BYTE);
	LL_DMA_SetMemorySize(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_MDATAALIGN_BYTE);
}

void eeprom_dma_read_datachunk(uint32_t memaddr, uint32_t size, uint8_t* data){
	eeprom_check4errorsANDreset();
	eeprom_write_addr_handle(memaddr);
	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
    LL_DMA_DisableChannel(I2C_DMA, I2C_DMA_CHANNEL);
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, I2C_EEPROM_I2CADDR, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)size, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);
    // Disable DMA to load new length to be tranmitted
	LL_DMA_SetPeriphRequest(I2C_DMA, I2C_DMA_CHANNEL, I2C_DMA_REQ_RX);
    // set length to be transmitted
    LL_DMA_SetDataLength(I2C_DMA, I2C_DMA_CHANNEL, size);
    // configure address to be transmitted by DMA
    LL_DMA_ConfigAddresses(I2C_DMA, I2C_DMA_CHANNEL,(uint32_t)&I2C_MCU4EEPROM->RXDR,(uint32_t)&data[0],LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
    //configuration adjusted
    LL_DMA_SetDataTransferDirection(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
	// Enable DMA channel
    LL_DMA_EnableChannel(I2C_DMA, I2C_DMA_CHANNEL);
    while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM));
    LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
}

void eeprom_dma_write_datachunk(uint32_t memaddr, uint32_t size, uint8_t* data){
	LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
    LL_DMA_DisableChannel(I2C_DMA, I2C_DMA_CHANNEL);
	#if I2C_EEPROM_PAGES<3
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, I2C_EEPROM_I2CADDR, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)(size+1), LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);
	#else
	LL_I2C_HandleTransfer(I2C_MCU4EEPROM, I2C_EEPROM_I2CADDR, LL_I2C_ADDRSLAVE_7BIT, (uint32_t)(size+2), LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);
	#endif
	eeprom_write_addr_nohandle(memaddr);

    // Disable DMA to load new length to be transmitted

	LL_DMA_SetPeriphRequest(I2C_DMA, I2C_DMA_CHANNEL, I2C_DMA_REQ_TX);
    // set length to be transmitted
    LL_DMA_SetDataLength(I2C_DMA, I2C_DMA_CHANNEL, size);
    // configure address to be transmitted by DMA
    // LL_DMA_ConfigAddresses(DMAx, Channel,         SrcAddress,      DstAddress,                     Direction)
    LL_DMA_ConfigAddresses(I2C_DMA, I2C_DMA_CHANNEL,(uint32_t)&data[0],(uint32_t)&I2C_MCU4EEPROM->TXDR,LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
    //configuration adjusted
    LL_DMA_SetDataTransferDirection(I2C_DMA, I2C_DMA_CHANNEL, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
	// Enable DMA channel
    LL_DMA_EnableChannel(I2C_DMA, I2C_DMA_CHANNEL);
    while(!LL_I2C_IsActiveFlag_STOP(I2C_MCU4EEPROM)){
    	osDelay(1);
    }
    LL_I2C_ClearFlag_STOP(I2C_MCU4EEPROM);
}

void eeprom_check4errorsANDreset(void){
	if(LL_I2C_IsActiveFlag_BERR(I2C_MCU4EEPROM)){
		eeprom_dma_init();
	}
}

#endif
