/*
 * taskcaller_systick.c
 *
 *  Created on: 15 May 2023
 *      Author: mchhe
 */

#include "unified.h"


void taskcaller_init(void) {
    taskcaller_fill();

    NVIC_EnableIRQ(SysTicK_IRQn);
    SysTick->SR &= ~(1 << 0);
    SysTick->CMP = 1;
    SysTick->CNT = 0;
    SysTick->CTLR =  STK_STE | STK_STRE | STK_STIE;
}

void SysTick_Reset_Flag(void){
    SysTick->SR = 0;
}

void SysTick_Set_Time(uint32_t time){
    SysTick->CMP=(time/16)-1;
}

uint32_t SysTick_Get_Passed_Time(void){
    return ((SysTick->CMP+1+SysTick->CNT)<<4);
}

void SysTick_Handler(void)
{
    SysTick_Reset_Flag();
    //taskdemo();
    //SysTick_Set_Time(48000000);
    tc_worker(SysTick_Get_Passed_Time());
    SysTick_Set_Time(tc_get_lowest_time());
    //SysTick_Set_Time(24000000);
}

