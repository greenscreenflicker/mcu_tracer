/*
 * taskcaller_app.c
 *
 *  Created on: 18 Aug 2023
 *      Author: mchhe
 */

#include "unified.h"

void taskdemo(void){
    //mcu_tracer_msg("TaskCaller Demo: 10 Seconds Ping.");
}


void taskcaller_fill(void){
    //Task outputing 10 Seconds Ping
    task_t new_task={};
    new_task.function=taskdemo;
    new_task.interval=48000000*10;
    new_task.priority=1;
    new_task.time_remaining=new_task.interval;
    tc_task_add(new_task);

    //Mcu Tracer task, executed each 0.1 seconds

    task_t mcu_tracer_task={};
    mcu_tracer_task.function=mcu_tracer_tick;
    mcu_tracer_task.interval=48000000/10;
    mcu_tracer_task.priority=2;
    mcu_tracer_task.time_remaining=mcu_tracer_task.interval;
    tc_task_add(mcu_tracer_task);
}
