# MCU Tracer

## Overview
MCU Tracer is a software tool for embedded hardware debugging. It's maintained by Digital Power Systems GmbH Karslsruhe and is there used for the development and control of power electronics. It's ment to replace classical serial/printf debugging. 
MCU Tracer allows to readout variables, but also allows to set variables.
The configuration is initially transmitted by the MCU to the PC.
Also the MCU can send classic strings to the PC.

## Supported data types
The following data types are currently supported:
 * int32_t
 * float
 * bool
 
Note that uint64_t is currently not supported and may add strange behaviour.


## Tested OS
 * Linux
 * Ubuntu
 
 
## Getting Started
### Compiling on Linux
* Install the required packages: 
* ```sudo apt install git build-essential libgtk-3-dev```
* Clone the repository
* ```git clone https://gitlab.com/greenscreenflicker/mcu_tracer/```
* Goto the pository
* ```cd pc_software```
* Compile (with 16 jobs)
* ```make -j16```

### Compiling on Windows
* Download and install MSYS2 from http://www.msys2.org
* Open Mingw Shell: Windowskey->mingw64
* Install packages by typing 'pacman -S base-devel mingw-w64-x86_64-toolchain mingw-w64-x86_64-gtk3'
* Open the MinGW64 Shell and compile pc software by typing 'make'
* In case the insallation fails, execute the following command and try angain:
* export PKG_CONFIG_PATH=/mingw32/lib/pkgconfig
* export PKG_CONFIG_PATH=/mingw64/lib/pkgconfig
#### Windows Desktop
To be able to start the device from the desktop, you can add a DeskTopLink
* Find the compiled file in the windows folder.
    * Typically this is in C:\msys64\home\username
* Go to the desktop and create a link

## Crash reporting
### Windows GDB Install
* To report download gdb
* ```pacman -S mingw-w64-x86_64-gdb```
### All Systems
* Open mcu tracer in gdb
* ```gdb ./mcu_tracer```
* Type ```run``` to run the application
* Reproduce crash
* type ```bt```
* Include output in crash report.

## Linux not-enough permissions
### Ubuntu
* ```sudo usermod -a -G tty $USER```
* ```sudo usermod -a -G dialout $USER```


### Arduino
Program the system with the arduino code. Configure A0 to an analog voltage, e.g. 3.3V

## Porting
Arduino is used for demonstration proposes. The code should be easily adoptable
to your specific microcontroller system.

