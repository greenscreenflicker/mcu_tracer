# Collection of Teststrings for the master to send

## Basic ping

A5 00 A5

## Double Ping

A5 00 A5 A5 00 A5

## Init 

A5 01 A4

## Complex init

A5 00 A5 A5 01 A4

Should return something like this

A5 01 01 01 53 79 73 54 69 63 6B 73 01 01 00 4C 45 44 20 52 45 44 01 01 00 4C 45 44 20 47 52 45 45 4E 01 01 00 4C 45 44 20 42 4C 41 55 01 00 00 01 C4

## Send all values

A5 02 A7

## Complex init and send all values

A5 00 A5 A5 01 A4 A5 02 A7

## Request a single value

A5 04 00 00 A1

## Update A Signle Value 

Sets Variable index 00 to zero

A5 03 00 00 00 00 00 00 A6

Should return an answer like this if variable non writeable

A5 03 00 00 00 00 03 7C D9

## Corrupt Update Single Value to test if update runs

A5 03 00 00 00 00 00 A6 A5 03 00 00 00 00 00 00 A6

Should return an answer like

A5 03 00 00 00 00 03 7C D9

## Function init

A5 08 AD

## Function execution

Executes function one (first function

A5 09 01 AD

Should return

A5 09 01 01 AC
