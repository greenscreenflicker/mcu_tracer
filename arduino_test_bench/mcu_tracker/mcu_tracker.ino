#define MCU_TRACER_STARTBYTE 0xA5
#define MCU_TRACER_MAXSETVARIABLES 10

typedef struct mcu_tracer{
  char type;
  char rw;
  char varname[30];
  union {
    int32_t *data_l;
    float    *data_f;
  };
  union {
    int32_t data_lmin;
    float    data_fmin;
  };
  union {
    int32_t data_lmax;
    float    data_fmax;
  };
} mcu_tracer_t;

typedef struct i2cmem_log{
	uint64_t time;
	uint8_t log[40];
        union{
	  int32_t data_l[MCU_TRACER_MAXSETVARIABLES]; //4*32
          float data_f[MCU_TRACER_MAXSETVARIABLES]; //4*32
        };
} i2cmem_logmsg_t;


typedef struct mcu_func{
  char func_name[30];
  uint8_t (*func_ptr)(void);
}  mcu_func_t;

typedef struct converter{
  union {
    int32_t *data_l;
    float    *data_f;
  };
} convert_t;

mcu_tracer_t monitorvars[MCU_TRACER_MAXSETVARIABLES];
mcu_func_t mcufunc[5];
i2cmem_logmsg_t logmsg[2];

int global_checksum;
uint8_t mcu_tracer_checksum;
int32_t debug1, debug2, debugbefore;
int32_t writeable_int;
float   writeable_float;
float debug3;

uint8_t mcu_tracer_registered_func=0;
#define MONITOR_ELEMENTS (sizeof(monitorvars)/sizeof(mcu_tracer_t))

void mcu_tracer_fill(void){
  uint8_t fill=0;
  monitorvars[fill].type=3;
  monitorvars[fill].rw=0;
  monitorvars[fill].data_l=&debug1;
  strcpy(monitorvars[fill].varname,"LED");
  monitorvars[fill].data_lmin=-2;
  monitorvars[fill].data_lmax=2;
  fill++;

  monitorvars[fill].type=1;
  monitorvars[fill].rw=1;
  monitorvars[fill].data_l=&debug2;
  strcpy(monitorvars[fill].varname,"ADC raw");
  monitorvars[fill].data_lmin=0;
  monitorvars[fill].data_lmax=1;
  fill++;

  monitorvars[fill].type=2;
  monitorvars[fill].rw=1;
  monitorvars[fill].data_f=&debug3;
  strcpy(monitorvars[fill].varname,"ADC converted");
  monitorvars[fill].data_fmin=0;
  monitorvars[fill].data_fmax=4;
  fill++;
  
  
  monitorvars[fill].type=1;
  monitorvars[fill].rw=0;
  monitorvars[fill].data_l=&writeable_int;
  strcpy(monitorvars[fill].varname,"int32");
  monitorvars[fill].data_lmin=-2000;
  monitorvars[fill].data_lmax=2000;
  fill++;
  
  monitorvars[fill].type=2;
  monitorvars[fill].rw=0;
  monitorvars[fill].data_f=&writeable_float;
  strcpy(monitorvars[fill].varname,"float");
  monitorvars[fill].data_fmin=-2000;
  monitorvars[fill].data_fmax=2000;
  fill++;
}

uint8_t test_led_on(void){
  mcu_tracer_msg("LED ON");
  debug1=1;
  return 1;
}
uint8_t test_led_off(void){
  mcu_tracer_msg("LED OFF");
  debug1=0;
  return 1;
}
uint8_t print_hello1(void){
  mcu_tracer_msg("This function returns 2.");
  return 2;
}

void mcu_tracer_func_fill(void){
  int fipo=0;
  strcpy(mcufunc[fipo].func_name,"LED on");
  mcufunc[fipo].func_ptr=test_led_on;
  fipo++;

  strcpy(mcufunc[fipo].func_name,"LED off");
  mcufunc[fipo].func_ptr=test_led_off;
  fipo++;

  strcpy(mcufunc[fipo].func_name,"Fail Func");
  mcufunc[fipo].func_ptr=print_hello1;
  fipo++;

  mcu_tracer_registered_func=fipo-1;
}

void mcu_tracer_func_init(void){
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(8); //Reply with order code
  uint8_t fipo=0;
  while(mcufunc[fipo].func_ptr){
    //send data
    if(fipo>254) break;  
    mcu_tracer_write_serial(fipo+1);
    mcu_tracer_write_string(mcufunc[fipo].func_name);
    fipo++;
  }
  mcu_tracer_write_serial(0);//send last element indicator
  mcu_tracer_write_serial(1);//empty string
  mcu_tracer_send_checksum();
}

void mcu_tracer_func_execute(uint8_t id){
  int sucess=0;
  if(id>0){
    //check id;
    uint8_t internal;
    internal=id-1;
    if(internal<=mcu_tracer_registered_func){
      sucess= (mcufunc[internal].func_ptr)();  
    }    
  }
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(9);
  mcu_tracer_write_serial(id);
  mcu_tracer_write_serial(sucess);
  mcu_tracer_send_checksum();
}

#define ORDER_F0_SUPPLIER_INFO 0xF0
const char firstpage_version_major=1;
const char firstpage_version_minor=2;
const char firstpage_manufacturer[]="CrazyCow GmbH";
const char firstpage_description[]="FlyingMouse";

void mcu_tracer_supply_information_reply(void){
  //mcu_tracer_send_mutex_get();
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(ORDER_F0_SUPPLIER_INFO);
  //-Software Version Major (1 bytes)
  mcu_tracer_write_serial((char)firstpage_version_major);
  mcu_tracer_write_serial((char)firstpage_version_minor);
  mcu_tracer_write_string(&firstpage_manufacturer[0]);
  mcu_tracer_write_string(&firstpage_description[0]);
  mcu_tracer_send_checksum();
  //mcu_tracer_send_mutex_giveback();
}

#define ORDER_F1_UNIQUE_ID 0xF1

uint32_t mcu_tracer_uniqueid0(void){
  return 0x1234;
}
uint32_t mcu_tracer_uniqueid1(void){
  return 0x5678;
}
uint32_t mcu_tracer_uniqueid2(void){
  return 0x9ABC;
}


void mcu_tracer_write_4byte_var(int32_t var){
  mcu_tracer_write_serial((var>>(8*3))&0xFF);
  mcu_tracer_write_serial((var>>(8*2))&0xFF);
  mcu_tracer_write_serial((var>>(8*1))&0xFF);
  mcu_tracer_write_serial((var>>(8*0))&0xFF);
}


void mcu_tracer_uniqueid_reply(void){
  //mcu_tracer_send_mutex_get();
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(ORDER_F1_UNIQUE_ID);
  //-Software Version Major (1 bytes)
  mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid0());
  mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid1());
  mcu_tracer_write_4byte_var((int32_t)mcu_tracer_uniqueid2());
  mcu_tracer_send_checksum();
  //mcu_tracer_send_mutex_giveback();
}

/*
MCU->PC
 -Startbyte (1 Byte)
 -Order (0xFD)
 -Time (mcutracer) uint64_t
 -Message (Terminator1)
 -Values (4 Bytes) exactly as Order2 (Transfer all values)
 -SUM(XOR)*/


#define ORDER_FD_LOG 0xFD

void mcu_tracer_write_8byte_var(int64_t var){
  mcu_tracer_write_serial((var>>(8*7))&0xFF);
  mcu_tracer_write_serial((var>>(8*6))&0xFF);
  mcu_tracer_write_serial((var>>(8*5))&0xFF);
  mcu_tracer_write_serial((var>>(8*4))&0xFF);
  mcu_tracer_write_serial((var>>(8*3))&0xFF);
  mcu_tracer_write_serial((var>>(8*2))&0xFF);
  mcu_tracer_write_serial((var>>(8*1))&0xFF);
  mcu_tracer_write_serial((var>>(8*0))&0xFF);
}


void mcu_tracer_single_log_write(int *ptr){
      i2cmem_logmsg_t* msg=(i2cmem_logmsg_t*) ptr;
     	mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
   	mcu_tracer_write_serial(ORDER_FD_LOG);
   
   	mcu_tracer_write_8byte_var(msg->time);
   	mcu_tracer_write_string((char*)msg->log);
   	uint32_t i;
   	for(i=0; i<MCU_TRACER_MAXSETVARIABLES; i++){
   		//check if we have a terminator =>if, break
   		if(monitorvars[i].type==0) break;
   		mcu_tracer_write_4byte_var(msg->data_l[i]);
   	}
   
   	mcu_tracer_send_checksum();
}

//To test: A5 fd 58
void mcu_tracer_log_write_all(void){
  //we fake some data, as storage is not implemted in arduino test bench
 
  logmsg[0].time=8;
  logmsg[0].data_l[0]=0;
  logmsg[0].data_f[2]=3456.7;
  logmsg[0].data_l[1]=0x3333;
  logmsg[0].data_l[3]=3; 
  logmsg[0].data_l[4]=4;
  logmsg[0].data_l[5]=5;
  logmsg[0].data_l[5]=5;
  logmsg[0].data_l[6]=5;
  logmsg[0].data_l[7]=5;
  logmsg[0].data_l[8]=5;
  logmsg[0].data_l[9]=5; 
  strcpy((char*)logmsg[0].log,"Firstlog");
  mcu_tracer_single_log_write((int*)&logmsg[0]);
  
  logmsg[0].time=8;
  logmsg[0].data_l[0]=1;
  logmsg[0].data_f[2]=3456.78;
  logmsg[0].data_l[1]=0xABCD;
  logmsg[0].data_l[3]=3; 
  logmsg[0].data_l[4]=4;
  logmsg[0].data_l[5]=5;
  logmsg[0].data_l[5]=5;
  logmsg[0].data_l[6]=5;
  logmsg[0].data_l[7]=5;
  logmsg[0].data_l[8]=5;
  logmsg[0].data_l[9]=5; 
  strcpy((char*)logmsg[0].log,"Secondlog");
  mcu_tracer_single_log_write((int*)&logmsg[0]);
}
  
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Up and running");
  // Serial.print(monitor_elements,DEC);
  mcu_tracer_fill();
  mcu_tracer_func_fill();
  //DEBUG LED
  pinMode(13, OUTPUT);
}

uint8_t rec_char(void){
  while(!(Serial.available()));
  uint8_t ch;
  ch=Serial.read();
  mcu_tracer_checksum=ch^mcu_tracer_checksum;
  return ch;
}

uint8_t rec_checksum(void){
  while(!(Serial.available()));
  uint8_t ch;
  ch=Serial.read();
  if(mcu_tracer_checksum==ch){
    return 1;
  }
  else{
    //Serial.println("checksum expected:");
    //Serial.write(mcu_tracer_checksum);  
    //Serial.println("<-");
    return 0; 
  }
}

void mcu_tracer_process(void){
  while(1){
    mcu_tracer_checksum=0;
    if(!(Serial.available())){
      //We have no data, we're done
      return;
    }
    uint8_t startbyte=rec_char();
    if(!(startbyte==MCU_TRACER_STARTBYTE)){
      //we have no startbyte
      mcu_tracer_checksum=0;
      //Serial.println("not a start byte");
      //Serial.print(startbyte,HEX);
      continue;
    }
    else{
      //Serial.println("Recieved startbyte");
    }
    //Now we synced to the startbyte

      uint8_t order=rec_char();
    //Serial.print("Order:");
    //Serial.print(order,HEX);
    //Serial.print("\n");
    if(order==1){
      //Serial.println("Order one");
      //we have a Init request
      //we need to check mcu_tracer_checksum now

      if(rec_checksum()){
        // call the init routine;
        //erial.println("Checksum ok");
        mcu_tracer_init();
      }
    }
    else if(order==0){
      if(rec_checksum()){
        mcu_tracer_init_reply();
      }

    }
    else if(order==2){
      if(rec_checksum()){
        mcu_tracer_vals();
      }
    }
    else if(order==3){
      uint32_t data=0;
      uint16_t arraynumber=0;
      uint8_t rec;
      int i;
      //load arrynumber
      for(i=2;i>0;i--){
        arraynumber=(arraynumber<<8);
        rec=rec_char();
        arraynumber=(rec+arraynumber);
      }
      //load data
      for(i=4;i>0;i--){
        data=(data<<8);
        rec=rec_char();
        data=(rec+data);
      }
      if(rec_checksum()){
        mcu_tracer_update(arraynumber,data);
      }
    }
    else if(order==0xF0){
      if(rec_checksum()){
        mcu_tracer_supply_information_reply();
      }
    }
    else if(order==ORDER_F1_UNIQUE_ID){
      if(rec_checksum()){
        mcu_tracer_uniqueid_reply();    
      } 
    }
    else if(order==ORDER_FD_LOG){
      if(rec_checksum()){
        mcu_tracer_log_write_all();
      }
    }
    else if(order==0xFF){
      if(rec_checksum()){
        //execute emergency function
        mcu_tracer_emergency();
        mcu_tracer_emergency_reply();
      }
    }
    else if(order==8){
      //Init reply
      if(rec_checksum()){
        //checksum valid?
        mcu_tracer_func_init();
      }
    }
    else if(order==9){
      //Init reply
      uint8_t funcid=rec_char();
      if(rec_checksum()){
        mcu_tracer_func_execute(funcid);
      }
    }
    else{
      mcu_tracer_msg("recieved unkown order");
    }
  }
}

void mcu_tracer_write_serial(char data){
  global_checksum=global_checksum^data;
  Serial.write(data);
}

void mcu_tracer_write_string(const char* data){
  while( *data != '\0' ){
    mcu_tracer_write_serial(*data++ );
  }
  mcu_tracer_write_serial(1);
}

void mcu_tracer_send_checksum(void){
  Serial.write(global_checksum);
  global_checksum=0;
}
void mcu_tracer_init(void){
  int i;
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(1); //Order type 1: Sending init
  for(i=0; i<MONITOR_ELEMENTS; i++){
    //check if we have a terminator =>if, break
    if(monitorvars[i].type==0) break;
    mcu_tracer_write_serial(monitorvars[i].type);
    mcu_tracer_write_serial(monitorvars[i].rw);
    mcu_tracer_write_string(monitorvars[i].varname);
  }

  //sending terminating byte
  mcu_tracer_write_serial(0); //last byte
  mcu_tracer_write_serial(0); //read only
  mcu_tracer_write_string("");

  mcu_tracer_send_checksum();
}

//Sends actual values to pc
void mcu_tracer_vals(void){
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(2); //order two, we transfer variables
  char i;
  for(i=0; i<MONITOR_ELEMENTS; i++){
    //check if we have a terminator =>if, break
    if(monitorvars[i].type==0) break;
    uint32_t data;
    data=*(monitorvars[i].data_l);
    mcu_tracer_write_serial(data>>(3*8));
    mcu_tracer_write_serial(data>>(2*8));
    mcu_tracer_write_serial(data>>(1*8));
    mcu_tracer_write_serial(data>>(0*8));
  }
  mcu_tracer_send_checksum();
}

//Updates the value in the register
void mcu_tracer_update(uint16_t addr, int32_t val){
  if(addr>MONITOR_ELEMENTS){
    //error, we do not have this addr
    return;
  }

  if(monitorvars[addr].type==1||monitorvars[addr].type==3){
    //Integer
    int32_t *toupdate;
    toupdate=monitorvars[addr].data_l; //getting mcu address of variable
    if(val>monitorvars[addr].data_lmax){
      val=monitorvars[addr].data_lmax;
    }
    if(val<monitorvars[addr].data_lmin){
      val=monitorvars[addr].data_lmin;
    }

    //memcpy(monitorvars[addr].data_l,&val,4);


    *(monitorvars[addr].data_l)=val;
    /*
    *(monitorvars[addr].data_l+1)=(uint8_t)(val<<8);
     *(monitorvars[addr].data_l+1)=(uint8_t)(val<<8);*/
  }else if(monitorvars[addr].type==2){
    convert_t con;
    *(con.data_l)=val;
    float f;
    f=*(con.data_f);
    int32_t *toupdate;
    toupdate=monitorvars[addr].data_f; //getting mcu address of variable
    if(f>monitorvars[addr].data_fmax){
      f=monitorvars[addr].data_fmax;
    }
    if(f<monitorvars[addr].data_fmin){
      f=monitorvars[addr].data_fmin;
    }

    //memcpy(monitorvars[addr].data_l,&val,4);


    *(monitorvars[addr].data_f)=f;
  else{
    mcu_tracer_msg("Data type not yet supported");
  }
  mcu_tracer_inform(addr);
}

void mcu_tracer_msg(const char* msg){
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(0xFE);
  mcu_tracer_write_string(msg);
  mcu_tracer_send_checksum();
}

void mcu_tracer_inform(uint16_t addr){
  if(addr>MONITOR_ELEMENTS) return; //we do not have this.
  int32_t val=*(monitorvars[addr].data_l);

  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(3); //order 3, we transfer single value
  mcu_tracer_write_serial(addr>>(1*8));
  mcu_tracer_write_serial(addr>>(0*8));
  mcu_tracer_write_serial(val>>(3*8));
  mcu_tracer_write_serial(val>>(2*8));
  mcu_tracer_write_serial(val>>(1*8));
  mcu_tracer_write_serial(val>>(0*8));
  mcu_tracer_send_checksum();
}

void mcu_tracer_init_reply(void){
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(0x00);
  mcu_tracer_send_checksum();
}


void mcu_tracer_emergency_reply(void){
  mcu_tracer_write_serial(MCU_TRACER_STARTBYTE);
  mcu_tracer_write_serial(0xFF);
  mcu_tracer_send_checksum();
}

void mcu_tracer_emergency(void){
  mcu_tracer_msg("Place here your emergency code");
}

char msg[40];
void loop() {
  // put your main code here, to run repeatedly:
  mcu_tracer_process();

  if(debug1==1){
    digitalWrite(13, HIGH);
  }
  else{
    digitalWrite(13, LOW);
  }
  /*
  if(debugbefore!=debug1){
   itoa(debug1,&msg[0],10);
   strcat(msg,"=Debug one");
   mcu_tracer_msg(msg);
   
   }*/
  debugbefore=debug1;
  debug2=analogRead(A0);
  debug3=debug2 * (5.0 / 1023.0);
  /*
  if(Serial.available()){
   Serial.write(Serial.read());
   }*/
}

