#define MCU_TRACER_STARTBYTE 0xA5


typedef struct mcu_tracer{
  char type;
  char rw;
  char varname[30];
  union {
    int32_t *data_l;
    float    *data_f;
  };
  union {
    int32_t data_lmin;
    float    data_fmin;
  };
  union {
    int32_t data_lmax;
    float    data_fmax;
  };
} mcu_tracer_t;

typedef struct i2cmem_log{
	uint64_t time;
	uint8_t log[40];
	int32_t data[10]; //4*32
} i2cmem_logmsg_t;


typedef struct mcu_func{
  char func_name[30];
  uint8_t (*func_ptr)(void);
}  mcu_func_t;
